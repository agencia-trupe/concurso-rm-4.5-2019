# Programa RM 4.5 2019

Para subir o projeto:

```
docker-compose up -d
```

O docker-compose vai subir dois containers. Sendo que a aplicação vai estar disponível na porta `81`.

```
rm-2019-app
rm-2019-mysql
```

É possível acessar o container do Laravel para executar os comandos artisan:

```bash
docker exec -it rm-2019-app bash
cd laravel-rm
php artisan migrate:refresh --seed    # <- zera a base e importa a seed configurada no .env
```

Com o npm instalado é possivel também utilizar o LiveReload do Laravel Mix:

```bash
npm run watch   # <- Executa por default na porta 3000
```
