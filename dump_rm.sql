-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 172.24.0.2    Database: rm
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avaliacoes`
--

DROP TABLE IF EXISTS `avaliacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `casos_id` int(10) unsigned NOT NULL,
  `avaliador_id` int(10) unsigned NOT NULL,
  `criterio_1` decimal(3,1) DEFAULT NULL,
  `criterio_2` decimal(3,1) DEFAULT NULL,
  `criterio_3` decimal(3,1) DEFAULT NULL,
  `criterio_4` decimal(3,1) DEFAULT NULL,
  `criterio_5` decimal(3,1) DEFAULT NULL,
  `media` decimal(3,1) DEFAULT NULL,
  `avaliado_em` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avaliacoes_casos_id_foreign` (`casos_id`),
  KEY `avaliacoes_avaliador_id_foreign` (`avaliador_id`),
  CONSTRAINT `avaliacoes_avaliador_id_foreign` FOREIGN KEY (`avaliador_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE,
  CONSTRAINT `avaliacoes_casos_id_foreign` FOREIGN KEY (`casos_id`) REFERENCES `casos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacoes`
--

LOCK TABLES `avaliacoes` WRITE;
/*!40000 ALTER TABLE `avaliacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `embed` text COLLATE utf8mb4_unicode_ci,
  `texto` text COLLATE utf8mb4_unicode_ci,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `casos`
--

DROP TABLE IF EXISTS `casos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coordenador_id` int(10) unsigned NOT NULL,
  `categoria` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arquivo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_preenchimento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'a_preencher',
  `titulo_pt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo_paciente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idade_paciente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doenca_diagnosticada` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relato_pt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `relato_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussao_pt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussao_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `evolucao` text COLLATE utf8mb4_unicode_ci,
  `referencias` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `preenchido_em` datetime DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  `distribuido_em` datetime DEFAULT NULL,
  `excluido_em` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `casos_coordenador_id_foreign` (`coordenador_id`),
  CONSTRAINT `casos_coordenador_id_foreign` FOREIGN KEY (`coordenador_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casos`
--

LOCK TABLES `casos` WRITE;
/*!40000 ALTER TABLE `casos` DISABLE KEYS */;
INSERT INTO `casos` VALUES (1,9,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(2,9,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(3,9,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(4,9,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(5,10,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(6,10,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(7,10,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL),(8,10,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a_preencher','','','','','','','','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `casos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_invite`
--

DROP TABLE IF EXISTS `group_invite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_invite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_invite_group_name_unique` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_invite`
--

LOCK TABLES `group_invite` WRITE;
/*!40000 ALTER TABLE `group_invite` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_invite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_erros`
--

DROP TABLE IF EXISTS `log_erros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_erros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_erros`
--

LOCK TABLES `log_erros` WRITE;
/*!40000 ALTER TABLE `log_erros` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_erros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (101,'2014_10_12_000000_create_users_table',1),(102,'2014_10_12_100000_create_password_resets_table',1),(103,'2017_05_26_150221_create_casos_table',1),(104,'2017_05_26_150230_create_avaliacoes_table',1),(105,'2017_05_31_154557_create_log_erros_table',1),(106,'2018_06_06_235941_alter_usuarios_table',1),(107,'2019_03_17_160358_alter_coautor_confirm_table',1),(108,'2019_03_21_015911_create_blog_table',1),(109,'2019_04_21_185928_create_group_invites_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperar_senha`
--

DROP TABLE IF EXISTS `recuperar_senha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperar_senha` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `recuperar_senha_email_index` (`email`),
  KEY `recuperar_senha_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperar_senha`
--

LOCK TABLES `recuperar_senha` WRITE;
/*!40000 ALTER TABLE `recuperar_senha` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperar_senha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `centro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grupo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_termos` int(11) NOT NULL DEFAULT '0',
  `check_termos_date` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_criacao_senha` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_enviado_em` datetime DEFAULT NULL,
  `senha_criada_em` datetime DEFAULT NULL,
  `confirmado_em` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','Daniel Oliveira (Admin)','000000000','São Paulo','SP','','','','daniel.oliveira@novartis.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(2,'admin','Usuário Trupe (Admin)','000000000','São Paulo','SP','','','','leila@trupe.net',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(3,'avaliador','Daniel Oliveira (Avaliador)','000000000','Belo Horizonte','MG','','','','danielmalavazi@bol.com.br',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(4,'avaliador','Usuário Trupe (Avaliador) #1','000000000','Belo Horizonte','MG','','','','leilaavaliador@trupe.net',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(5,'avaliador','Usuário Trupe (Avaliador) #2','000000000','Curitiba','PR','','','','avaliador2@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(6,'avaliador','Usuário Trupe (Avaliador) #3','000000000','São Paulo/Santo André','SP','','','','avaliador3@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(7,'avaliador','Usuário Trupe (Avaliador) #4','000000000','Rio de Janeiro','RJ','','','','avaliador4@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(8,'avaliador','Usuário Trupe (Avaliador) #5','000000000','São Paulo','SP','','','','avaliador5@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(9,'coordenador','Daniel Oliveira (Coordenador)','000000000','São Paulo','SP','','','grupo_b','coordenador@gmail.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(10,'coordenador','Usuário Trupe (Coordenador)','000000000','Curitiba','PR','','','grupo_a','coordenador1@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(11,'medico','Daniel Oliveira (Médico)','000000000','Curitiba','PR','','','grupo_a','danielmalavazi40@gmail.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(12,'medico','Usuário Trupe (Médico) #1','000000000','Curitiba','PR','','','grupo_a','medico1@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(13,'medico','Usuário Trupe (Médico) #2','000000000','Curitiba','PR','','','grupo_a','medico2@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(14,'medico','Usuário Trupe (Médico) #3','000000000','Curitiba','PR','','','grupo_b','medico3@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(15,'medico','Usuário Trupe (Médico) #4','000000000','São Paulo','SP','','','grupo_b','medico4@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(16,'medico','Usuário Trupe (Médico) #5','000000000','São Paulo','SP','','','grupo_b','medico5@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(17,'medico_naoparticipante','Daniel Oliveira (Médico não participante)','000000000','Curitiba','PR','','','','dani_m_o@yahoo.com.br',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(18,'medico_naoparticipante','Usuário Trupe (Médico não participante) #1','000000000','Rio de Janeiro','RJ','','','','mediconp1@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL),(19,'editor','Usuário Trupe (Editor)','000000000','Curitiba','PR','','','','editor@trupe.com',0,NULL,'$2y$10$dR5nTdAU/LOqWW29sUPeoOd59tjVsscjmUjcjbbm6h1iMgrZop7pC',NULL,NULL,'2019-04-28 19:01:41',NULL,NULL,'2019-04-28 19:01:41',NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-28 19:03:21
