<?php

namespace RM\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = [
        'titulo',
        'slug',
        'embed',
        'texto',
        'titulo_caso_clinico',
        'texto_caso_clinico'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function scopeOrdenado($query) {
        return $query->orderBy('ordem');
    }
}
