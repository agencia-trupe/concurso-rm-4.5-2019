<?php

namespace RM\Models;

use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Caso extends Model
{

  protected $table = 'casos';

  protected $fillable = [
    'codigo',
    'arquivo',
    'autor',
    'coautor_1',
    'coautor_2',
    'coautor_3',
    'coautor_4',
    'coautor_5',
    'enviado_em',
    'distribuido_em',
    'excluido_em'
  ];

  protected $hidden = [
    'arquivo'
  ];

  protected $dates = [
    'enviado_em',
    'distribuido_em',
    'excluido_em',
    'created_at',
    'updated_at'
  ];

  protected $appends = ['sent_at_f', 'is_enviado'];

  // COMPUTED ATTRIBUTES

  public function getIsEnviadoAttribute()
  {
    return is_null($this->enviado_em) ? false : true;
  }

  public function getIsDistribuidoAttribute()
  {
    return is_null($this->distribuido_em) ? false : true;
  }

  public function getPodeSerEnviadoAttribute()
  {
    $datamax = Carbon::createFromFormat('d/m/Y', env('SITE_DATA_MAX_ENVIO'));
    $hoje = Carbon::now();
    return $hoje->lte($datamax);
  }

  public function getPontuacaoAttribute()
  {
    $avaliacoes = $this->avaliacoes()->avaliados()->get();
    $total = 0;

    foreach($avaliacoes as $aval)
      $total = $total + $aval->media;

    return $total;
  }

  public function getStatusFrontAttribute()
  {
    switch ($this->status_preenchimento) {
      case 'a_preencher':
        return __('rm.A_PREENCHER');
        break;
      case 'em_andamento':
        return __('rm.EM_ANDAMENTO');
        break;
      case 'preenchido':
        return __('rm.PREENCHIDO');
        break;
      case 'enviado':
        return __('rm.ENVIADO');
        break;
    }
  }

  public function getSentAtFAttribute()
  {
    return $this->enviado_em->format('d/m/Y - H:i');
  }

  // SCOPES

  public function scopePorDia($query, $data)
  {
    $data = Carbon::createFromFormat('d/m/Y', $data);
    return $query->where(DB::raw('DATE(enviado_em)'), $data->format('Y-m-d'));
  }

  public function scopeCategoria($query, $value)
  {
    if (is_array($value)) {
      return $query->whereIn('categoria', $value);
    }

    return $query->where('categoria', $value);
  }

  public function scopeDistribuiveis($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNull('distribuido_em');
  }

  public function scopeDistribuidos($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNotNull('distribuido_em');
  }

  public function scopeAvaliados($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNotNull('distribuido_em')
                 ->whereHas('avaliacoes', function($query){
                   $query->avaliados();
                 });
  }

  public function scopeFindByCodigoOrFail($query, $codigo)
  {
    return $query->where('codigo', $codigo)->firstOrFail();
  }

  public function scopeOrdenarPorRanking($query)
  {
    return $query;
  }

  // RELATIONSHIPS

  public function avaliacoes()
  {
    return $this->hasMany('RM\Models\Avaliacao', 'casos_id');
  }

  public function coordenador()
  {
    return $this->belongsTo('RM\Models\User', 'coordenador_id');
  }

}
