<?php

namespace RM\Models;

use Illuminate\Database\Eloquent\Model;

class GroupInvite extends Model
{
    protected $table = 'group_invite';

    protected $fillable = ['group_name'];
}
