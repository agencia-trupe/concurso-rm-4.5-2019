<?php

namespace RM\Models;

use RM\Notifications\ResetarSenha;
use RM\Notifications\UsuarioCriado;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use RM\Models\GroupInvite;

class User extends Authenticatable
{

  use Notifiable;

  protected $table = 'usuarios';

  protected $fillable = [
    'tipo',
    'nome',
    'crm',
    'is_ativo',
    'cidade',
    'estado',
    'telefone',
    'centro',
    'grupo',
    'email',
    'password',
    'check_termos',
    'check_termos_date',
  ];

  protected $hidden = [
    'password',
    'remember_token'
  ];

  protected $dates = [
    'senha_criada_em',
    'email_enviado_em',
    'check_termos_date',
    'confirmado_em',
    'created_at',
    'updated_at'
  ];

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetarSenha($token, $this->email));
  }

  public function sendPasswordCreationNotification($token)
  {
    $this->notify(new UsuarioCriado($token, $this->email));
  }

  // COMPUTED ATTRIBUTES

  public function getTipoExtensoAttribute()
  {
    return ucwords(str_replace('_', ' ', str_replace('naop', 'nao p', $this->tipo)));
  }

  public function getIsCoordenadorAttribute()
  {
    return $this->tipo == 'coordenador';
  }

  public function getIsAvaliadorAttribute()
  {
    return $this->tipo == 'avaliador';
  }

  public function getIsAdminAttribute()
  {
    return $this->tipo == 'admin';
  }

  public function getIsMedicoAttribute()
  {
    return $this->tipo == 'medico';
  }

  public function getIsMedicoNaoParticipanteAttribute()
  {
    return $this->tipo == 'medico_naoparticipante';
  }

  public function getIsEditorAttribute()
  {
    return $this->tipo == 'editor';
  }

  public function getIsAtivoAttribute()
  {
    return !is_null($this->senha_criada_em);
  }

  public function getIsConfirmedAttribute()
  {
    return !is_null($this->confirmado_em);
  }

  public function getIsConsentGivenAttribute()
  {
    return $this->check_termos == 1 && !is_null($this->check_termos_date);
  }

  public function getPodeAvaliarAttribute($caso_id)
  {
    // verificar se tem uma avaliação pra esse avaliador e id de caso
    return sizeof($this->avaliacoes()->where('casos_id', $caso_id)->get()) > 0 ? true : false;
  }

  public function getStatusEnvioCaso1Attribute()
  {
    $caso = $this->casos()->categoria(1)->first();

    return is_null($caso->enviado_em) ?
            "<strong>".__('rm.NAO_ENVIADO')."</strong>" :
            __("rm.ENVIADO_EM").$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso2Attribute()
  {
    $caso = $this->casos()->categoria(2)->first();

    return is_null($caso->enviado_em) ?
            "<strong>".__('rm.NAO_ENVIADO')."</strong>" :
            __("rm.ENVIADO_EM").$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso3Attribute()
  {
    $caso = $this->casos()->categoria(3)->first();

    return is_null($caso->enviado_em) ?
            "<strong>".__('rm.NAO_ENVIADO')."</strong>" :
            __("rm.ENVIADO_EM").$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso4Attribute()
  {
    $caso = $this->casos()->categoria(4)->first();

    return is_null($caso->enviado_em) ?
            "<strong>".__('rm.NAO_ENVIADO')."</strong>" :
            __("rm.ENVIADO_EM").$caso->enviado_em->format('d/m/Y');
  }

  public function getLinkAtivacaoAttribute()
  {
    return ENV('APP_URL') . "/criar-senha/{$this->token_criacao_senha}/{$this->email}";
  }

  // SCOPES

  public function scopeAdmin($query)
  {
    return $query->where('tipo', 'admin');
  }

  public function scopeWithConsent($query)
  {
    return $query->where('check_termos', 1)
                 ->whereNotNull('check_termos_date')
                 ->orderBy('check_termos_date', 'DESC');
  }

  public function scopeAvaliadores($query)
  {
    return $query->where('tipo', 'avaliador');
  }

  public function scopeCoordenadores($query)
  {
    return $query->where('tipo', 'coordenador');
  }

  public function scopeEditores()
  {
    return $query->where('tipo', 'editor');
  }

  public function scopeFiltrarLocal($query, $cidade)
  {
    return $query->where('cidade', '!=', $cidade);
  }

  public function scopeAtivos($query)
  {
    return $query->whereNotNull('senha_criada_em');
  }

  public function scopeNaoIniciados($query)
  {
    return $query->whereNull('email_enviado_em');
  }

  public function scopeOrdenarPorMenosAvaliacoes($query)
  {
    return $query->withCount('avaliacoes')
                 ->orderBy('avaliacoes_count', 'asc');
  }

  public function scopeGroup($query, $group)
  {
    return $query->where([
      ['grupo', '=', $group],
      ['grupo', '!=', ''],
      ['tipo', '=', 'medico']
    ]);
  }

  public function scopeGroupCoordenador($query, $group)
  {
    return $query->where([
      ['grupo', '=', $group],
      ['grupo', '!=', ''],
      ['tipo', '=', 'coordenador']
    ]);
  }

  public function scopeWholeGroup($query, $group)
  {
    return $query->where([
      ['grupo', '=', $group],
      ['grupo', '!=', '']
    ]);
  }

  public function canRemove() {
    $casos = $this->casos()->get();
    foreach ($casos as $caso) {
      if ($caso->isEnviado) {
        return false;
      }
    }
    return true;
  }

  public function canAddToGroup() {
    return !GroupInvite::where('group_name', $this->grupo)->exists();
  }

  // RELATIONSHIPS

  public function invite() {
    return $this->hasOne('RM\Models\GroupInvite', 'group_name', 'grupo');
  }

  public function casos()
  {
    if ($this->tipo == 'coordenador') {
      return $this->hasMany('RM\Models\Caso', 'coordenador_id');
    }
    
    $coordenador = User::groupCoordenador(Auth::user()->grupo)->firstOrFail();
    return $coordenador->casos();
  }

  public function avaliacoes()
  {
    return $this->hasMany('RM\Models\Avaliacao', 'avaliador_id');
  }

}
