<?php

namespace RM\Models;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{

  protected $table = 'avaliacoes';

  protected $fillable = [
    'casos_id',
    'avaliador_id',
    'criterio_1',
    'criterio_2',
    'criterio_3',
    'avaliado_em'
  ];

  //protected $hidden = [];

  protected $dates = [
    'avaliado_em',
    'created_at',
    'updated_at'
  ];

  // COMPUTED ATTRIBUTES

  public function getIsAvaliadoAttribute()
  {
    return is_null($this->avaliado_em) ? false : true;
  }

  public function getPodeSerAvaliadoAttribute()
  {
    $datamax = Carbon::createFromFormat('d/m/Y', env('SITE_DATA_MAX_AVALIACAO'));
    $hoje = Carbon::now();
    return $hoje->lte($datamax);
  }

  // SCOPES

  public function scopeCategoria($query, $categoria)
  {
    return $query->whereHas('caso', function($query) use ($categoria){
      $query->where('categoria', $categoria);
    });
  }

  public function scopeAvaliados($query)
  {
    return $query->whereNotNull('avaliado_em');
  }

  // SCOPES ESPECIAIS Avaliacoes@index

  public function scopeNovas($query, $categoria){
    return $query->categoria($categoria)->whereNull('avaliado_em')->get();
  }

  public function scopeHistorico($query, $categoria){
    return $query->categoria($categoria)->whereNotNull('avaliado_em')->get();
  }

  // RELATIONSHIPS

  public function caso()
  {
    return $this->belongsTo('RM\Models\Caso', 'casos_id');
  }

  public function avaliador()
  {
    return $this->belongsTo('RM\Models\User', 'avaliador_id');
  }

}
