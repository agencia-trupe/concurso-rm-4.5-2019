<?php

namespace RM\Providers;

use Auth;
use Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Validator::extend('casoNaoEnviado', function ($attribute, $value, $parameters, $validator) {
        $caso = Auth::user()->casos()->categoria($value)->first();
        return !$caso->isEnviado;
      });

      Validator::extend('coordenadorPodeEnviar', function ($attribute, $value, $parameters, $validator) {
        $caso = Auth::user()->casos()->categoria($value)->first();
        return $caso->podeSerEnviado;
      });

      Validator::extend('notaValida', function ($attribute, $nota, $parameters, $validator) {
        return $nota >= 1 && $nota <= 10;
      });

      Validator::extend('avaliacaoPodeSerAtualizada', function ($attribute, $value, $parameters, $validator) {
        // retorna true se avaliador logado é dono da avaliação e está dentro da data max da avaliação
        $avaliacao_id = $value;
        $avaliacao = \RM\Models\Avaliacao::findOrFail($avaliacao_id);

        if(Auth::user()->id != $avaliacao->avaliador_id) {
          return false;
        }

        return $avaliacao->podeSerAvaliado;
      });

      Validator::extend('groupUnique', function($attribute, $value, $parameters, $validator) {
        if ($parameters[0] == 'medico') {
          return true;
        }

        $groupNotInUse = \RM\Models\User::where('grupo', '=', $value)->where('tipo', '=', 'coordenador')->get()->isEmpty();        
        return $groupNotInUse;
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
