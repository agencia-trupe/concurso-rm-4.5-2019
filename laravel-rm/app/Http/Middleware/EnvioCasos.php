<?php

namespace RM\Http\Middleware;

use Closure;
use Auth;

class EnvioCasos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->isMedico && !Auth::user()->isCoordenador) {
            return redirect('/');
        }
        return $next($request);
    }
}
