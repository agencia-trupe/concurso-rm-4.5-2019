<?php

namespace RM\Http\Middleware;

use Closure;
use Auth;
use RM\Models\User;

class FullyAuthorizedGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->canSendCase() && !$this->isGroupFullyAuthorized()) {
            return redirect('aviso-grupo');
        }
        return $next($request);
    }

    private function canSendCase() {
        return Auth::user()->isCoordenador || Auth::user()->isMedico;
    }

    private function isGroupFullyAuthorized(): bool
    {
        $groupFullyAuthorized = true;
        $group = User::group(Auth::user()->grupo)->get();
        
        foreach ($group as $group_member) {
            if (is_null($group_member->confirmado_em)) {
                $groupFullyAuthorized = false;
            }
        }
        
        return $groupFullyAuthorized;
    }
}
