<?php

namespace RM\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthAtivos
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!Auth::user()->isAtivo){
        $request->session()->flash('flash_login', Auth::user()->email);
        Auth::logout();
        return redirect('/login')->withErrors(['email' => ['Usuário inativo']]);
      }

      return $next($request);
    }
}
