<?php

namespace RM\Http\Middleware;

use Closure;
use Auth;

class CheckEditor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->isEditor) {
            return redirect('/');
        }

        return $next($request);
    }
}
