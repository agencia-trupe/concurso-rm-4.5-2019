<?php

namespace RM\Http\Middleware;

use Closure;
use Auth;

class CheckMedicoNaoParticipante
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->isMedicoNaoParticipante) {
            return redirect('/');
        }
        return $next($request);
    }
}
