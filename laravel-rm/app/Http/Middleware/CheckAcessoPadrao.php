<?php

namespace RM\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAcessoPadrao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!Auth::user()->isCoordenador && 
         !Auth::user()->isAvaliador && 
         !Auth::user()->isMedico && 
         !Auth::user()->isMedicoNaoParticipante &&
         !Auth::user()->isEditor) {
        return redirect('/');
      }

      return $next($request);
    }
}
