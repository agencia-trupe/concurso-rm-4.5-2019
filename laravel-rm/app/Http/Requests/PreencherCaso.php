<?php

namespace RM\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreencherCaso extends FormRequest
{

    protected $errorBag = 'preencher';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria' => 'required|in:1,2,3,4',
            'titulo_pt' => 'required|max:255',
            'titulo_en' => 'required|max:255',
            'sexo_paciente' => 'required|in:masculino,feminino',
            'idade_paciente' => 'required|numeric|min:18',
            'doenca_diagnosticada' => 'required|in:leucemia_mieloide_cronica',
            'relato_pt' => 'required',
            'relato_en' => 'required',
            'discussao_pt' => 'required',
            'discussao_en' => 'required',
            'evolucao' => 'sometimes',
            'referencias' => 'required',
        ];
    }
}
