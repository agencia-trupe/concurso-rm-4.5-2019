<?php

namespace RM\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmeterCaso extends FormRequest
{

    protected $errorBag = 'submeter';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '_categoria' => 'required|in:1,2,3,4|casoNaoEnviado|coordenadorPodeEnviar',
            'autor_principal' => 'required'
        ];
    }
}
