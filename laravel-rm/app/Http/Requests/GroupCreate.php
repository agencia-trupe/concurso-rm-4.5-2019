<?php

namespace RM\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'tipo' => 'required|in:coordenador,medico',
            'crm' => 'required',
            'grupo' => "sometimes|groupUnique:{$this->request->get('tipo')}",
            'email' => 'required|email|unique:usuarios,email',
            'estado' => 'required|max:2'
        ];
    }
}
