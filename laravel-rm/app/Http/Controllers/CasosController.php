<?php

namespace RM\Http\Controllers;

use DB;
use Auth;
use Notification;
use RM\Models\User;
use RM\Notifications\CasoEnviado;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use RM\Mail\NotificarFarmacovigilancia;
use RM\Http\Requests\SubmeterCaso;
use RM\Http\Requests\PreencherCaso;

class CasosController extends Controller{

  public function __construct()
  {
    $this->middleware(['auth', 'user_consent']);
  }

  public function submeter(){

    $casoCat1 = Auth::user()->casos()->categoria('1')->first();
    $casoCat2 = Auth::user()->casos()->categoria('2')->first();
    $casoCat3 = Auth::user()->casos()->categoria('3')->first();
    $casoCat4 = Auth::user()->casos()->categoria('4')->first();

    return view('casos.submeter', [
      'casoCat1' => $casoCat1,
      'casoCat2' => $casoCat2,
      'casoCat3' => $casoCat3,
      'casoCat4' => $casoCat4
    ]);
  }

  public function postSubmeter(SubmeterCaso $request){
    try {

      $caso = Auth::user()->casos()->categoria($request->_categoria)->first();

      $codigo = $this->gerarCodigo(Auth::user()->id, $caso->categoria, $caso->id);
      $caso->codigo = $codigo;
      $caso->autor = $request->autor_principal;      
      $caso->status_preenchimento = 'enviado';
      $caso->enviado_em = Carbon::now();

      $caso->save();

      $admins = User::admin()->ativos()->get();

      foreach($admins as $adm) {
        $adm->notify(new CasoEnviado($caso, $adm->email));
      }

      Mail::to(env('EMAIL_FARMACOVIGILANCIA'))->send(new NotificarFarmacovigilancia($caso));

      $request->session()->flash('sucesso', __('Seu caso clínico foi enviado com sucesso!'));

      return redirect('submeter-caso');

    } catch (\Exception $e) {

      $request->flash();

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Erro ao enviar seu caso clínico! (Erro: :err)', ['err' => $erro])));

    }
  }

  public function downloadFormulario(){    
    $titulo = "Formulário de Submissão RM 45.docx";
    $path = env('SITE_ARQUIVO_FORM');
    return response()->download($path, $titulo);
  }

  private function gerarCodigo($idcoordenador, $idcategoria, $idcaso)
  {
    $ids = $idcategoria.$idcoordenador.$idcaso;
    return 'C'.str_pad($ids, 6, '0', STR_PAD_LEFT);
  }

  public function preencherCaso($categoria) {
    $caso = Auth::user()->casos()->categoria($categoria)->first();
    return view('casos.preencher', [
      'caso' => $caso,
      'nro_categoria' => $categoria,
      'coautores' => $this->getCoautores($caso)
    ]);
  }

  private function getCoautores($caso) {
    return [
      'grupo' => $caso->coordenador->grupo,
      'coordenador' => $caso->coordenador,
      'medicos' => User::group($caso->coordenador->grupo)->get()
    ];
  }

  public function postPreencherCaso(PreencherCaso $request) {
    $this->updateCaso($request, 'em_andamento');
    return redirect('submeter-caso');
  }

  public function postPreencherCasoFinalizar(PreencherCaso $request) {
    $this->updateCaso($request, 'preenchido');
    return back();
  }

  public function quillImageUpload(Request $request) {
    $this->validate($request, [
      'upload_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
    ]);

    $image = $request->file('upload_file');
    $upload_dir = 'images/uploads';
    $filename = uniqid() . '.' . $image->getClientOriginalExtension();
    $image->move(base_path().'/../public_html/'.$upload_dir, $filename);

    return json_encode(['file_path' => ENV('APP_URL').'/images/uploads/'.$filename]);
  }

  private function updateCaso($request, $status) {
    $request->replace(array_map(function($field) {
      return str_replace("\r\n", "\n", $field);
    }, $request->all()));
    
    try {
      $caso = Auth::user()->casos()->categoria($request->categoria)->first();
      $caso->preenchido_em = Carbon::now();
      $caso->titulo_pt = $request->titulo_pt;
      $caso->titulo_en = $request->titulo_en;
      $caso->sexo_paciente = $request->sexo_paciente;
      $caso->idade_paciente = $request->idade_paciente;
      $caso->doenca_diagnosticada = $request->doenca_diagnosticada;
      $caso->relato_pt = $request->relato_pt;
      $caso->relato_en = $request->relato_en;
      $caso->discussao_pt = $request->discussao_pt;
      $caso->discussao_en = $request->discussao_en;
      $caso->evolucao = $request->evolucao;
      $caso->referencias = $request->referencias;
      $caso->status_preenchimento = $status;
      $caso->save();
    } catch (\Exception $e) {
      $request->flash();

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Erro ao atualizar caso clínico! (Erro: :err)', ['err' => $erro])));
    }
  }
}
