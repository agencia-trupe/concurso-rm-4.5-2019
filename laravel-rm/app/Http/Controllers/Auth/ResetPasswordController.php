<?php

namespace RM\Http\Controllers\Auth;

use RM\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords{
        reset as postResetTrait;
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request){
        $this->validate($request, [
            'token' => 'required',
            'email'    => 'required|email|max:255|exists:usuarios,email',
            'password' => 'required|min:8|confirmed|regex:/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z]).*$/'
        ], [
            'password.regex' => __('A senha deve conter pelo menos 1 letra maiúscula e 1 letra minúscula')
        ]);
        return $this->postResetTrait($request);
      }    
}
