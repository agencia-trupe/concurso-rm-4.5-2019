<?php

namespace RM\Http\Controllers;

use RM\Models\User;
use Illuminate\Http\Request;
use RM\Http\Requests\GroupCreate;
use RM\Notifications\UsuarioCriado;
use Illuminate\Support\Str;

class AdminGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index() {
        return view('admin.group-list',[
            'coordenadores' => User::coordenadores()->get()
        ]);
    }

    public function create() {
        return view('admin.group-create', [
            'title' => __('rm.CADASTRAR_COORDENADOR'),
            'destino_form' => 'manage-groups.store',
            'tipo' => 'coordenador'
        ]);
    }

    public function store(GroupCreate $request) {
        try {
            $coordenador = \RM\Models\User::create($request->only([
                'nome', 'crm', 'cidade', 'estado',
                'telefone', 'email', 'tipo'
            ]));
            $coordenador->grupo = Str::slug($coordenador->nome).date('YmdHis');
            $coordenador->save();
            for ($c=1; $c <= 4; $c++) {
                \DB::table('casos')->insert([
                    'coordenador_id' => $coordenador->id,
                    'categoria' => $c
                ]);
            }
            $request->session()->flash('sucesso', __('Coordenador inserido com sucesso!'));
            return redirect('gerenciar-grupos');
        } catch (\Exception $e) {
            $request->flash();
      
            $erro = logar_erro($e, Auth::user()->id);
            return back()->withErrors(array(__('Erro ao inserir coordenador! (Erro: :err)', ['err' => $erro])));
        }
    }

    public function addForm($group_name) {
        return view('admin.group-create', [
            'title' => __('rm.CADASTRAR_MEDICO'),
            'destino_form' => 'manage-groups.add',
            'tipo' => 'medico',
            'group_name' => $group_name
        ]);
    }

    public function add(GroupCreate $request) {
        try {
            $medico = \RM\Models\User::create($request->only([
                'nome', 'crm', 'cidade', 'estado',
                'telefone', 'grupo', 'email', 'tipo'
            ]));
            $request->session()->flash('sucesso', __('Médico inserido no grupo com sucesso!'));
            return redirect('gerenciar-grupos');
        } catch (\Exception $e) {
            $request->flash();
      
            $erro = logar_erro($e, \Auth::user()->id);
            return back()->withErrors(array(__('Erro ao inserir médico! (Erro: :err)', ['err' => $erro])));
        }
    }

    public function invite(Request $request, $group_name) {
        try {
            $invite = \RM\Models\GroupInvite::create([
                'group_name' => $group_name
            ]);
            $grupo = \RM\Models\User::wholeGroup($group_name)->get();
            foreach ($grupo as $membro) {
                $token = str_random(32);
                $membro->email_enviado_em = Date('Y-m-d H:i:s');
                $membro->token_criacao_senha = $token;
                $membro->notify( new UsuarioCriado($token, $membro->email));
                $membro->save();
            }
            $request->session()->flash('sucesso', __('E-mail enviado com sucesso'));
            return back();
        } catch (\Exception $e) {
            $erro = logar_erro($e, \Auth::user()->id);
            return back()->withErrors(array(__('Erro ao convidar grupo! (Erro: :err)', ['err' => $erro])));
        }
        
    }

    public function destroy(Request $request, $coordenador_id) {
        $coordenador = User::find($coordenador_id);

        if (!$coordenador || !$coordenador->canRemove()) {
            return back();
        }
        
        $coordenador->invite()->delete();
        $coordenador->casos()->delete();
        $coordenador->group($coordenador->grupo)->delete();
        $coordenador->delete();
        
        $request->session()->flash('sucesso', __('Coordenador removido com sucesso'));
        return back();
    }

    public function destroyGroupMember(Request $request, $medico_id) {
        $medico = User::find($medico_id);

        if (!$medico) {
            return back();
        }
        
        $medico->delete();
        
        $request->session()->flash('sucesso', __('Membro do grupo removido com sucesso'));
        return back();
    }
}
