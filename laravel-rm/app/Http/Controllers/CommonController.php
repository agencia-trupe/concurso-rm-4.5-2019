<?php

namespace RM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;

class CommonController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth', 'auth.ativos', 'user_consent']);
  }

  public function home(){
    return view('common.home');
  }

  public function regulamento(){
    if (App::getLocale() == 'en') {
      return view('common.regulamento-en');  
    }
    return view('common.regulamento');
  }

  public function cronograma(){
    $today = date('Y-m-d');

    $faseAtual = 1;

    if ($today >= '2019-09-03' && $today <= '2019-09-10') {
      $faseAtual = 2;
    } elseif($today >= '2019-09-11' && $today <= '2019-11-06') {
      $faseAtual = 3;
    } elseif($today >= '2019-11-07' && $today <= '2019-11-08') {
      $faseAtual = 4;
    } elseif($today >= '2019-11-09') {
      $faseAtual = 5;
    } 

    return view('common.cronograma', [
      'faseAtual' => $faseAtual
    ]);
  }

  public function avaliadores(){
    return view('common.avaliadores');
  }

  public function comoFunciona()
  {
    return view('common.como-funciona', [
      'regulations_file_path' => App::getLocale() == 'en' ? env('REGULAMENTO_PDF_EN') : env('REGULAMENTO_PDF')
    ]);
  }

  public function apresentacao()
  {
    return view('common.apresentacao');
  }
}
