<?php

namespace RM\Http\Controllers;

use Auth;
use Session;
use RM\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function __construct()
    {
      $this->middleware(['auth', 'user_consent']);
    }

    public function listagem() {
      return view('blog.listagem', [
        'list' => Blog::ordenado()->get()
      ]);
    }

    public function detalhe($id) {
      return view('blog.detalhe', [
        'post' => Blog::findOrFail($id)
      ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('blog.index', [
        'list' => Blog::ordenado()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $post = new Blog;
            $post->fill($request->only([
                'titulo',
                'embed',
                'texto',
                'titulo_caso_clinico',
                'texto_caso_clinico'
            ]))->save();
            $request->session()->flash('sucesso', __('Post criado com sucesso!'));
            return redirect('blog');
        } catch(Exception $e) {
            $request->flash();

            $erro = logar_erro($e, Auth::user()->id);
            return back()->withErrors(array(__('Erro ao salvar postagem! (Erro: :err)', ['err' => $erro])));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('blog.edit', [
            'post' => Blog::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $post = Blog::findOrFail($id);
            $post->fill($request->only([
                'titulo',
                'embed',
                'texto',
                'titulo_caso_clinico',
                'texto_caso_clinico'
            ]))->save();
            $request->session()->flash('sucesso', __('Post atualizado com sucesso!'));
            return redirect('blog');
        } catch(Exception $e) {
            $request->flash();

            $erro = logar_erro($e, Auth::user()->id);
            return back()->withErrors(array(__('Erro ao salvar postagem! (Erro: :err)', ['err' => $erro])));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $post = Blog::findOrFail($id);
            $post->delete();
            Session::flash('sucesso', __('Post removido com sucesso!'));
            return redirect('blog');
        } catch(Exception $e) {
            Session::flash();

            $erro = logar_erro($e, Auth::user()->id);
            return back()->withErrors(array(__('Erro ao salvar postagem! (Erro: :err)', ['err' => $erro])));
        }
    }

    public function reorder(Request $request) {
        $itens = $request->data;
        
        for ($i = 0; $i < count($itens); $i++) {
            Blog::where('id', $itens[$i])->update([
                'ordem' => $i
            ]);
        }
    }
}
