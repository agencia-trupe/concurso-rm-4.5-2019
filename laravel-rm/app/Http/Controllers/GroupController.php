<?php

namespace RM\Http\Controllers;

use Illuminate\Http\Request;
use RM\Models\User;
use Auth;

class GroupController extends Controller
{

    public function __construct()
    {
      $this->middleware(['auth', 'auth.ativos', 'user_consent', 'envio_casos']);
    }

    public function get() {
        $list = User::group(Auth::user()->grupo, Auth::user()->id)->get();
        return view('group.list', ['list' => $list]);
    }
}
