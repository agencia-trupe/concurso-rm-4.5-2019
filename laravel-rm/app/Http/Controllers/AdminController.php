<?php

namespace RM\Http\Controllers;

use DB;
use Mail;
use Auth;
use Notification;
use Carbon\Carbon as Carbon;
use RM\Notifications\UsuarioCriado;
use RM\Notifications\CasoRemovido;
use RM\Mail\CasoCompletoDistribuido;
use RM\Mail\ExtratoFarmacovigilancia;

use RM\Models\Caso;
use RM\Models\User;
use RM\Models\Avaliacao;
use Illuminate\Http\Request;

class AdminController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth']);
  }

  public function novosCasos()
  {
    $novosCat1 = Caso::categoria('1')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();
    $novosCat2 = Caso::categoria('2')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();
    $novosCat3 = Caso::categoria('3')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();
    $novosCat4 = Caso::categoria('4')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();

    return view('admin.novos-casos', [
      'novosCat1' => $novosCat1,
      'novosCat2' => $novosCat2,
      'novosCat3' => $novosCat3,
      'novosCat4' => $novosCat4
    ]);
  }

  public function downloadCaso($caso_codigo)
  {
    $caso = Caso::findByCodigoOrFail($caso_codigo);
    $path = env('SITE_ARQUIVOS_DIR').$caso->arquivo;
    return response()->download($path);
  }

  private function selecionarAvaliadores($caso)
  {
    return User::avaliadores()
               ->filtrarLocal($caso->coordenador->cidade)
			         ->ordenarPorMenosAvaliacoes()
               ->get();
  }

  public function iniciarUsuarios(Request $request)
  {
    $usuarios = User::naoIniciados()->get();

    $body = "";

    if(count($usuarios) > 0){

      foreach($usuarios as $i => $u){
          try {

            $token = str_random(32);


            $u->email_enviado_em = Date('Y-m-d H:i:s');
            $u->token_criacao_senha = $token;
            $u->notify( new UsuarioCriado($token, $u->email));
            $u->save();

            $body .= "<li>";
            $body .= __('E-mail enviado para: :nome (:email)', ['nome' => $u->nome, 'email' => $u->email]);
            $body .= "</li>";

          } catch (\Exception $e) {
              $body .= "<li class='erro'>";
              $body .= __('Erro ao enviar. (:email) :err', ['email' => $u->email, 'err' => $e->getMessage()]);
              $body .= "</li>";
          }
      }

    }else{
      $body .= '<li>';
      $body = __('Todos os e-mails iniciais para definição de senha já foram enviados.');
      $body .= '</li>';
    }

    return view('admin.envios', ['body' => $body]);
  }

  public function distribuirCaso(Request $request, $caso_codigo)
  {
    try {

      $caso = Caso::findByCodigoOrFail($caso_codigo);

      if ($caso->isDistribuido) {
        return back();
      }

      $arquivo = env('SITE_ARQUIVOS_DIR').$caso->arquivo;

      $avaliadores = $this->selecionarAvaliadores($caso);

      if(count($avaliadores) < 3) {
        return back()->withErrors(array('Não foi possível distribuir o caso clínico. (Erro: Total de avaliadores insuficiente (3), selecionados: :count)', ['count' => count($avaliadores)]));
      }

      foreach ($avaliadores as $avaliador) {

        // Gerar 1 registro de Avaliacao para cada avaliador
        $avaliacao = new Avaliacao(['casos_id' => $caso->id]);
        $avaliador->avaliacoes()->save($avaliacao);

        // Notificar cada um dos avaliadores (com o arquivo anexo)
        Mail::to($avaliador->email)->send(new CasoCompletoDistribuido($caso));
      }

      $caso->distribuido_em = date('Y-m-d H:i:s');
      $caso->arquivo = null;
      $caso->save();

      $request->session()->flash('sucesso', __('Caso clínico distribuído para os Avaliadores com sucesso.'));

      return back();

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Não foi possível distribuir o caso clínico. (Erro: :err)', ['err' => $erro])));

    }
  }

  public function excluirCaso(Request $request, $caso_codigo)
  {
    try {

      $caso = Caso::findByCodigoOrFail($caso_codigo);

      $caso->codigo = null;
      $caso->autor = null;
      $caso->coautor_1 = null;
      $caso->coautor_2 = null;
      $caso->coautor_3 = null;
      $caso->coautor_4 = null;
      $caso->coautor_5 = null;
      $caso->enviado_em = null;
      $caso->titulo_pt = null;
      $caso->titulo_en = null;
      $caso->sexo_paciente = null;
      $caso->idade_paciente = null;
      $caso->doenca_diagnosticada = null;
      $caso->relato_pt = null;
      $caso->relato_en = null;
      $caso->discussao_pt = null;
      $caso->discussao_en = null;
      $caso->evolucao = null;
      $caso->referencias = null;
      $caso->status_preenchimento = null;

      $caso->arquivo = null;

      $caso->excluido_em = date('Y-m-d H:i:s');
      $caso->save();

      // Notificar Coordenador que Caso foi Removido
      $caso->coordenador->notify(new CasoRemovido($caso, $caso->coordenador->email));

      $request->session()->flash('sucesso', __('Caso clínico removido com sucesso.'));

      return back();

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Não foi possível remover o caso clínico. (Erro: :err)', ['err' => $erro])));
    }
  }

  public function liberarCaso(Request $request, $caso_codigo) {
    try {

      $caso = Caso::findByCodigoOrFail($caso_codigo);

      $caso->status_preenchimento = 'em_andamento';
      $caso->enviado_em = null;
      $caso->save();

      $request->session()->flash('sucesso', __('Caso clínico liberado com sucesso.'));

      return back();

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Não foi possível liberado o caso clínico. (Erro: :err)', ['err' => $erro])));
    }
  }

  public function exportarPdfCaso(Request $request, $caso_codigo)
  {
    try {

      $caso = Caso::findByCodigoOrFail($caso_codigo);
      $cabecalho = $request->has('cabecalho');

      $dompdf = new \Dompdf\Dompdf();
      $dompdf->loadHtml(view('vendor.pdf.case-excerpt', [
        'caso'      => $caso,
        'cabecalho' => $cabecalho
      ])->render());
      $dompdf->render();
      $dompdf->stream($caso_codigo.($cabecalho ? '-cabecalho' : '').'.pdf');

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array(__('Não foi possível exportar o pdf do caso clínico. (Erro: :err)', ['err' => $erro])));

    }
  }

  public function statusEnvios()
  {
    $coordenadores = User::coordenadores()->orderBy('nome', 'asc')->get();
    return view('admin.status-envios', compact('coordenadores'));
  }

  public function historico(){
    $historicoCat1 = Caso::categoria(1)->distribuidos()->get();
    $historicoCat2 = Caso::categoria(2)->distribuidos()->get();
    $historicoCat3 = Caso::categoria(3)->distribuidos()->get();
    $historicoCat4 = Caso::categoria(4)->distribuidos()->get();
    $avaliadores = User::avaliadores()->orderBy('id', 'asc')->get();

    return view('admin.historico',[
      'historicoCat1' => $historicoCat1,
      'historicoCat2' => $historicoCat2,
      'historicoCat3' => $historicoCat3,
      'historicoCat4' => $historicoCat4,
      'avaliadores' => $avaliadores
    ]);
  }

  public function ranking(){

    $rankingCat1 = DB::table('avaliacoes')
                     ->select('casos.*', 'usuarios.grupo as grupo', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                     ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                     ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                     ->whereNotNull('avaliacoes.media')
                     ->where('categoria', 1)
                     ->groupBy('avaliacoes.casos_id')
                     ->orderBy('ranking', 'DESC')
                     ->get();

    $rankingCat2 = DB::table('avaliacoes')
                    ->select('casos.*', 'usuarios.grupo as grupo', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                    ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                    ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                    ->whereNotNull('avaliacoes.media')
                    ->where('categoria', 2)
                    ->groupBy('avaliacoes.casos_id')
                    ->orderBy('ranking', 'DESC')
                    ->get();

    $rankingCat3 = DB::table('avaliacoes')
                    ->select('casos.*', 'usuarios.grupo as grupo', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                    ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                    ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                    ->whereNotNull('avaliacoes.media')
                    ->where('categoria', 3)
                    ->groupBy('avaliacoes.casos_id')
                    ->orderBy('ranking', 'DESC')
                    ->get();

    $rankingCat4 = DB::table('avaliacoes')
                    ->select('casos.*', 'usuarios.grupo as grupo', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                    ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                    ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                    ->whereNotNull('avaliacoes.media')
                    ->where('categoria', 4)
                    ->groupBy('avaliacoes.casos_id')
                    ->orderBy('ranking', 'DESC')
                    ->get();

    $datamax = Carbon::createFromFormat('d/m/Y', env('SITE_PUBLICACAO_RANKING'));
    $hoje = Carbon::now();

    return view('admin.ranking', [
      'rankingCat1' => $rankingCat1,
      'rankingCat2' => $rankingCat2,
      'rankingCat3' => $rankingCat3,
      'rankingCat4' => $rankingCat4,
      'publicarRanking' => $hoje->gte($datamax)
    ]);
  }

  public function usuarios(){
    $usuarios = User::orderBy('nome', 'asc')->get();
    return view('admin.usuarios', compact('usuarios'));
  }

  public function ativarUsuario(Request $request, $email){
    $token = str_random(32);

    $usuario = User::where('email', $email)->firstOrFail();
    $usuario->email_enviado_em = Date('Y-m-d H:i:s');
    $usuario->token_criacao_senha = $token;
    $usuario->notify( new UsuarioCriado($token, $usuario->email));
    $usuario->save();

    $request->session()->flash('sucesso', __('E-mail enviado com sucesso'));

    return back();
  }

  public function logAdesaoAosTermos(Request $request) {
    return view('admin.log-adesao', [
      'usuarios' => User::withConsent()->get()
    ]);
  }

  public function encaminharFarmaco(Request $request) {
    $data = $request->data;

    $casos = Caso::porDia($data)->get();

    if(count($casos) > 0) {
      $this->notificarFarmacovigilancia($casos, $data);
      $request->session()->flash('sucesso', __('E-mail de casos do dia :d enviados com sucesso', ['d' => $data]));
    } else {
      $request->session()->flash('sucesso', __('Nenhum caso recebido no dia'));
    }
    return back();
  }

  private function notificarFarmacovigilancia($casos, $data) {
    $msg = "RELATÓRIO DE CASOS CLÍNICOS RECEBIDOS<br>";
    $msg .= "DATA: {$data}<br><br>";

    foreach($casos as $caso) {
      $msg .= "Código do concurso: {$caso->codigo}<br>";
      $msg .= "Nome do concurso: RM 4,5 2019<br>";
      $msg .= "Nome do médico relator: {$caso->coordenador->nome}<br>";
      $msg .= "E-mail do médico relator: {$caso->coordenador->email}<br>";
      $msg .= "Data do envio: {$data}<br>";
      $msg .= "<br><br>";
    }

    Mail::to(env('EMAIL_FARMACOVIGILANCIA'))->send(new ExtratoFarmacovigilancia($msg));
  }

  function caseData($case_code) {
    return Caso::findByCodigoOrFail($case_code);
  }
}
