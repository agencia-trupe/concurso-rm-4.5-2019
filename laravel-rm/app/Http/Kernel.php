<?php

namespace RM\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \RM\Http\Middleware\TrimStrings::class,        
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \RM\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \RM\Http\Middleware\Locale::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \RM\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.ativos' => \RM\Http\Middleware\AuthAtivos::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \RM\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'acesso_padrao' => \RM\Http\Middleware\CheckAcessoPadrao::class,
        'acesso_medico_coordenador' => \RM\Http\Middleware\CheckMedicoCoordenador::class,
        'acesso_avaliador' => \RM\Http\Middleware\CheckAvaliador::class,
        'acesso_admin' => \RM\Http\Middleware\CheckAdmin::class,
        'user_consent' => \RM\Http\Middleware\UserConsent::class,
        'fully_authorized_group' => \RM\Http\Middleware\FullyAuthorizedGroup::class,
        'acesso_medico' => \RM\Http\Middleware\CheckMedico::class,
        'acesso_medico_naoparticipante' => \RM\Http\Middleware\CheckMedicoNaoParticipante::class,
        'envio_casos' => \RM\Http\Middleware\EnvioCasos::class,
        'acesso_editor' => \RM\Http\Middleware\CheckEditor::class,
    ];
}
