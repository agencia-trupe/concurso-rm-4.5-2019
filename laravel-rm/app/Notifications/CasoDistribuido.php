<?php

namespace RM\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CasoDistribuido extends Notification
{
    use Queueable;

    protected $caso;
    protected $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $email)
    {
        $this->caso = $caso;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $link = url(config('app.url').route('avaliacoes', [
        'login' => $this->email
      ], false));

      return (new MailMessage)
                    ->subject('Concurso RM4,5 - Novo caso clínico para avaliação')
                    ->level('success')
                    ->greeting('Novo caso clínico para avaliação')
                    ->line('Um novo arquivo foi recebido para avaliação no Concurso RM4,5 de Casos Clínicos.')
                    ->line('Código do Caso: '.$this->caso->codigo)
                    ->action('Avaliar Caso', $link)
                    ->line('Acesse o sistema para avaliar este caso.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
