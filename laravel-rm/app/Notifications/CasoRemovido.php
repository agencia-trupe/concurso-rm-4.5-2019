<?php

namespace RM\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CasoRemovido extends Notification
{
    use Queueable;

    protected $caso;
    protected $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $email)
    {
        $this->caso = $caso;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $link = url(config('app.url').route('submeter-caso', [
        'login' => $this->email
      ], false));

      return (new MailMessage)
                  ->subject('Programa RM4.5 - Caso clínico removido')
                  ->level('error')
                  ->greeting('Caso clínico removido')
                  ->line('Seu arquivo foi rejeitado pela Administração do Programa RM4.5 de Casos Clínicos.')
                  ->action('Reenviar Caso', $link)
                  ->line('Acesse o sistema para reenviar este caso para o Programa.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
