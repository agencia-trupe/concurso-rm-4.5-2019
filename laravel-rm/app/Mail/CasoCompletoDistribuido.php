<?php

namespace RM\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CasoCompletoDistribuido extends Mailable
{
    use Queueable, SerializesModels;

    protected $caso;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($caso)
    {
        $this->caso = $caso;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('vendor.notifications.case-excerpt', [
        'data' => $this->caso->enviado_em->format('d/m/Y'),
        'categoria_titulo' => 'CATEGORIA '.$this->caso->categoria.' '.__('rm.CATEGORIA_'.$this->caso->categoria.'_TITULO'),
        'categoria_texto' => __('rm.CATEGORIA_'.$this->caso->categoria.'_TEXTO'),
        'label1' => 'TÍTULO DO RELATO DO CASO CLÍNICO - PORTUGUÊS:',
        'value1' => $this->caso->titulo_pt,
        'label2' => 'TÍTULO DO RELATO DO CASO CLÍNICO - INGLÊS:',
        'value2' => $this->caso->titulo_en,
        'sexo' => $this->caso->sexo_paciente,
        'idade' => $this->caso->idade_paciente,
        'doenca' => 'Leucemia Mieloide Crônica (LMC) com resposta RM 4.5',
        'label3' => 'RELATO DE CASO CLÍNICO - PORTUGUÊS:',
        'value3' => $this->caso->relato_pt,
        'label4' => 'RELATO DE CASO CLÍNICO - INGLÊS:',
        'value4' => $this->caso->relato_en,
        'label5' => 'DISCUSSÃO E CONCLUSÃO - PORTUGUÊS:',
        'value5' => $this->caso->discussao_pt,
        'label6' => 'DISCUSSÃO E CONCLUSÃO - INGLÊS:',
        'value6' => $this->caso->discussao_en,
        'label7' => 'INDIQUE A EVOLUÇÃO DE EXAMES LABORATORIAIS OU OUTROS EVENTOS QUE ACHAR RELEVANTES (OPCIONAL):',
        'value7' => $this->caso->evolucao,
        'label8' => 'REFERÊNCIAS COMPLETAS:',
        'value8' => $this->caso->referencias
      ])
      ->subject('Programa RM4.5 - Novo caso clínico: '.$this->caso->codigo);
    }
}
