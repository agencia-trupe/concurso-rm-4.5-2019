<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('casos_id')->unsigned();
            $table->foreign('casos_id')->references('id')->on('casos')->onDelete('cascade');

            $table->integer('avaliador_id')->unsigned();
            $table->foreign('avaliador_id')->references('id')->on('usuarios')->onDelete('cascade');

            $table->decimal('criterio_1', 3, 1)->nullable();
            $table->decimal('criterio_2', 3, 1)->nullable();
            $table->decimal('criterio_3', 3, 1)->nullable();
            $table->decimal('criterio_4', 3, 1)->nullable();
            $table->decimal('criterio_5', 3, 1)->nullable();
            $table->decimal('media', 3, 1)->nullable();

            $table->datetime('avaliado_em')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacoes');
    }
}
