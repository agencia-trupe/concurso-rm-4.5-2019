<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('coordenador_id')->unsigned();
            $table->foreign('coordenador_id')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('categoria'); // 1, 2, 3
            $table->string('codigo')->nullable(); // gerado pela aplicação
            $table->string('arquivo')->nullable();
            $table->string('autor')->nullable();
            $table->string('coautor_1')->nullable();
            $table->string('coautor_2')->nullable();
            $table->string('coautor_3')->nullable();
            $table->string('coautor_4')->nullable();
            $table->string('coautor_5')->nullable();
            $table->string('coautor_6')->nullable();
            $table->string('coautor_7')->nullable();
            $table->string('status_preenchimento')->default('a_preencher'); // a_preencher | em_andamento | preenchido | enviado
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('sexo_paciente');
            $table->string('idade_paciente');
            $table->string('doenca_diagnosticada');
            $table->text('relato_pt');
            $table->text('relato_en');
            $table->text('discussao_pt');
            $table->text('discussao_en');
            $table->text('evolucao')->nullable();
            $table->text('referencias');
            $table->datetime('preenchido_em')->nullable();
            $table->datetime('enviado_em')->nullable();
            $table->datetime('distribuido_em')->nullable();
            $table->datetime('excluido_em')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos');
    }
}
