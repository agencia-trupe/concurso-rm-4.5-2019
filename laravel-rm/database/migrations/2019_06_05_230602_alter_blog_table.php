<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog', function (Blueprint $table) {
            $table->string('titulo')->nullable()->change();
            $table->string('titulo_caso_clinico')->nullable()->after('texto');
            $table->text('texto_caso_clinico')->nullable()->after('titulo_caso_clinico');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog', function (Blueprint $table) {
            $table->dropColumn('texto_caso_clinico');
            $table->dropColumn('titulo_caso_clinico');
        });
    }
}
