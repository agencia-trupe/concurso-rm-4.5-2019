@extends('template.index')


@section('conteudo')
<div id="app-vue">
    <div class="conteudo conteudo-submeter-caso preencher-form">
        <div class="centralizar">
            <h1>{{__('rm.FORMULARIO_DE_SUBMISSAO')}}</h1>

            @if($errors->preencher->any())
                <p class="alerta alerta-erro" style='margin-top: 15px; text-align: center;'>
                    {{$errors->preencher->first()}}
                </p>
            @endif

            <div class="colunas-info">
                <div class="coluna-info">
                    <p>
                        {{__('Preencha todos os campos abaixo e faça o envio através do website. Nenhum outro arquivo além deste deverá ser enviado. O envio pressupõe o seu aceite integral a todos os itens do regulamento. A indicação no caso relatado deve ser de acordo com a bula de nilotinibe. Ao enviar o caso, o autor principal garante que o paciente assinou consentimento para uso de dados e imagens para esse fim.')}}
                    </p>
                    <p>
                        <strong>{{__('Lembre-se: Não mencione o seu nome, o nome da sua instituição ou de profissionais em qualquer um dos campos.')}}</strong>
                    </p>
                </div>

                <div class="coluna-categoria">
                    <h2>{{__('rm.CATEGORIA')}} {{$nro_categoria}}</h2>
                    <h3>{{__('rm.CATEGORIA_'.$nro_categoria.'_TITULO')}}</h3>
                    <p>{{__('rm.CATEGORIA_'.$nro_categoria.'_TEXTO')}}</p>
                </div>
            </div>

            <form action="preencher-caso" class="form-caso" method="post" novalidate>
                {!!csrf_field()!!}
                <input type="hidden" name="categoria" value="{{$caso->categoria}}">
                <div class="input">
                    <label for="titulo_pt">{{__('rm.TITULO_RELATO_PT')}}</label>
                    <input type="text" name="titulo_pt" id="titulo_pt" required maxlength="255" value="{{old('titulo_pt', $caso->titulo_pt)}}" @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif >
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '255'])}}</span>
                </div>
                <div class="input">
                    <label for="titulo_en">{{__('rm.TITULO_RELATO_EN')}}</label>
                    <input type="text" name="titulo_en" id="titulo_en" required maxlength="255" value="{{old('titulo_pt', $caso->titulo_en)}}" @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif >
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '255'])}}</span>
                </div>
                <div class="box-paciente">
                    <h2>{{__('rm.DADOS_PACIENTE')}}</h2>
                    <div class="input">
                        <h3>{{__('rm.SEXO')}}</h3>
                        <div class="radio-buttons">
                            <label><input type="radio" name="sexo_paciente" value="masculino" @if($caso->sexo_paciente == 'masculino' || old('sexo_paciente') == 'masculino') checked @endif required @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif > {{__('rm.MASCULINO')}}</label>
                            <label><input type="radio" name="sexo_paciente" value="feminino" @if($caso->sexo_paciente == 'feminino' || old('sexo_paciente') == 'feminino') checked @endif required @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif > {{__('rm.FEMININO')}}</label>
                        </div>
                    </div>
                    <div class="input">
                        <label for="idade_paciente">{{__('rm.IDADE')}}</label>
                        <input type="text" name="idade_paciente" id="idade_paciente" required maxlength="3" value="{{old('idade_paciente', $caso->idade_paciente)}}" @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif >
                        <span class="right-label">{{__('rm.ANOS')}} <small>{{__('rm.IDADE_MAIOR_18')}}</small></span>
                    </div>
                    <div class="input">
                        <label>{{__('rm.DOENCA_DIAGNOSTICADA')}}</label>
                        <div class="radio-buttons">
                            <label><input type="checkbox" name="doenca_diagnosticada" required @if($caso->doenca_diagnosticada == 'leucemia_mieloide_cronica' || old('doenca_diagnosticada') == 'leucemia_mieloide_cronica') checked @endif  @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado') disabled @endif value="leucemia_mieloide_cronica"> {{__('rm.LEUCEMIA')}}</label>
                        </div>
                    </div>
                </div>
                <div class="input">
                    <label for="relato_pt">{{__('rm.RELATO_PT')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-relato_pt">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('relato_pt', $caso->relato_pt)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="8000">{!!old('relato_pt', $caso->relato_pt)!!}</div>
                        @endif
                        <input type="hidden" name="relato_pt" id="input-relato_pt">
                    </div>
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '8000'])}}</span>
                </div>
                <div class="input">
                    <label for="relato_en">{{__('rm.RELATO_EN')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-relato_en">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('relato_en', $caso->relato_en)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="8000">{!!old('relato_en', $caso->relato_en)!!}</div>
                        @endif
                        <input type="hidden" name="relato_en" id="input-relato_en">
                    </div>
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '8000'])}}</span>
                </div>
                <div class="input">
                    <label for="discussao_pt">{{__('rm.DISCUSSAO_PT')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-discussao_pt">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('discussao_pt', $caso->discussao_pt)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="5500">{!!old('discussao_pt', $caso->discussao_pt)!!}</div>
                        @endif
                        <input type="hidden" name="discussao_pt" id="input-discussao_pt">
                    </div>
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '5500'])}}</span>
                </div>
                <div class="input">
                    <label for="discussao_en">{{__('rm.DISCUSSAO_EN')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-discussao_en">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('discussao_en', $caso->discussao_en)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="5500">{!!old('discussao_en', $caso->discussao_en)!!}</div>
                        @endif
                        <input type="hidden" name="discussao_en" id="input-discussao_en">
                    </div>
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '5500'])}}</span>
                </div>
                <div class="input">
                    <label for="evolucao">{{__('rm.EVOLUCAO')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-evolucao">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('evolucao', $caso->evolucao)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="0">{!!old('evolucao', $caso->evolucao)!!}</div>
                        @endif
                        <input type="hidden" name="evolucao" id="input-evolucao">
                    </div>
                    <span class="right-label">{{__('rm.OPCIONAL')}}</span>
                </div>
                <div class="input">
                    <label for="referencias">{{__('rm.REFERENCIAS')}}</label>
                    <div class="img-editor-wrapper" id="wrapper-referencias">
                        @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                            <div class="readonly-editor">{!!old('referencias', $caso->referencias)!!}</div>
                        @else
                            <div class="img-editor" data-maxlength="5500">{!!old('referencias', $caso->referencias)!!}</div>
                        @endif
                        <input type="hidden" name="referencias" id="input-referencias">
                    </div>
                    <span class="right-label">{{__('rm.MAX_CARACTERES', ['n' => '5500'])}}</span>
                </div>
                <div class="copyright">
                    <p>
                        {{__('Material produzido em março de 2019. Material destinado a médicos participantes do Programa de Educação Médica RM 4,5 de casos clínicos 2019.')}}
                    </p>
                    <p>
                        <strong>{{__('TS RESUMO PROGRAMA DE EDUCAÇÃO MÉDICA 0,25 0118 BR-04615')}}</strong>
                    </p>
                </div>
                <div class="botoes">
                    @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                        <input type="submit" value="{{__('rm.SALVAR_PARCIALMENTE')}}" class="desabilitado" onclick="return false;">
                    @else
                        <input type="submit" value="{{__('rm.SALVAR_PARCIALMENTE')}}">
                    @endif
                    
                    @if($caso->status_preenchimento == 'preenchido' || $caso->status_preenchimento == 'enviado')
                        <input type="submit" value="{{__('rm.SALVAR_E_FINALIZAR')}}" class="desabilitado" onclick="return false;">
                    @else
                        <input type="submit" value="{{__('rm.SALVAR_E_FINALIZAR')}}" formaction="preencher-caso-e-finalizar">
                    @endif
                    

                    @if($caso->status_preenchimento == 'preenchido')
                        <a href="#" @click.prevent="mostrarForm('{{$caso->categoria}}', '{{!$caso->isEnviado && $caso->podeSerEnviado ? 1 : 0}}', '{{$caso->status_preenchimento}}')">
                            <img src="images/icone-enviar.png" alt="">{{__('rm.ENVIAR_CASO_CLINICO')}}
                        </a>
                    @elseif($caso->status_preenchimento == 'enviado')
                        <a href="#" onclick="return false;">
                            {{__('rm.ENVIADO_EM')}} {{$caso->enviado_em->format('d/m/Y H:i:s')}}
                        </a>
                    @else
                        <a href="#" onclick="return false;" class="desativado" title="{{__('rm.NECESSARIO_FINALIZAR_ANTES_DE_ENVIAR')}}">
                            <img src="images/icone-enviar.png" alt="">{{__('rm.ENVIAR_CASO_CLINICO')}}
                        </a>
                    @endif                    
                </div>
            </form>
        </div>
    </div>
    <transition name="fade">
        <div class="modal" v-show="form.mostrar" v-cloak>
            <div class="backdrop" @click.stop="fecharModal"></div>
            <div class="modal-conteudo">
                <form class="formCaso" action="{{route('submeter-caso')}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
            
                    @if($errors->submeter->any())
                        <p class="alerta alerta-erro" id="retorno-submissao">
                        {{$errors->submeter->first()}}
                        </p>
                    @endif
            
                    <input type="hidden" name="_categoria" :value="form.categoria">
                    <input type="hidden" name="_grupo" value="{{$coautores['coordenador']}}">
                    <input type="hidden" name="_categoria_old" value="{{old('_categoria')}}">
                    
                    <select name="autor_principal" required>
                        <option value="">{{__('rm.AUTOR_PRINCIPAL')}}</option>
                        @if(isset($coautores['coordenador'])) <option value="{{$coautores['coordenador']->nome}}">{{$coautores['coordenador']->nome}}</option> @endif
                        @if(isset($coautores['medicos'][0])) <option value="{{$coautores['medicos'][0]->nome}}">{{$coautores['medicos'][0]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][1])) <option value="{{$coautores['medicos'][1]->nome}}">{{$coautores['medicos'][1]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][2])) <option value="{{$coautores['medicos'][2]->nome}}">{{$coautores['medicos'][2]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][3])) <option value="{{$coautores['medicos'][3]->nome}}">{{$coautores['medicos'][3]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][4])) <option value="{{$coautores['medicos'][4]->nome}}">{{$coautores['medicos'][4]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][5])) <option value="{{$coautores['medicos'][5]->nome}}">{{$coautores['medicos'][5]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][6])) <option value="{{$coautores['medicos'][6]->nome}}">{{$coautores['medicos'][6]->nome}}</option> @endif
                        @if(isset($coautores['medicos'][7])) <option value="{{$coautores['medicos'][7]->nome}}">{{$coautores['medicos'][7]->nome}}</option> @endif
                    </select>
                    
                    <p>
                        {{__('Garantir que o autor principal do relato de caso seja o médico que acompanha o paciente. O autor principal deve ser Hematologista/Oncologista. Em caso de mais de um médico acompanhando o paciente, um deles deve ser apontado como autor principal.')}}
                    </p>

                    <h2>{{__('rm.INTEGRANTES_DO_GRUPO')}}:</h2>
                    
                    <p class="lista-medicos">
                        @if(isset($coautores['coordenador'])) {{$coautores['coordenador']->nome}} - {{$coautores['coordenador']->email}}<br> @endif
                        @if(isset($coautores['medicos'][0])) {{$coautores['medicos'][0]->nome}} - {{$coautores['medicos'][0]->email}}<br> @endif
                        @if(isset($coautores['medicos'][1])) {{$coautores['medicos'][1]->nome}} - {{$coautores['medicos'][1]->email}}<br> @endif
                        @if(isset($coautores['medicos'][2])) {{$coautores['medicos'][2]->nome}} - {{$coautores['medicos'][2]->email}}<br> @endif
                        @if(isset($coautores['medicos'][3])) {{$coautores['medicos'][3]->nome}} - {{$coautores['medicos'][3]->email}}<br> @endif
                        @if(isset($coautores['medicos'][4])) {{$coautores['medicos'][4]->nome}} - {{$coautores['medicos'][4]->email}}<br> @endif
                        @if(isset($coautores['medicos'][5])) {{$coautores['medicos'][5]->nome}} - {{$coautores['medicos'][5]->email}}<br> @endif
                        @if(isset($coautores['medicos'][6])) {{$coautores['medicos'][6]->nome}} - {{$coautores['medicos'][6]->email}}<br> @endif
                        @if(isset($coautores['medicos'][7])) {{$coautores['medicos'][7]->nome}} - {{$coautores['medicos'][7]->email}}<br> @endif
                    </p>

                    <p class="aviso-grupo">
                        {{__('Seu grupo é formado pelos integrantes acima. Caso exista qualquer divergência, por favor, contate imediatamente a organização do Programa via tel: :tel ou :email', ['tel' => 'TELEFONE', 'email' => 'EMAIL'])}}
                    </p>

                    <input type="submit" value="{{__('rm.ENVIAR')}}" @click.once="">
                </form>
                </div>
            </div>
        </div>
    </transition>
@endsection