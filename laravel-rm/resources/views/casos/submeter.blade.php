@extends('template.index')

@section('conteudo')
<div class="conteudo conteudo-submeter-caso com-recuoo">
  <div class="centralizar">

    @if(session('sucesso'))
      <p class="alerta alerta-sucesso auto-close">
        {{session('sucesso')}}
      </p>
    @endif
    
    <p>
      <strong>
        {!!__('ORIENTAÇÕES PARA SUBMISSÃO')!!}
      </strong>
    </p>
    
    <ul>
      <li>
        <p>
          {!!__('Deve-se preservar a identidade do médico e do Serviço/Hospital onde o paciente do caso clínico foi atendido, não havendo nenhuma citação ou referência ao nome, local e/ou cidade no título e corpo do texto do relato de caso. É essencial preservar a identidade dos pacientes.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('Não usar o nome ou iniciais do paciente, omitir detalhes que possam identificar as pessoas, caso não sejam essenciais para o relato do caso.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('Necessário ter o consentimento, por escrito, do paciente para que seus dados e imagens sejam utilizados no relato de caso clínico submetido ao Programa RM4.5.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('Informações de pacientes deverão ser anonimizadas como forma de não ser possível a identificação do paciente.')!!}
        <p>
      </li>
      <li>
        <p>
         {!!__('Não serão aceitos relatos de casos clínicos que considerem o uso de nilotinibe para indicações diferentes das de bula no país ou de pacientes que sejam oriundos de estudos clínicos.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('É de responsabilidade dos autores a exatidão das referências bibliográficas utilizadas no trabalho.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('Por conta do volume de trabalhos e a necessidade legal de reporte dos eventos adversos aos órgãos regulatórios nacionais e globais pela área de Farmacovigilância da Novartis, haverá 3 datas limite como prazo de submissão entre os grupos, essa definição ocorrerá através de um sorteio a ser realizado no dia 22/03/19 durante o kick-off com os coordenadores sobre as regras do Programa.')!!}
        <p>
      </li>
      <li>
        <p>
          {!!__('O sorteio considerará o número total de coordenadores que serão divididos em 3 blocos (A,B e C).')!!}
        <p>
      </li>
    </ul>
    </p>

    <p>
      {!!__('Após o sorteio, os prazos serão:')!!}
    </p>
    
    <p>
      {!!__('<strong>Bloco A</strong>: às 23h59 min do dia 03 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.')!!}
    </p>
    <p>
      {!!__('<strong>Bloco B</strong>: às 23h59 min do dia 05 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.')!!}
    </p>
    <p>
      {!!__('<strong>Bloco C</strong>: às 23h59 min do dia 10 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.')!!}
    </p>
    
    <p>
      {!!__('Poderá ser submetido somente 01 (um) relato de caso de cada categoria por grupo de trabalho. O caso clínico deve ser enviado dentro da data limite do grupo que o seu grupo ficou inserido (A, B ou C). Os casos devem ser submetidos em português e inglês. A Novartis disponibilizará uma tradutora especializada para o coordenador do grupo caso seja necessário para o grupo.')!!}
    </p>

    <p style="text-align: center;">
      {!!__('Clique em <strong>ENVIAR CASO CLÍNICO</strong> para preencher o formulário e depois fazer o envio oficial.')!!}
    </p>

    <div class="botoes-envio">

      <div class="box {{!$casoCat1->isEnviado && $casoCat1->podeSerEnviado ? 'ativo' : 'inativo'}}">
        <a href="preencher-caso/1" title="{{__('rm.ENVIAR_CASO_CLINICO')}}" id="mostrarFormCat1">
          <h3>{{__('rm.CATEGORIA')}} 1</h3>
          <h2>{{__('rm.CATEGORIA_1_TITULO')}}</h2>
          <p>
              {{__('rm.CATEGORIA_1_TEXTO')}}
          </p>
          <div class="botao">
            @if($casoCat1->isEnviado)
              <span class="light">
                {{__('rm.CASO_CLINICO_ENVIADO_EM')}}: {{$casoCat1->enviado_em->format('d/m/Y')}}.
              </span>
            @else
              @if($casoCat1->podeSerEnviado)
                <span>
                  <img src="images/icone-enviar.png" alt="Enviar arquivo">
                </span>
                <span>
                  {{__('rm.ENVIAR_CASO_CLINICO')}} <small>{{$casoCat1->statusFront}}</small>
                </span>
              @else
                <span>
                  {{__('rm.PRAZO_MAXIMO_EXPIROU_EM')}} <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                </span>
              @endif
            @endif                
          </div>
        </a>
      </div>

      <div class="box {{!$casoCat2->isEnviado && $casoCat2->podeSerEnviado ? 'ativo' : 'inativo'}}">
        <a href="preencher-caso/2" title="{{__('rm.ENVIAR_CASO_CLINICO')}}" id="mostrarFormCat2">
          <h3>{{__('rm.CATEGORIA')}} 2</h3>
          <h2>{{__('rm.CATEGORIA_2_TITULO')}}</h2>
          <p>
              {{__('rm.CATEGORIA_2_TEXTO')}}
          </p>
          <div class="botao">
            @if($casoCat2->isEnviado)
              <span class="light">
                {{__('rm.CASO_CLINICO_ENVIADO_EM')}}: {{$casoCat2->enviado_em->format('d/m/Y')}}.
              </span>
            @else
              @if($casoCat2->podeSerEnviado)
                <span>
                  <img src="images/icone-enviar.png" alt="Enviar arquivo">
                </span>
                <span>
                  {{__('rm.ENVIAR_CASO_CLINICO')}} <small>{{$casoCat2->statusFront}}</small>
                </span>
              @else
                <span>
                  {{__('rm.PRAZO_MAXIMO_EXPIROU_EM')}} <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                </span>
              @endif
            @endif                
          </div>
        </a>
      </div>

      <div class="box {{!$casoCat3->isEnviado && $casoCat3->podeSerEnviado ? 'ativo' : 'inativo'}}">
          <a href="preencher-caso/3" title="{{__('rm.ENVIAR_CASO_CLINICO')}}" id="mostrarFormCat1">
            <h3>{{__('rm.CATEGORIA')}} 3</h3>
            <h2>{{__('rm.CATEGORIA_3_TITULO')}}</h2>
            <p>
                {{__('rm.CATEGORIA_3_TEXTO')}}
            </p>
            <div class="botao">
              @if($casoCat3->isEnviado)
                <span class="light">
                  {{__('rm.CASO_CLINICO_ENVIADO_EM')}}: {{$casoCat3->enviado_em->format('d/m/Y')}}.
                </span>
              @else
                @if($casoCat3->podeSerEnviado)
                  <span>
                    <img src="images/icone-enviar.png" alt="Enviar arquivo">
                  </span>
                  <span>
                    {{__('rm.ENVIAR_CASO_CLINICO')}} <small>{{$casoCat3->statusFront}}</small>
                  </span>
                @else
                  <span>
                    {{__('rm.PRAZO_MAXIMO_EXPIROU_EM')}} <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </span>
                @endif
              @endif                
            </div>
          </a>
        </div>

        <div class="box {{!$casoCat4->isEnviado && $casoCat4->podeSerEnviado ? 'ativo' : 'inativo'}}">
          <a href="preencher-caso/4" title="{{__('rm.ENVIAR_CASO_CLINICO')}}" id="mostrarFormCat2">
            <h3>{{__('rm.CATEGORIA')}} 4</h3>
            <h2>{{__('rm.CATEGORIA_4_TITULO')}}</h2>
            <p>
                {{__('rm.CATEGORIA_4_TEXTO')}}
            </p>
            <div class="botao">
              @if($casoCat4->isEnviado)
                <span class="light">
                  {{__('rm.CASO_CLINICO_ENVIADO_EM')}}: {{$casoCat4->enviado_em->format('d/m/Y')}}.
                </span>
              @else
                @if($casoCat4->podeSerEnviado)
                  <span>
                    <img src="images/icone-enviar.png" alt="Enviar arquivo">
                  </span>
                  <span>
                    {{__('rm.ENVIAR_CASO_CLINICO')}} <small>{{$casoCat4->statusFront}}</small>
                  </span>
                @else
                  <span>
                    {{__('rm.PRAZO_MAXIMO_EXPIROU_EM')}} <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </span>
                @endif
              @endif                
            </div>
          </a>
        </div>

    </div>

  </div>
</div>
@endsection
