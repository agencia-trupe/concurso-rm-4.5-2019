@extends('template.index')

@section('conteudo')

<div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

        <h1 class="titulo-grupo"><span>{{__('rm.INTEGRANTES_DO_GRUPO')}}</span></h1>

        @if(session('sucesso'))
            <p class="alerta alerta-sucesso auto-close">
                {{session('sucesso')}}
            </p>
        @endif

        @if($list->count() == 0)
            <h2>{{__('rm.NENHUM_MEDICO_NO_GRUPO')}}</h2>
        @else        
            <table class="lista-grupo">
                <tbody>
                    @foreach($list as $coautor)
                        <tr>
                            <td>{{$coautor->nome}}</td>
                            <td>{{$coautor->email}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <p class="aviso-grupo">
                {{__('Seu grupo é formado pelos integrantes acima. Caso exista qualquer divergência, por favor, contate imediatamente a organização do Programa via tel: :tel ou :email', ['tel' => 'TELEFONE', 'email' => 'EMAIL'])}}
            </p>
        @endif
    </div>
</div>

@endsection
