@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento @if(Auth::check()) com-recuoo @endif">
    <div class="centralizar">

      <h2>{{__('rm.PAGINA_NAO_ENCONTRADA')}}</h2>

    </div>
  </div>

@endsection
