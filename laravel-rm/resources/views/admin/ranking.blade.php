@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.RANKING_DE_CASOS_CLINICOS')}}</span></h2>

      @if($publicarRanking)

        <p class="titulo-categoria">
          {{__('rm.CATEGORIA')}} 1
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat1 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="{{__('rm.CLASSIFICACAO/CODIGO')}}">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="{{__('rm.NOME_DO_GRUPO')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="{{__('rm.AUTOR_E_CO-AUTORES')}}">
                  <p>
                    {!!integrantes_grupo($caso->grupo)!!}
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="{{__('rm.NOME_DO_COORDENADOR')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="{{__('rm.TOTAL_DE_PONTOS')}}">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                {{__('rm.NENHUM_CASO_COM_PONTUACAO')}}
              </div>
            </div>

          @endforelse
        </div>

        <p class="titulo-categoria">
          {{__('rm.CATEGORIA')}} 2
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat2 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="{{__('rm.CLASSIFICACAO/CODIGO')}}">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="{{__('rm.NOME_DO_GRUPO')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="{{__('rm.AUTOR_E_CO-AUTORES')}}">
                  <p>
                    {!!integrantes_grupo($caso->grupo)!!}
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="{{__('rm.NOME_DO_COORDENADOR')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="{{__('rm.TOTAL_DE_PONTOS')}}">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                {{__('rm.NENHUM_CASO_COM_PONTUACAO')}}
              </div>
            </div>

          @endforelse
        </div>

        <p class="titulo-categoria">
          {{__('rm.CATEGORIA')}} 3
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat3 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="{{__('rm.CLASSIFICACAO/CODIGO')}}">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="{{__('rm.NOME_DO_GRUPO')}}">
                <span>
                  {{$caso->autor}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="{{__('rm.AUTOR_E_CO-AUTORES')}}">
                  <p>
                    {!!integrantes_grupo($caso->grupo)!!}
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="{{__('rm.NOME_DO_COORDENADOR')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="{{__('rm.TOTAL_DE_PONTOS')}}">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                {{__('rm.NENHUM_CASO_COM_PONTUACAO')}}
              </div>
            </div>

          @endforelse
        </div>

        <p class="titulo-categoria">
          {{__('rm.CATEGORIA')}} 4
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat4 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="{{__('rm.CLASSIFICACAO/CODIGO')}}">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="{{__('rm.NOME_DO_GRUPO')}}">
                <span>
                  {{$caso->autor}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="{{__('rm.AUTOR_E_CO-AUTORES')}}">
                  <p>
                    {!!integrantes_grupo($caso->grupo)!!}
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="{{__('rm.NOME_DO_COORDENADOR')}}">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="{{__('rm.TOTAL_DE_PONTOS')}}">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                {{__('rm.NENHUM_CASO_COM_PONTUACAO')}}
              </div>
            </div>

          @endforelse
        </div>

      @else

        <p class="destaque">
          {{__('rm.RANKING_DISPONIVEL_EM')}} <strong>{{env('SITE_PUBLICACAO_RANKING')}}</strong>.
        </p>

      @endif

    </div>
  </div>

@endsection
