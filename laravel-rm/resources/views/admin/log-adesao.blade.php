@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.LOG_CONSENTIMENTO')}}</span></h2>

      <table class="lista-usuarios">
        <thead>
          <tr>
            <th>{{__('rm.NOME')}}</th>
            <th>{{__('rm.EMAIL')}}</th>
            <th>{{__('rm.TIPO')}}</th>
            <th>{{__('rm.DATA_DE_CADASTRO')}}</th>
            <th>{{__('rm.DATA_ATIVACAO_CONTA')}}</th>
            <th>{{__('rm.DATA_DE_ACEITE')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuarios as $u)
            <tr>
              <td>{{$u->nome}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->tipoExtenso}}</td>
              <td class='status'>
                <strong class="verde">{{$u->created_at->format('d/m/Y H:i:s')}}</strong>
              </td>
              <td class='status'>
                <strong class="verde">{{$u->senha_criada_em->format('d/m/Y H:i:s')}}</strong>
              </td>
              <td class='status'>
                <strong class="verde">{{$u->check_termos_date->format('d/m/Y H:i:s')}}</strong>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>


    </div>
  </div>

@endsection
