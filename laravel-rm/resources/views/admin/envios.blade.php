@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.ENVIO_EMAIL_NOVOS_USUARIOS')}}</span></h2>

      <a href="usuarios" class='botao-padrao'>{{__('rm.VOLTAR')}}</a>
      <a href="iniciar-usuarios" class='botao-padrao'>{{__('rm.REINICIAR_ENVIO')}}</a>

      <ul class="padrao">
        {!! $body !!}
      </ul>
    </div>
  </div>

@endsection
