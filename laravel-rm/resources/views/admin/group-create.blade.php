@extends('template.index')

@section('conteudo')

    <div class="conteudo conteudo-admin com-recuoo">
        <div class="centralizar">

            <h2><span>{{__('rm.GRUPOS')}} - {{$title}}</span></h2>

            @if($errors->any())
                <p class="alerta alerta-erro" style='margin-top: 15px; text-align: center;'>
                    {{$errors->first()}}
                </p>
            @endif
           
            <form action="{{route($destino_form)}}" method="POST" id="add-group-form">
                {!!csrf_field()!!}
                <input type="text" name="nome" required placeholder="{{__('rm.NOME')}} *" value="{{old('nome')}}">
                <input type="text" name="crm" required placeholder="{{__('rm.CRM')}} *" value="{{old('crm')}}">
                <input type="text" name="cidade" placeholder="{{__('rm.CIDADE')}}" value="{{old('cidade')}}">
                <input type="text" name="estado" required placeholder="{{__('rm.ESTADO')}} *" value="{{old('estado')}}" maxlength="2">
                <input type="text" name="telefone" placeholder="{{__('rm.TELEFONE')}}" value="{{old('telefone')}}">
                @if (isset($group_name))
                    <input type="hidden" name="grupo" value="{{$group_name}}">
                @endif
                <input type="email" name="email" required placeholder="{{__('rm.EMAIL')}} *" value="{{old('email')}}">
                <input type="hidden" name="tipo" value="{{$tipo}}">
                <input type="submit" value="{{__('rm.SALVAR')}}">
            </form>

        </div>
    </div>

@endsection