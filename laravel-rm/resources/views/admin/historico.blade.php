@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.HISTORICO_CASOS_NO_SISTEMA')}} &middot; {{__('rm.AVALIACOES_REALIZADAS')}}</span></h2>

      <div class="lista-avaliacoes">

        @forelse($historicoCat1 as $caso)
          <div class="linha com-titulo" data-label="{{__('rm.CATEGORIA')}} 1">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="{{__('rm.AVALIADOR')}} {{$k+1}}" data-nome="{{$avaliador->nome.' - '.$avaliador->cidade}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      {{__('rm.AVALIADO')}} <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">{{__('rm.NAO_ENVIADO')}}</span>
                  @endif

                @else

                  <span class="disabled"><small>{{__('rm.NAO_AVALIA')}}</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty

          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO')}} 1.</div>
          </div>

        @endforelse

      </div>

      <div class="lista-avaliacoes">

        @forelse($historicoCat2 as $caso)
          <div class="linha com-titulo" data-label="{{__('rm.CATEGORIA')}} 2">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="{{__('rm.AVALIADOR')}} {{$k+1}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      {{__('rm.AVALIADO')}} <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">{{__('rm.NAO_ENVIADO')}}</span>
                  @endif

                @else

                  <span class="disabled"><small>{{__('rm.NAO_AVALIA')}}</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty
        
          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO')}} 2.</div>
          </div>

        @endforelse

      </div>
      
      <div class="lista-avaliacoes">
        @forelse($historicoCat3 as $caso)
          <div class="linha com-titulo" data-label="{{__('rm.CATEGORIA')}} 3">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="{{__('rm.AVALIADOR')}} {{$k+1}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      {{__('rm.AVALIADO')}} <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">{{__('rm.NAO_ENVIADO')}}</span>
                  @endif

                @else

                  <span class="disabled"><small>{{__('rm.NAO_AVALIA')}}</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty

          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO')}} 3.</div>
          </div>

        @endforelse
      </div>

      <div class="lista-avaliacoes">
          @forelse($historicoCat4 as $caso)
            <div class="linha com-titulo" data-label="{{__('rm.CATEGORIA')}} 4">
  
              <div class="avaliacao-codigo">
                <span>{{$caso->codigo}}</span>
              </div>
  
              @foreach($avaliadores as $k => $avaliador)
                <div class="avaliacao-status" data-label="{{__('rm.AVALIADOR')}} {{$k+1}}">
  
                  @if(caso_avaliavel_por($caso->id, $avaliador->id))
                    @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                      <span>
                        {{__('rm.AVALIADO')}} <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                      </span>
                    @else
                      <span class="pendente">{{__('rm.NAO_ENVIADO')}}</span>
                    @endif
  
                  @else
  
                    <span class="disabled"><small>{{__('rm.NAO_AVALIA')}}</small></span>
  
                  @endif
  
                </div>
              @endforeach
  
            </div>
          @empty
  
            <div class="linha">
              <div class="nenhum">{{__('rm.NENHUM_CASO')}} 4.</div>
            </div>
  
          @endforelse
        </div>

    </div>
  </div>

@endsection
