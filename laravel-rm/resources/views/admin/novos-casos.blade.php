@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo" id="app-vue-admin">
    <div class="centralizar">

      <h2><span>{{__('rm.NOVOS_CASOS')}}</span></h2>

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      @if($errors->any())
        <p class="alerta alerta-erro">
          {{$errors->first()}}
        </p>
      @endif

      @include('partials.envio-farmacovigilancia')

      <p class="header">
        <span>{{__('rm.CATEGORIA')}} 1</span>
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat1 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="" :disabled="!isLoaded" @click.prevent="mostrarDados('{{$caso->codigo}}')">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data" data-label="{{__('rm.RECEBIMENTO')}}">
              <span data-label="{{__('rm.RECEBIDO_EM')}}">
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.DISTRIBUIR_PARA_AVALIADORES')}}
              </a>
            </div>
            <div class="liberar">
              <a href="liberar-caso/{{$caso->codigo}}" :disabled="!isLoaded">
                {{__('rm.LIBERAR_PARA_EDICAO')}}
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.EXCLUIR')}}
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_A_SER_DISTRIBUIDO')}}</div>
          </div>
        @endforelse
      </div>

      <p class="header">
        <span>{{__('rm.CATEGORIA')}} 2</span>
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat2 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="" :disabled="!isLoaded" @click.prevent="mostrarDados('{{$caso->codigo}}')">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data" data-label="{{__('rm.RECEBIMENTO')}}">
              <span data-label="{{__('rm.RECEBIDO_EM')}}">
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.DISTRIBUIR_PARA_AVALIADORES')}}
              </a>
            </div>
            <div class="liberar">
                <a href="liberar-caso/{{$caso->codigo}}" :disabled="!isLoaded">
                  {{__('rm.LIBERAR_PARA_EDICAO')}}
                </a>
              </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.EXCLUIR')}}
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_A_SER_DISTRIBUIDO')}}</div>
          </div>
        @endforelse
      </div>

      <p class="header">
        <span>{{__('rm.CATEGORIA')}} 3</span>
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat3 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="" :disabled="!isLoaded" @click.prevent="mostrarDados('{{$caso->codigo}}')">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data" data-label="{{__('rm.RECEBIMENTO')}}">
              <span data-label="{{__('rm.RECEBIDO_EM')}}">
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.DISTRIBUIR_PARA_AVALIADORES')}}
              </a>
            </div>
            <div class="liberar">
                <a href="liberar-caso/{{$caso->codigo}}" :disabled="!isLoaded">
                  {{__('rm.LIBERAR_PARA_EDICAO')}}
                </a>
              </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                {{__('rm.EXCLUIR')}}
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_A_SER_DISTRIBUIDO')}}</div>
          </div>
        @endforelse
      </div>

      <p class="header">
          <span>{{__('rm.CATEGORIA')}} 4</span>
        </p>
  
        <div class="linha-loader" v-if="!isLoaded"></div>
        <div class="lista-casos" v-cloak>
          @forelse($novosCat4 as $caso)
            <div class="linha">
              <div class="titulo">
                <a href="" :disabled="!isLoaded" @click.prevent="mostrarDados('{{$caso->codigo}}')">
                  <span>{{$caso->codigo}}</span>
                </a>
              </div>
              <div class="data" data-label="{{__('rm.RECEBIMENTO')}}">
                <span data-label="{{__('rm.RECEBIDO_EM')}}">
                  {{$caso->enviado_em->format('d/m/Y - H:i')}}
                </span>
              </div>
              <div class="distribuir">
                <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                  {{__('rm.DISTRIBUIR_PARA_AVALIADORES')}}
                </a>
              </div>
              <div class="liberar">
                  <a href="liberar-caso/{{$caso->codigo}}" :disabled="!isLoaded">
                    {{__('rm.LIBERAR_PARA_EDICAO')}}
                  </a>
                </div>
              <div class="remover">
                <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                  {{__('rm.EXCLUIR')}}
                </a>
              </div>
            </div>
          @empty
            <div class="linha">
              <div class="nenhum">{{__('rm.NENHUM_CASO_A_SER_DISTRIBUIDO')}}</div>
            </div>
          @endforelse
        </div>

    </div>
    
    <div class="modal" v-show="alerta.mostrar" v-cloak>
      <div class="backdrop" @click.stop="fecharModal"></div>
      <div class="modal-conteudo">
        <div class="confirm-box">
          <div v-if="alerta.tipo == 'distribuir'">
            <p>
              {{__('Confirmar a distribuição do arquivo do caso clínico para os Avaliadores.')}}
            </p>
            <p class="code"><strong>@{{alerta.codigo}}</strong></p>
          </div>
          <div v-if="alerta.tipo == 'excluir'">
            <p>
              {{__('Confirmar a exclusão do arquivo do caso clínico.')}}
            </p>
            <p class="code"><strong>@{{alerta.codigo}}</strong></p>
          </div>
          <div v-if="alerta.tipo == 'dados'">
            <p class="code"><strong>@{{alerta.codigo}}</strong></p>
            <div class="lista-dados">
              <p style="font-weight:bold;text-align:center">
                <a :href="'pdf-caso/' + alerta.codigo" style="color:#F49228">exportar PDF sem cabeçalho</a>
                &middot;
                <a :href="'pdf-caso/' + alerta.codigo + '?cabecalho=true'" style="color:#F49228">exportar PDF com cabeçalho</a>
              </p>

              <hr>

              <p>
                {{__('rm.DATA')}}: @{{alerta.caso.sent_at_f}}
              </p>
              
              <p>
                <strong>
                  @{{alerta.caso.categoria_titulo}}
                </strong>
              </p>

              <p>
                <strong>
                  @{{alerta.caso.categoria_texto}}
                </strong>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.TITULO_RELATO_PT')}}
                </strong>
              </p>
              
              <p>
                @{{alerta.caso.titulo_pt}}
              </p>
              
              <p>
                <strong>
                  {{__('rm.TITULO_RELATO_EN')}}
                </strong>
              </p>
              
              <p>
                @{{alerta.caso.titulo_en}}
              </p>
              
              <hr>
              
              <p>
                <strong>{{__('rm.DADOS_PACIENTE')}}</strong>
              </p>
              <p>
                <strong>{{__('rm.SEXO')}}</strong>: @{{alerta.caso.sexo_paciente}}
              </p>
              <p>
                <strong>{{__('rm.IDADE')}}</strong>: @{{alerta.caso.idade_paciente}}
              </p>
              <p>
                <strong>{{__('rm.DOENCA_DIAGNOSTICADA')}}</strong>: {{__('rm.LEUCEMIA')}}
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.RELATO_PT')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.relato_pt"></span>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.RELATO_EN')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.relato_en"></span>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.DISCUSSAO_PT')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.discussao_pt"></span>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.DISCUSSAO_EN')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.discussao_en"></span>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.EVOLUCAO')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.evolucao"></span>
              </p>
              
              <hr>
              
              <p>
                <strong>
                  {{__('rm.REFERENCIAS')}}
                </strong>
              </p>
              
              <p>
                <span v-html="alerta.caso.referencias"></span>
              </p>
              
              <hr>
              
              <p>
                {{__('Material produzido em março de 2019. Material destinado a médicos participantes do Programa de Educação Médica RM 4,5 de casos clínicos 2019.')}}
              </p>
              <p>
                <strong>{{__('TS RESUMO PROGRAMA DE EDUCAÇÃO MÉDICA 0,25 0118 BR-04615')}}<strong>
              </p>
            </div> 
          </div>
          <div class="controles">
            <a href="#" @click.prevent="fecharModal" class="botao-voltar">{{__('rm.CANCELAR')}}</a>
            <a :disabled="disableAction" @click.prevent.once="goToDestination()" class="botao-envio" v-if="alerta.tipo == 'distribuir'">{{__('rm.CONFIRMAR')}}</a>
            <a :disabled="disableAction" @click.prevent.once="goToDestination()" class="botao-remover" v-if="alerta.tipo == 'excluir'">{{__('rm.EXCLUIR')}}</a>
          </div>
        </div>
      </div>
    </div>
    
  </div>

@endsection
