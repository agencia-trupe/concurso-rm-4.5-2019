@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.STATUS_DE_ENVIO_COORDENADORES')}}</span></h2>

      <div class="lista-coordenadores">
      @foreach($coordenadores as $coord)
        <div class="linha">
          <div class="coordenador-nome" data-label="{{__('rm.COORDENADOR')}}">
            <span>
              {{$coord->nome}}
            </span>
          </div>
          <div class="coordenador-envio" data-label="{{__('rm.CATEGORIA')}} 1">
            <span>
              {!! $coord->StatusEnvioCaso1 !!}
            </span>
          </div>
          <div class="coordenador-envio" data-label="{{__('rm.CATEGORIA')}} 2">
            <span>
              {!! $coord->StatusEnvioCaso2 !!}
            </span>
          </div>
          <div class="coordenador-envio" data-label="{{__('rm.CATEGORIA')}} 3">
            <span>
              {!! $coord->StatusEnvioCaso3 !!}
            </span>
          </div>
          <div class="coordenador-envio" data-label="{{__('rm.CATEGORIA')}} 4">
              <span>
                {!! $coord->StatusEnvioCaso4 !!}
              </span>
            </div>
        </div>
      @endforeach
      </div>

    </div>
  </div>

@endsection
