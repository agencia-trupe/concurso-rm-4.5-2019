@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>{{__('rm.PARTICIPANTES')}}</span></h2>

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      <a href="iniciar-usuarios" class='botao-padrao'>{{__('rm.ENVIAR_EMAIL_INICIAIS')}}</a>

      <table class="lista-usuarios">
        <thead>
          <tr>
            <th>{{__('rm.NOME')}}</th>
            <th>{{__('rm.EMAIL')}}</th>
            <th>{{__('rm.TIPO')}}</th>
            <th>{{__('rm.STATUS')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuarios as $u)
            <tr>
              <td>{{$u->nome}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->tipoExtenso}}</td>
              <td class='status'>
                @if(!is_null($u->senha_criada_em))
                  <strong class="verde">{{__('rm.ATIVADO_EM')}} {{$u->senha_criada_em->format('d/m/Y H:i')}}</strong>
                @elseif(!is_null($u->email_enviado_em))
                  <strong class="vermelho">{{__('rm.INATIVO')}}</strong> &bull; {{__('rm.EMAIL_INICIAL_ENVIADO_EM')}} {{$u->email_enviado_em->format('d/m/Y H:i')}} <br>
                  <a href="ativar-usuario/{{$u->email}}" title="{{__('rm.REENVIAR_EMAIL')}}">{{__('rm.REENVIAR_EMAIL')}}</a>
                  <button activation-link="{{ $u->linkAtivacao }}" class="activation-link-modal-open" title="{{__('rm.COPIAR_LINK_ATIVACAO')}}">{{__('rm.COPIAR_LINK_ATIVACAO')}}</button>
                @else
                  {{__('rm.EMAIL_INICIAL_NAO_ENVIADO')}}<br>
                  <a href="ativar-usuario/{{$u->email}}" title="{{__('rm.ENVIAR_EMAIL_DE_ATIVACAO')}}">{{__('rm.ENVIAR_EMAIL_DE_ATIVACAO')}}</a>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>

  <div id="activation-link-modal">
    <div id="activation-link-backdrop"></div>
    <div id="activation-link-content">
      <h1>{{__('rm.LINK_ATIVACAO')}}</h1>
      <div class="activation-link-modal-body">
        <input type="text" id="activation-link-text" class="activation-link-text" onclick="this.select()" readonly='readonly'>
        <button id="activation-link-btn" data-clipboard-target="#activation-link-text" title="{{__('rm.COPIAR_LINK_ATIVACAO')}}">{{__('rm.COPIAR')}}</button>
      </div>
    </div>
  </div>

@endsection
