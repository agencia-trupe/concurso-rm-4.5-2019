@extends('template.index')

@section('conteudo')

    <div class="conteudo conteudo-admin com-recuoo">
        <div class="centralizar">

            <h2><span>{{__('rm.GRUPOS')}}</span></h2>

            @if(session('sucesso'))
                <p class="alerta alerta-sucesso auto-close">
                    {{session('sucesso')}}
                </p>
            @endif

            @if($errors->any())
                <p class="alerta alerta-erro" id="retorno-submissao">
                    {{$errors->first()}}
                </p>
            @endif

            <a href="{{route('manage-groups.create')}}" class="group-create-btn" title="{{__('rm.CADASTRAR_COORDENADOR')}}">{{__('rm.CADASTRAR_COORDENADOR')}}</a>

            <div class="" id="group-filter">
                <input type="text" id="group-filter-input" placeholder="{{__('rm.BUSCA')}}">
            </div>

            <table class="group-table">
                <tbody>
                    @foreach($coordenadores as $coordenador)
                    <tr>
                        <td>{{$coordenador->nome}}</td>
                        <td>
                            <p>
                                {!!integrantes_grupo($coordenador->grupo, !$coordenador->invite()->first())!!}
                            </p>
                            @if ($coordenador->canAddToGroup())
                                <a href="{{route('manage-groups.add-form', $coordenador->grupo)}}" title="{{__('rm.CADASTRAR_MEDICOS_DO_GRUPO')}}">{{__('rm.CADASTRAR_MEDICOS_DO_GRUPO')}}</a>
                            @endif
                        </td>
                        <td>
                            @if (!$coordenador->invite()->first())
                                <a href="{{route('manage-groups.invite', $coordenador->grupo)}}" title="{{__('rm.ENVIAR_E-MAIL_CONVITE')}}">{{__('rm.ENVIAR_E-MAIL_CONVITE')}}</a>
                            @else
                                <p>
                                    E-MAIL CONVITE<br>
                                    Enviado em: {{$coordenador->invite()->first()->created_at->format('d/m/Y')}}
                                </p>
                            @endif
                        </td>
                        <td>
                            @if ($coordenador->canRemove())
                                <a href="{{route('manage-groups.destroy', $coordenador->id)}}" title="{{__('rm.EXCLUIR')}}">{{__('rm.EXCLUIR')}}</a>    
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection