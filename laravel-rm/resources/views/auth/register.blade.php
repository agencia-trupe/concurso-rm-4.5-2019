@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        {{__('Prezado(a) Dr(a)., seja bem-vindo ao website do Programa de Educação Médica RM 4.5 de Casos Clínicos. Faça o login pra acessar mais informações.')}}
      </p>

      <div class="form-register">

        <p>
          {{__('rm.SEU_EMAIL')}}: <strong>{{$login}}</strong>
        </p>

        <form action="{{ route('gravar-senha') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          <input type="hidden" name="email" value="{{$login}}" required>
          <input type="hidden" name="token" value="{{$token}}" required>
          <input type="password" name="password" placeholder="{{__('rm.SENHA')}}" required>
          <input type="password" name="password_confirmation" placeholder="{{__('rm.CONFIRMAR_SENHA')}}" required>
          <input type="submit" value="{{__('rm.ACESSAR_SISTEMA')}}">
        </form>
      </div>

    </div>
  </div>

@stop
