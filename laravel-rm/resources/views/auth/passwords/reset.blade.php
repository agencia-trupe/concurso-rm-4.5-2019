@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        {{__('Informe sua nova senha:')}}
      </p>

      <div class="form-login">
        <form action="{{ route('password.request') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          @if($email)
            <p style="margin-bottom:20px;">
              {{__('rm.SEU_EMAIL')}}: <strong>{{$email}}</strong>
            </p>
            <input type="hidden" name="email" value="{{ $email }}" required>
          @else
            <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" autofocus required>
          @endif

          <input type="hidden" name="token" value="{{ $token }}">
          <input type="password" name="password" placeholder="{{__('rm.NOVA_SENHA')}}" required>
          <input type="password" name="password_confirmation" placeholder="{{__('rm.CONFIRMAR_NOVA_SENHA')}}" required>

          <input type="submit" value="{{__('rm.REDEFINIR_SENHA')}}">
        </form>
      </div>

    </div>
  </div>


@endsection
