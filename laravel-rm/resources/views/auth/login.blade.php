@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        {{__('Prezado(a) Dr(a)., seja bem-vindo ao website do Programa de Educação Médica RM 4.5 de Casos Clínicos. Faça o login pra acessar mais informações.')}}
      </p>

      <div class="form-login">
        <form action="{{ route('login') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          @if(session('erros_sessao'))
            <p class="alerta alerta-erro">
              {{session('erros_sessao')}}
            </p>
          @endif

          @if($email)

            <p style="margin-bottom:20px;">
              {{__('rm.SEU_EMAIL')}}: <strong>{{$email}}</strong>
            </p>
            <input type="hidden" name="email" value="{{ $email }}" required>

          @else

            <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" autofocus required>

          @endif

          <input type="password" name="password" value="{{old('password')}}" placeholder="{{__('rm.SENHA')}}" autofocus required>
          <input type="submit" value="{{__('rm.ACESSAR_SISTEMA')}}">
        </form>
        <a href="{{ route('password.request') }}"
            title="{{__('rm.ESQUECI_MINHA_SENHA')}}"
            class="recuperar-senha">
            {{__('rm.ESQUECI_MINHA_SENHA')}} &raquo;
        </a>
      </div>

    </div>
  </div>

@stop
