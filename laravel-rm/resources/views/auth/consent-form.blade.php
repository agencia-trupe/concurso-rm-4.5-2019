@extends('template.index', ['hideMenu' => true])

@section('conteudo')

<div class="conteudo conteudo-login">
    <div class="centralizar">

        <div class="form-login">
            <form action="{{ route('user.consent.submit') }}" method="post" class="wide consent-form">
                {!! csrf_field() !!}

                @if($errors->any())
                    <p class="alerta alerta-erro">
                    {{$errors->first()}}
                    </p>
                @endif

                @if(session('erros_sessao'))
                    <p class="alerta alerta-erro">
                    {{session('erros_sessao')}}
                    </p>
                @endif

                <label>
                    <input type="checkbox" name="check_termos" value="1" required>
                    {!!__("Consinto na coleta e uso dos meus dados pela Novartis conforme consta do <a href='/aviso-de-privacidade' title='Ler aviso de privacidade'>aviso de privacidade</a> do Programa de Educação Médica RM 4,5 de Casos Clínicos, que li e aceito.")!!}
                </label>

                <input type="submit" value="OK">

            </form>
        </div>

    </div>
  </div>

@stop
