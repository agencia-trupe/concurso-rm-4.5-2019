<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, no-follow" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <title>{{__('rm.SITE_NAME')}}</title>

  <base href="{{ base_url() }}">

  <link href="https://fonts.googleapis.com/css?family=Asap:400,700|Montserrat:400,700" rel="stylesheet">
  <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <link href="css/app.css" rel="stylesheet" type="text/css" />
  <link href="css/quill.snow.css" rel="stylesheet">
</head>
<body>

  @include('partials.header')

  @yield('conteudo')

  @include('partials.footer')
  
  <script type="text/javascript" src="js/clipboard.min.js"></script>
  <script type="text/javascript" src="js/app.js"></script>
</body>
</html>
