@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento">
    <div class="centralizar">
      <h1>REGULAMENTO</h1>
      
      <h2>I – DO PROGRAMA</h2>
      <p>
        Promovido pela Novartis Biociências S/A, o Programa de Educação Médica Continuada RM4.5 de Relatos de Casos Clínicos 2019 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de Nilotinibe por grupos de trabalho organizados pela Novartis, envolvendo médicos do Brasil. Website exclusivo: <a href='www.rm45.com.br' title='RM 4.5'>www.rm45.com.br</a>.
      </p>

      <h2>II – DOS OBJETIVOS</h2>
      <p>
        a. Difundir o conhecimento técnico e científico sobre o tratamento da LMC com o uso de nilotinibe entre médicos Hematologistas e Oncologistas.
      </p>
      <p>
        b. Permitir o intercâmbio de boas práticas médicas dirigidas à área de Hematologia entre médicos especialistas do Brasil.
      </p>

      <h2>III – DA ELEGIBILIDADE DOS PARTICIPANTES</h2>

      <p class="destaque">
        Os participantes do programa deverão atender aos seguintes critérios:
      </p>
      <ul>
        <li>Ser médico e atuar no Brasil;</li>
        <li>Estar no painel de visitação dos consultores técnicos da Hematologia da Novartis.</li>
      </ul>
      <p>
          Os participantes do programa serão selecionados e convidados pela Novartis, com base nos critérios acima após validação pela área médica. Serão formados grupos de trabalho de 3 a 7 médicos participantes, que terão pelo menos 1 encontro organizado pela área comercial Novartis para discussão de casos clínicos. Os participantes receberão as regras de participação e deverão se inscrever no site e assinar o termo de participação on-line para confirmar a participação e comprovar o cumprimento dos critérios de participação acima. Cada grupo de trabalho terá 1 coordenador (já considerado dentro do número de 3 a 7 médicos de cada grupo de trabalho). O coordenador será indicado pela área comercial Novartis e sujeitos à validação da Área Médica da Novartis, conforme processo descrito a seguir:
      </p>
      <p>
        <strong>
          Os coordenadores deverão seguir critérios de palestrante nacional/regional de acordo com a política P3 Novartis.
        </strong>
      </p>
      <p class="destaque">
        O coordenador de cada grupo terá as seguintes atribuições:
      </p>
      <ul>
        <li>
          Liderar pelo menos 1 reunião com o seu grupo de trabalho e ser responsável pelo engajamento dos membros do grupo.</li>
        <li>
          Garantir que todos os membros do grupo realizem sua inscrição na plataforma on-line e confirme o aceite no regulamento antes da primeira reunião do grupo.</li>
        <li>
          Apresentar dados que demonstrem a importância do monitoramento molecular do paciente com LMC, a importância de se atingir dados de resposta de acordo com guidelines e dados sobre o uso de nilotinibe nesses pacientes. Para isso, deverá ser utilizado o slide kit padrão enviado pela área Médica Novartis.</li>
        <li>
          Esclarecer dúvidas dos membros do grupo.</li>
        <li>
          Facilitar as discussões do grupo em relação aos casos clínicos.</li>
        <li>
          Assegurar que os casos discutidos não envolvem indicações não aprovadas em bula.</li>
        <li>
          Assegurar que os casos de pacientes oriundos de estudo clínico não poderão ser submetidos ao programa.</li>
        <li>
          Assegurar que só serão submetidos casos clínicos em que o paciente estiver em uso de nilotinibe no momento da submissão.</li>
        <li>
          Assegurar que os eventos adversos que possam ser identificados durante as apresentações dos casos clínicos serão relatados à Farmacovigilância da Novartis.</li>
        <li>
          Garantir que os casos que serão submetidos ao Programa possuem consentimento do paciente, por escrito, e que informações pessoais, como nome, registro hospitalar e data de nascimento estejam blindadas no caso de envio de arquivo com exames laboratoriais.</li>
        <li>
          Realizar a submissão dos relatos de casos clínicos escolhidos pelo grupo para o Programa RM4.5, sendo obrigatório o envio de, no mínimo, um caso clínico do seu grupo, versões português e inglês. O coordenador receberá honorário dentro da política Novartis para sua prestação de serviço dentro das atribuições relacionadas acima.</li>
        </ul>

      <h3>IV – DO COMITÊ AVALIADOR</h3>
      <p>
        O Comitê Avaliador será formado por 5 participantes, médicos Hematologistas líderes nacionais e internacionais selecionados pela área médica da Novartis, os quais atuarão com total independência em relação à Novartis.
      </p>
      <p class="destaque">
          Os critérios de avaliação serão (Pontuação total possível por avaliador: 100 pontos):
      </p>
      <ul>
        <li>1. Título 1-10 pontos (peso 1);</li>
        <li>2. Relato do caso clínico 1-10 pontos (peso 3);</li>
        <li>3. Discussão e conclusão 1-10 pontos (peso 2).</li>
      </ul>
      
      <p class="destaque">
        Se existir empate após a conciliação das notas dos 5 avaliadores, os critérios de desempate seguirão a seguinte sequência:
      </p>
      <ul>
        <li>Maior pontuação obtida em Relato de caso clínico;</li>
        <li>Maior pontuação obtida em Discussão e conclusão;</li>
        <li>Maior pontuação obtida em Título.</li>
      </ul>

      <p>
        Os trabalhos não serão avaliados pelo(s) membro(s) do Comitê Avaliador da mesma cidade / município do autor principal do caso do grupo de trabalho, visando garantir imparcialidade na avaliação. Os casos serão avaliados de forma sigilosa – o avaliador não terá informação do nome dos médicos, Hospital ou cidade do relato de caso a ser avaliado.
      </p>

    <h3>
      V – DO TEMA
    </h3>
    <p>
      O programa será destinado aos grupos de médicos pré-selecionados conforme item III deste regulamento, que participarem das reuniões de discussão de casos clínicos organizadas pela Novartis. Os casos clínicos deverão ser de pacientes em tratamento para LMC em uso de nilotinibe no momento da submissão do caso conforme as 4 categorias abaixo:
    </p>
    <p>
      <span class="destaque">Categoria 1</span> - Rumo ao RM4.5: Caso clínico de paciente com LMC em uso de nilotinibe como segunda linha de tratamento (pós-uso de imatinibe) que atingiu ao menos BCR-ABL < 0,1% (RMM) em 12 meses apresentada em exame de PCR;
    </p>
    <p>
      <span class="destaque">Categoria 2</span> – Melhor tolerabilidade: Caso clínico de paciente com LMC que apresentou qualquer tipo de intolerância ao imatinibe e que esteja em uso de nilotinibe como segunda linha de tratamento e apresente RM4.5 no momento da submissão do caso em exame de PCR;
    </p>
    <p>
      <span class="destaque">Categoria 3</span> – Resposta Sustentada: Caso clínico de paciente com LMC em uso de nilotinibe por pelo menos 3 anos como segunda linha de tratamento (pós uso de imatinibe) e apresente resposta molecular profunda sustentada por 1 ano (RM4.5 na última avaliação, nenhum teste acima de RM4.0 nos últimos 12 meses) no momento da submissão do caso em exame de PCR;
    </p>
    <p>
      <strong>Categoria 4</strong> - Primeira linha: Caso clínico de paciente com LMC em uso de nilotinibe como primeira linha de tratamento e apresente RM4.5 no momento da submissão do caso em exame de PCR;
    </p>
    <p>
      Pacientes oriundos de estudo clínico não poderão ser submetidos ao programa. Só serão aceitos casos clínicos em que o paciente estiver em uso de nilotinibe no momento da submissão. Além do motivo da troca, os casos deverão demonstrar o porquê da escolha do nilotinibe (perfil de tolerabilidade, perfil do paciente etc).
    </p>
        
    <h3>
      VI – DA PREPARAÇÃO E SUBMISSÃO DOS CASOS CLÍNICOS
    </h3>
    <p>
      A estrutura do caso clínico deverá respeitar os campos indicados no arquivo modelo enviado via e-mail aos coordenadores que forem participar das reuniões de discussão de casos clínicos organizadas pela Novartis e também disponível no website do Programa.
    </p>
    <p class="destaque">
      A estrutura para o relato de caso clínico compreende:
    </p>
    <ul>
      <li>1. Título;</li>
      <li>2. Relato do caso clínico;</li>
      <li>3. Discussão e conclusão.</li>
    </ul>
    <p class="destaque">
      Os relatos de casos clínicos podem ter até 7 autores.
    </p>
    <p>
      Deve-se preservar a identidade do médico e do Serviço/Hospital onde o paciente do caso clínico foi atendido, não havendo nenhuma citação ou referência ao nome, local e/ou cidade no título e corpo do texto do relato de caso. É essencial preservar a identidade dos pacientes. Não usar o nome ou iniciais do paciente, omitir detalhes que possam identificar as pessoas, caso não sejam essenciais para o relato do caso. Necessário ter o consentimento, por escrito, do paciente para que seus dados e imagens sejam utilizados no relato de caso clínico submetido ao Programa RM4.5. Informações de pacientes deverão ser anonimizadas como forma de não ser possível a identificação do paciente. Não serão aceitos relatos de casos clínicos que considerem o uso de nilotinibe para indicações diferentes das de bula no país ou de pacientes que sejam oriundos de estudos clínicos. É de responsabilidade dos autores a exatidão das referências bibliográficas utilizadas no trabalho. Por conta do volume de trabalhos e a necessidade legal de reporte dos eventos adversos aos órgãos regulatórios nacionais e globais pela área de Farmacovigilância da Novartis, haverá 3 datas limite como prazo de submissão entre os grupos, essa definição ocorrerá através de um sorteio a ser realizado no dia 17/03/19 durante o kick-o_ com os coordenadores sobre as regras do Programa. Além disso, os trabalhos deverão ser desenvolvidos e submetidos exclusivamente pelo website www.rm45.com.br . O sorteio considerará o número total de coordenadores que serão divididos em 3 blocos (A,B e C).
    </p>
    <p class="destaque">
      Após o sorteio, os prazos serão:
    </p>
    <p>
      <span class="destaque">Bloco A:</span> às 23h59 min do dia 03 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.
    </p>
    <p>
      <span class="destaque">Bloco B:</span> às 23h59 min do dia 05 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.
    </p>
    <p>
      <span class="destaque">Bloco C:</span> às 23h59 min do dia 10 de setembro de 2019. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.
    </p>
    <p>
      Poderá ser submetido somente 01 (um) relato de caso de cada categoria por grupo de trabalho. O caso clínico deve ser enviado dentro da data limite do grupo que o seu grupo ficou inserido (A, B ou C). Os casos devem ser submetidos em português e inglês. A Novartis disponibilizará uma tradutora especializada para o coordenador do grupo caso seja necessário para o grupo.
    </p>
    <h3>
      VII - DA DINÂMICA DO PROGRAMA E DIVULGAÇÃO DO RESULTADO
    </h3>
    <p>
        De acordo com os critérios definidos na sessão III, serão formados grupos de discussão de casos clínicos a partir de convite da área comercial Novartis. Cada grupo terá entre 3 a 7 membros, sendo um coordenador, que ficará responsável pelo alinhamento e coordenação dos trabalhos de seu grupo. Entre março e agosto de 2019, com suporte e organização da Novartis (locação de sala para evento, equipamentos, refeição, logística de participantes e honorário do coordenador), serão realizadas reuniões dos grupos de discussão para o debate sobre casos clínicos. Ao final das reuniões, cada grupo selecionará um relato de caso clínico de cada categoria que deverá ser submetido ao Programa RM4,5. Consultores técnicos da Novartis estarão presentes nas reuniões, porém, não terão qualquer ingerência ou interferência nas discussões dos grupos de trabalho, a não ser sob solicitação e caso possam colaborar com o grupo, dentro das limitações de informações dos presentes, considerando o cargo de cada consultor técnico Novartis presente na reunião. A área médica dará o suporte para o conteúdo de nilotinibe a ser apresentado. As aulas a serem apresentadas pelos coordenadores nos grupos de trabalho deverão utilizar o slide kit padrão enviado pela área Médica Novartis. Os casos serão avaliados de forma sigilosa através de ferramenta de votação, seguindo os critérios mencionados na sessão IV.
    </p>
    <p class="destaque">
      Serão escolhidos 3 grupos vencedores do Programa, sendo eles:
    </p>
    <ul>
      <li>Vencedor 1: Caso com maior pontuação na categoria 1;</li>
      <li>Vencedor 2: Caso com maior pontuação na categoria 2;</li>
      <li>Vencedor 3: Caso com maior pontuação na categoria 3;</li>
      <li>Vencedor 4: Caso com maior pontuação na categoria 4.</li>
    </ul>
    <p>
        Vencedores serão divulgados durante um evento de divulgação dos resultados a ser realizado em Novembro de 2019. Os relatos de caso vencedores poderão ser publicados em material promocional Novartis. A assinatura do termo de participação formaliza o consentimento do autor. Além disso, os vencedores poderão ser convidados pela Novartis para apresentar seus relatos de casos clínicos em eventos Novartis.
    </p>
    <h3>
      VIII - DA DIVULGAÇÃO DOS RESULTADOS
    </h3>
    <p>
      Os grupos vencedores (coordenador e demais membros do grupo) serão convidados a participar de um preceptorship na instituição de um dos avaliadores internacionais, para educação e apresentação do caso em data a ser definida pela Novartis, além de terem o caso clínico comentado publicado em 2019 em material promocional desenvolvido pela Novartis. O apoio educacional acima mencionado (exclusivamente para os membros e coordenadores dos grupos vencedores) é de caráter pessoal e intransferível para cada membro do grupo e não poderá ser convertido em dinheiro ou substituído por qualquer outra forma de compensação material. O apoio consistirá em inscrição para o evento, passagem aérea em classe econômica e/ou transporte terrestre em veículo automotor (dependendo da localização de residência) e hospedagem em apartamento Single, restringindo-se exclusivamente aos membros do grupo vencedor, não sendo extensivo a acompanhantes. Quaisquer despesas pessoais a mais incorridas pelos participantes que receberem o apoio educacional acima descrito durante sua participação no evento serão de única e exclusiva responsabilidade do participante.
    </p>
    <h3>
      IX – DO CRONOGRAMA*
    </h3>
    
    <ul>
      <li>15 de fevereiro: prazo para indicações dos médicos coordenadores pela área comercial Novartis e validação e convite aos médicos coordenadores pela área médica Novartis.</li>
      <li>16 de março: reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do Programa.</li>
      <li>De 18 de março a 04 de setembro: reuniões dos grupos de trabalho para discussão dos casos clínicos.</li>
      <li>Até 03 de setembro: deadline submissão dos grupos dentro do bloco A dos casos clínicos para avaliação do comitê avaliador.</li>
      <li>Até 05 de setembro: deadline submissão dos grupos dentro do bloco B dos casos clínicos para avaliação do comitê avaliador.</li>
      <li>Até 10 de setembro: deadline submissão dos grupos dentro do bloco C dos casos clínicos para avaliação do comitê avaliador.</li>
      <li>De 11 de setembro a 06 de outubro: avaliação dos relatos de casos clínicos pelo comitê avaliador.</li>
      <li>Novembro: Evento de divulgação dos resultados.</li>
      <li>2020: Divulgação dos resultados - publicação de material promocional, com caso clínico de cada grupo vencedor e apoio ao preceptorship para todos os integrantes dos grupos vencedores.</li>
      <li class="destaque">Datas poderão sofrer alteração, mediante comunicação aos participantes.</li>
    </ul>
    <h3>
      X – DA DESCLASSIFICAÇÃO DOS CASOS
    </h3>
    <p class="destaque">
      A avaliação dos casos em relação às regras será realizada por uma empresa especializada em auditoria.
    </p>
    <p class="destaque">
      Serão desclassificados automaticamente e não avapados os casos clínicos que:
    </p>
    <p>
      1) Não estiverem dentro indicações aprovadas em bula, são elas:
    </p>
    <p>
      2ª linha de tratamento:
    </p>
    <p>
        LMC em fase crônica ou fase acelerada resistente a outro tratamento prévio incluindo imatinibe.
    </p>
    <p>
        LMC em fase crônica ou fase acelerada intolerante ao tratamento com imatinibe.
    </p>
    <p>
      1ª linha de tratamento:
    </p>
    <p>
        O tratamento de pacientes adultos com Leucemia Mieloide Crônica cromossomo Philadelphia positivo (LMC Ph+) em fase crônica recém-diagnosticada.
    </p>
      
    <p>
      2) Contemplem pacientes oriundos de estudo clínico.
    </p>
    <p>
        3) Não estiverem em uso de nilotinibe no momento da submissão do caso. Uma vez que as regras estão estabelecidas no regulamento a Novartis se reserva ao direito de não comunicar a desclassificação.
    </p>
    <h3>
      XI – CONSIDERAÇÕES FINAIS
    </h3>
    <p>
        Ao inscreverem-se para o Programa RM4.5 de Relatos de Casos Clínicos 2019, os participantes tacitamente concordam com as regras constantes deste documento, bem como a utilização sem ônus e sem qualquer contrapartida pela Novartis, de seu respectivo nome, imagem, fotografias tiradas durante os eventos e trabalho para divulgação em qualquer meio de comunicação e publicações científicas, em âmbito nacional ou internacional, conforme legislação vigente, assim como publicação em materiais científicos da Novartis, que se compromete a identificar a autoria do material divulgado. As opiniões ou declarações contidas nos relatos de casos clínicos são de responsabilidade única e exclusiva dos autores dos relatos e não refletem necessariamente a posição da Novartis. Suspeitas de conduta antiética entre os participantes do Programa e na elaboração dos casos clínicos serão apreciadas pelo Comitê Técnico da Novartis e, se consideradas procedentes, acarretarão sua desclassificação. Os relatos de caso clínico que não estejam de acordo com este regulamento também poderão ser desclassificados do Programa RM4.5. Não se estabelece, pela força dessa iniciativa, de caráter essencialmente científico e educacional, qualquer vínculo de qualquer natureza entre a Novartis e os participantes.
    </p>
    <p>
        A Novartis não será responsável por nenhuma despesa incorrida pelo participante deste Programa para a elaboração e envio dos seus trabalhos ou por qualquer ato do participante que possa causar danos materiais ou morais a sua pessoa ou de terceiros. O participante responderá integralmente pela veracidade e acuidade dos dados cadastrados e enviados, incluindo os dados constantes do relato de caso inscrito, incluindo as respectivas referências. A Novartis se reserva no direito de alterar este regulamento a qualquer tempo, mediante notificação prévia a todos os participantes. Quaisquer questões relativas a esta iniciativa de caráter científico e educacional que não estejam endereçadas no presente Regulamento serão avaliadas pelo Comitê Técnico da Novartis, cuja decisão será soberana, não sendo admitido questionamento posterior.
    </p>
      <p>
        <i>
          Regulamento Versão 3.0 São Paulo, 08 de janeiro de 2019.
        </i>
      </p>
      </div>
    </div>
  </div>

@endsection
