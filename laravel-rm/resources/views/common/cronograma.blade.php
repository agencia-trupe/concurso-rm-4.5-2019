@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-cronograma">
    <div class="centralizar">

      <div class="coluna coluna-37">
        <h1>{{__('rm.CRONOGRAMA')}}</h1>
        <img src="images/cronograma.png" alt="Cronograma">
      </div>

      <div class="coluna coluna-63">
        <ul class="lista-cronograma">
          <li @if($faseAtual == '1') class="ativo" @endif data-label-ativo="{{__('rm.FASE_ATUAL')}}">
            {!!__('<strong>22 de Março a 4 de Setembro</strong><span>Discussões Regionais</span>')!!}
          </li>
          <li @if($faseAtual == '2') class="ativo" @endif data-label-ativo="{{__('rm.FASE_ATUAL')}}">
            {!!__('<strong>3 a 10 de Setembro</strong><span>Submissão dos casos clínicos para avaliação</span>')!!}
          </li>
          <li @if($faseAtual == '3') class="ativo" @endif data-label-ativo="{{__('rm.FASE_ATUAL')}}">
            {!!__('<strong>11 de Setembro a 6 de Outubro</strong><span>Avaliação dos relatos de casos clínicos</span>')!!}
          </li>
          <li @if($faseAtual == '4') class="ativo" @endif data-label-ativo="{{__('rm.FASE_ATUAL')}}">
            {!!__('<strong>7 de Novembro</strong><span>Evento de divulgação de resultados</span>')!!}
          </li>
          <li @if($faseAtual == '5') class="ativo" @endif data-label-ativo="{{__('rm.FASE_ATUAL')}}">
            {!!__('<strong>2020</strong><span>Divulgação dos resultados</span>')!!}
          </li>
        </ul>
      </div>

    </div>
  </div>

@endsection
