@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento">
    <div class="centralizar">
      <h1>REGULATIONS</h1>
      
      <h2>I – PROGRAM</h2>
      <p>
        Promoted by Novartis Biociências S/A, the 2019 RM4.5 Continued Medical Education Program of Clinical Case Reports  will involve the submission of clinical case reports from subjects on Nilotinib treatment for Chronic Myeloid Leukemia (CML) by workgroups organized by Novartis, comprising physicians in Brazil. Exclusive website: <a href='www.rm45.com.br' title='RM 4.5'>www.rm45.com.br</a>.
      </p>

      <h2>II – OBJECTIVES</h2>
      <p>
        a. To disseminate the technical and scientific knowledge on CML treatment with nilotinib among Hematologists and Oncologists. 
      </p>
      <p>
        b. To allow for the exchange of good clinical practices directed to the Hematology area among specialists in Brazil. 
      </p>

      <h2>III – PARTICIPANT ELEGIBILITY</h2>

      <p class="destaque">
        Program participants must meet the following criteria:
      </p>
      <ul>
        <li>Being a physician acting in Brazil;</li>
        <li>Being in the Novartis Hematology technical consultants’ visitation panel.</li>
      </ul>
      <p>
        Program participants will be selected and invited by Novartis based on the criteria above, after validation by medical area. Workgroups of 3 to 7 participating physicians will be formed, having at least 1 meeting organized by Novartis’ commercial area for discussion of clinical cases. Participants will receive the participation rules and must register in the website and sign the on-line participation form to confirm participation and the fulfillment of participation criteria above. Each workgroup will have 1 coordinator (already considered within the number of 3 to 7 physicians in each workgroup). The coordinator will be appointed by Novartis’ commercial area and subject to validation by Novartis’ Medical Area, according to the process described as follows: 
      </p>
      <p>
        <strong>
          The Coordinators must follow the national/regional speaker criteria, in accordance with Novartis’ P3 policy. 
        </strong>
      </p>
      <p class="destaque">
        Each group coordinator will have the following attributions: 
      </p>
      <ul>
        <li>
          To host at least 1 meeting with their workgroup, being responsible for the group members engagement.</li>
        <li>
          To ensure that all group members register in the online platform and check the accept box in the regulations before the first group meeting.</li>
        <li>
          To present data showing the importance of molecular monitoring in subjects with CML, the importance of reaching response data according to guidelines and data on nilotinib use in these subjects. To that end, the standard slide kit sent by Novartis’ Medical area must be used.</li>
        <li>
          To answer questions from group members.</li>
        <li>
          To facilitate group discussions regarding clinical cases.</li>
        <li>
          To ensure that the cases discussed do not involve off-label indications.</li>
        <li>
          To ensure that patients’ cases derived from clinical trials cannot be submitted to the program.</li>
        <li>
          To ensure that only clinical cases where the patient is on nilotinib at the time of submission will be used.</li>
        <li>
          To ensure that adverse events which may be identified during the presentations of the clinical cases are reported to Novartis’ Pharmacovigilance department.</li>
        <li>
          To ensure that the cases to be submitted to the Program have the patient’s written consent, and that personal information such as name, medical record, and date of birth are blinded, in case of submission of a file with laboratory tests.</li>
        <li>
          To submit clinical case reports selected by the group to the RM4.5 Program, where the submission of at least one clinical case from your group, including the Portuguese and the English versions, is mandatory. The coordinators will receive a fee for their services, per Novartis’ policy, within the attributions listed above.</li>
        </ul>

      <h3>IV – EVALUATION COMMITTEE</h3>
      <p>
        The Evaluation Committee will be formed by 5 participants, national and international namely lead Hematologists selected by Novartis’ medical area, who will act in full independence in regards to Novartis. 
      </p>
      <p class="destaque">
        The evaluation criteria will be as follows (Total possible score by evaluator: 100 points):
      </p>
      <ul>
        <li>1. Title 1-10 points (value 1);</li>
        <li>2. Clinical case report 1-10 points (value 3);</li>
        <li>3. Discussion and conclusion 1-10 points (value 2).</li>
      </ul>
      
      <p class="destaque">
        If there is a tie after the presentation of scores from the 5 evaluators, the tiebreaker criteria will follow the sequence bellow: 
      </p>
      <ul>
        <li>Highest score in Clinical case report;</li>
        <li>Highest score in Discussion and conclusion;</li>
        <li>Highest score in Title.</li>
      </ul>

      <p>
        The papers will not be evaluated by Evaluation Committee members from the same city / district as the main author of the group’s case, ensuring impartiality in the evaluation. The cases will be evaluated on a confidential basis – the evaluator will not have information on the physicians’ names, the Hospital, or the city of the case report to be evaluated. 
      </p>

    <h3>
      V – THEME
    </h3>
    <p>
      The program is intended for the previously selected physician groups, per item III hereof, who participate in the clinical case discussion meetings organized by Novartis. The clinical cases must be from patients on CML treatment using nilotinib at the time of the case submission, according to the 4 categories below: 
    </p>
    <p>
      <span class="destaque">Category 1</span> - Aiming at RM4.5: Clinical case of a CML patient on nilotinib as a second-line treatment (post-imatinib use) who achieved at least BCR-ABL < 0.1% (RMM) in 12 months presented in a PCR test; 
    </p>
    <p>
      <span class="destaque">Category 2</span> – Best tolerability: Clinical case of a CML patient who experienced any type of intolerance to imatinib, who is on nilotinib as a second-line treatment and who has RM4.5 in a PCR test at the time of case submission; 
    </p>
    <p>
      <span class="destaque">Category 3</span> – Sustained Response: Clinical case of a CML patient on second-line (post-imatinib) nilotinib treatment for at least 3 years, who has had sustained deep molecular response for 1 year (RM4.5 at the last assessment, no tests above RM4.0 within the last 12 months) in a PCR test at the time of case submission; 
    </p>
    <p>
      <strong>Category 4</strong> - First Line: Clinical case of a CML patient on first-line nilotinib treatment, who has RM4.5 in a PCR test at the time of case submission; 
    </p>
    <p>
      Patients from clinical trials may not be submitted to the program. Only clinical cases where the patient is on nilotinib at the time of submission will be accepted. In addition to the reason for the switch, the cases must show the reason why nilotinib was chosen (tolerability profile, patient profile etc). 
    </p>
        
    <h3>
      VI – PREPARATION AND SUBMISSION OF CLINICAL CASES
    </h3>
    <p>
      The clinical case structure must observe the fields indicated in the template file emailed to the coordinators who participate in the clinical case discussion meetings organized by Novartis, which is also available at the Program’s website.
    </p>
    <p class="destaque">
      The clinical case reporting structure comprises: 
    </p>
    <ul>
      <li>1. Title;</li>
      <li>2. Clinical case report;</li>
      <li>3. Discussion and conclusion.</li>
    </ul>
    <p class="destaque">
      Clinical case reports may have up to 7 authors. 
    </p>
    <p>
      The identity of the physician and the Service/Hospital where the clinical case patient was treated must be preserved, and there must not be any mention or reference to the name, place, and/or city in the title and text of the case report. It is vital to preserve the patients’ identity. Do not use the patient’s name or initials and omit details that might identify someone if these are not essential to the case report. It is necessary to have the patient’s written consent for their data and images to be used in the clinical case report submitted to the RM4.5 Program. The patients’ information must be anonymized in such a way as to prevent patient identification. Clinical case reports considering the use of nilotinib for off-label indications in the country or from patients coming from clinical trials will not be accepted. The accuracy of the bibliographical references used in the paper is the authors’ responsibility. Due to the papers’ volume and the legal requirement of reporting adverse events to national and international regulatory agencies by Novartis’ Pharmacovigilance, there will be 3 limit dates as submission deadlines between the groups, which will be defined through a drawing of lots to be performed on 03/17/19, during the kick-off with the coordinators regarding the Program rules. Furthermore, papers must be developed and submitted exclusively through the website www.rm45.com.br . The drawing of lots will consider the total number of coordinators who will be divided into 3 groups (A, B, and C). 
    </p>
    <p class="destaque">
      After the drawing of lots, the deadlines will be as follows: 
    </p>
    <p>
      <span class="destaque">Group A:</span> 11:59 p.m. on September 03, 2019. Case reports not using the template file or which have been submitted after the limit date and time will not be accepted.
    </p>
    <p>
      <span class="destaque">Group B:</span> 11:59 p.m. on September 05, 2019. Case reports not using the template file or which have been submitted after the limit date and time will not be accepted. 
    </p>
    <p>
      <span class="destaque">Group C:</span> 11:59 p.m. on September 10, 2019. Case reports not using the template file or which have been submitted after the limit date and time will not be accepted. 
    </p>
    <p>
      Only one (01) case report per workgroup may be submitted for each category. The clinical case must be submitted within the limit date assigned to your workgroup (A, B, or C). The cases must be submitted in English and Portuguese. Novartis will make a specialized translator available to the group coordinator, in case this is necessary for the group. 
    </p>
    <h3>
      VII - PROGRAM DYNAMIC AND DISCLOSURE OF RESULTS
    </h3>
    <p>
      According to the criteria defined on section III, clinical case discussion groups will be formed upon an invite from Novartis’ commercial area. Each group will have 3 to 7 members, one of whom will be the coordinator, who will be responsible for the alignment and coordination of the papers in their group. Between March and August, 2019, with the support and organization from Novartis (room rental for the event, equipment, meals, participant logistics, and coordinator fees), group discussion meetings will be held to debate on the clinical cases. At the end of the meetings, each group will select one clinical case report for each category, which must be submitted to the RM4,5 Program. Technical consultants from Novartis will be present at the meetings, but will not interfere with the workgroup discussions in any way, except upon request and if they can collaborate with the group, within the information limitations of the attendees, and considering the position of each Novartis technical consultant at the meeting. Medical area will provide the support for the nilotinib content to be presented. The lectures to be presented by the workgroup coordinators must use the standard slide kit submitted by Novartis’ Medical area. The cases will be assessed on a confidential basis through voting, following the criteria mentioned on section IV. 
    </p>
    <p class="destaque">
      Three Program winning groups will be chosen, to which: 
    </p>
    <ul>
      <li>Winner 1: Case with the highest score on category 1;</li>
      <li>Winner 2: Case with the highest score on category 2;</li>
      <li>Winner 3: Case with the highest score on category 3;</li>
      <li>Winner 4: Case with the highest score on category 4.</li>
    </ul>
    <p>
      The winners will be revealed during a results disclosure event to be held in November, 2019. The winning case reports may be published in Novartis promotional materials. The signature on the participation form formalizes the author’s consent. In addition, the winners may be invited by Novartis to present their clinical case reports at Novartis events. 
    </p>
    <h3>
      VIII - DISCLOSURE OF RESULTS
    </h3>
    <p>
      The winning groups (coordinator and other group members) will be invited to participate in a preceptorship at the institution of one of the international evaluators, for education and presentation of the case in a date to be defined by Novartis, in addition to having the clinical case commented and published in 2019 in promotional material developed by Novartis. The educational support mentioned above (exclusively for members and coordinators of the winning groups) is personal and non-transferable for each group member, and may not be converted into cash or replaced by any other form of material compensation. The support will consist of enrollment in the event, coach section airline ticket and/or land transportation in an automotive vehicle (depending on the residence location) and accommodations in a Single room, restricted solely to the members of the winning group, not including guests. Any extra personal expenses incurred by the participants receiving the educational support described above during their participation in the event will be the sole responsibility of the participant. 
    </p>
    <h3>
      IX – SCHEDULE*
    </h3>
    
    <ul>
      <li>February 15: deadline for indications of coordinating physicians by Novartis’ commercial area and validation and invitation of the coordinating physicians by Novartis’ medical area.</li>
      <li>March 16: presential meeting with the coordinators and members of the judging committee for the Program presentation.</li>
      <li>From March 18 to September 04: meetings of the workgroups to discuss the clinical cases.</li>
      <li>Until September 03: group A submission deadline of clinical cases for evaluation by the evaluation committee.</li>
      <li>Until September 05: group B submission deadline of clinical cases for evaluation by the evaluation committee.</li>
      <li>Until September 10: group C submission deadline of clinical cases for evaluation by the evaluation committee.</li>
      <li>From September 11 to October 06: assessment of the clinical case reports by the evaluation committee.</li>
      <li>November: Event for disclosure of results.</li>
      <li>2020: Disclosure of results – publication of promotional material, with a clinical case from each winning group and support for the preceptorship for all members of the winning groups.</li>
      <li class="destaque">Dates may be changed upon communication to the participants.</li>
    </ul>
    <h3>
      X – DISQUALIFICATION OF CASES
    </h3>
    <p class="destaque">
      The assessment of the cases regarding the rules will be conducted by a specialized auditing company. 
    </p>
    <p class="destaque">
      Clinical cases will be automatically disqualified and not evaluated if they: 
    </p>
    <p>
      1) Are not within the labeled indications, to which: 
    </p>
    <p>
      2nd line treatment: 
    </p>
    <p>
      Chronic-phase or accelerated-phase CML that is resistant to another previous treatment, including imatinib. 
    </p>
    <p>
      Chronic-phase or accelerated-phase CML that is intolerant to imatinib treatment. 
    </p>
    <p>
      1st line treatment: 
    </p>
    <p>
      Treatment of adult patients with recently diagnosed, chronic-phase Philadelphia chromosome-positive Chronic Myeloid Leukemia (Ph+ CML). 
    </p>
      
    <p>
      2) Include patients coming from clinical trials. 
    </p>
    <p>
      3) Include patients who are not on nilotinib at the time of case submission. Once the rules have been established in the regulations, Novartis reserves the right not to communicate disqualification. 
    </p>
    <h3>
      XI – FINAL CONSIDERATIONS
    </h3>
    <p>
      Upon enrollment in the 2019 RM4.5 Clinical Case Report Program, participants implicitly agree to the rules herein, as well as the use, free of charge and with no considerations from Novartis, of their respective names, images, and photographs taken during the events and promotion works in any communication media and scientific publications, whether nationally or internationally, as per the legislation in force, as well as in publications of scientific materials from Novartis, that undertakes to indentify the author of the disclosed material. The opinions or statements contained in the clinical case reports are the sole responsibility of the authors of such reports, and do not necessarily reflect Novartis’ position. Suspect unethical conduct among Program participants and in the preparation of the clinical cases will be appraised by Novartis’ Technical Committee and, if considered justified, will entail their disqualification. Clinical case reports which are in disagreement with these regulations may also be disqualified from the RM4.5 Program. No relationship of any nature is established between Novartis and the participants by force of this initiative, which is essentially scientific and educational in nature. 
    </p>
    <p>
      Novartis will not be responsible for any expenses incurred by participants in this Program for the preparation and submission of their papers or for any acts on their part which may cause material or moral damage to themselves or others. Participants will fully respond for the veracity and accuracy of the data enrolled and submitted, including the data on the enrolled case report and the respective references. Novartis reserves the right to change these regulations at any time, upon previous notification to all participants. Any matters pertaining to this scientific and educational initiative which are not addressed herein will be assessed by Novartis’ Technical Committee, whose decision will prevail, where no subsequent challenges are allowed. 
    </p>
      <p>
        <i>
          Regulations Version 3.0 São Paulo, January 08, 2019. 
        </i>
      </p>
      </div>
    </div>
  </div>

@endsection
