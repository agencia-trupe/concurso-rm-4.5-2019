@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-como-funciona">
    <div class="centralizar">
      
        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>
                    <span>
                        {!!__('rm.OQUE_E_O_PROGRAMA')!!}
                    </span>
                </h1>
            </div>
            <div class="coluna coluna-63">
                <p>
                    {{__('O PROGRAMA DE EDUCAÇÃO MÉDICA RM4.5 DE CASOS CLÍNICOS 2019 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de nilotinibe, com resposta RM4.5, por grupos de trabalho organizados pela Novartis, envolvendo médicos do Brasil.')}}
                </p>
                <p>
                    {{__('O objetivo é difundir o conhecimento técnico e científico sobre o tratamento da LMC com uso de nilotinibe e permitir o intercâmbio de boas práticas entre os hematologistas do Brasil.')}}
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>{{__('rm.CATEGORIAS')}}</h1>
                <img src="images/categorias.png" alt="Categorias">
            </div>
            <div class="coluna coluna-63">
                <h2>{{__('rm.CATEGORIA')}} 1</h2>
                <h3>{{__('rm.CATEGORIA_1_TITULO')}}</h3>
                <p>
                    {{__('rm.CATEGORIA_1_TEXTO')}}                    
                </p>
                <h2>{{__('rm.CATEGORIA')}} 2</h2>
                <h3>{{__('rm.CATEGORIA_2_TITULO')}}</h3>
                <p>
                    {{__('rm.CATEGORIA_2_TEXTO')}}                    
                </p>
                <h2>{{__('rm.CATEGORIA')}} 3</h2>
                <h3>{{__('rm.CATEGORIA_3_TITULO')}}</h3>
                <p>
                    {{__('rm.CATEGORIA_3_TEXTO')}}                    
                </p>
                <h2>{{__('rm.CATEGORIA')}} 4</h2>
                <h3>{{__('rm.CATEGORIA_4_TITULO')}}</h3>
                <p>
                    {{__('rm.CATEGORIA_4_TEXTO')}}                    
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>{{__('rm.AVALIAÇÃO')}}</h1>
                <img src="images/avaliacao.png" alt="Avaliação">
            </div>
            <div class="coluna coluna-63">
                <p>
                    {!!__('<strong>O COMITÊ AVALIADOR SERÁ FORMADO POR 4 PARTICIPANTES</strong>, médicos Hematologistas líderes nacionais e internacionais, os quais atuarão com total independência em relação à Novartis.')!!}
                </p>
                <p>
                    <strong>{{__('rm.AVALIADORES')}}</strong>
                </p>
                <p>
                    <strong>Dr. Pierre Laneuville</strong><br>
                    {{__('Professor Associado - Department of Medicine, Division of Experimental Medicine -McGill University – Canadá.')}}
                </p>
                <p>
                    <strong>Dr. Valentin Garcia</strong><br>
                    {{__('Professor associado do Departamento de Medicina – Universidade de Alcalá – Espanha e Médico adjunto do Hospital Ramon y Cajal - Espanha.')}}
                </p>
                <p>
                    <strong>Dr. Gustavo Henrique Romani Magalhães</strong><br>
                    {{__('Médico hematologista da Universidade Federal de Minas Gerais e coordenador geral da Unidade de Hematologia e Oncologia do Hospital das Clínicas - UFMG.')}}
                </p>
                <p>
                    <strong>Dra. Vaneuza Araújo Moreira Funke</strong><br>
                    {{__('Professora assistente de Hematologia da Universidade Federal do Paraná e chefe da Divisão de adultos do Serviço de Transplante de Medula Óssea da Universidade Federal do Paraná.')}}
                </p>
               
                <p>
                    {!!__('<strong>CRITÉRIOS DE AVALIAÇÃO:</strong><br>1. Título; 2. Relato do caso clínico; 3. Discussão e conclusão.')!!}
                </p>
                
                <p>
                    {!!__('<em>OBSERVAÇÃO:</em><br>Os trabalhos não serão avaliados pelo(s) membro(s) do Comitê Avaliador da mesma cidade do grupo de trabalho, visando garantir imparcialidade na avaliação. Os casos serão avaliados de forma anônima, ou seja, o avaliador não terá informação do nome dos médicos, hospital ou cidade do relato de caso a ser avaliado. Esse controle será possível através da tecnologia da plataforma digital, onde os casos serão submetidos.')!!}
                </p>
                
                <p>
                    {!!__('<strong>ESCOLHA DOS VENCEDORES:</strong><br>Serão escolhidos 4 grupos vencedores do programa, sendo eles:<br><strong>VENCEDOR 1:</strong> Caso com maior pontuação na categoria 1<br><strong>VENCEDOR 2:</strong> Caso com maior pontuação na categoria 2<br><strong>VENCEDOR 3:</strong> Caso com maior pontuação na categoria 3<br><strong>VENCEDOR 4:</strong> Caso com maior pontuação na categoria 4')!!}
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>{{__('rm.PRÊMIO')}}</h1>
                <img src="images/premio.png" alt="PRÊMIO">
            </div>
            <div class="coluna coluna-63">
                <p>
                    {{__('Os grupos vencedores (coordenador e demais membros do grupo) serão convidados a participar e apresentar o caso em um preceptorship na instituição de um dos avaliadores internacionais dentro do programa de educação continuada da Novartis, além de terem o caso clínico publicado em 2019, em material promocional desenvolvido pela empresa.')}}
                </p>                
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>{{__('rm.CRONOGRAMA')}}</h1>
                <img src="images/cronograma.png" alt="Cronograma">
            </div>
            <div class="coluna coluna-63">
                <ul class="lista-cronograma">
                    <li>
                        {!!__('<strong>22 de Março a 4 de Setembro</strong><span>Discussões Regionais</span>')!!}
                    </li>
                    <li>
                        {!!__('<strong>3 a 10 de Setembro</strong><span>Submissão dos casos clínicos para avaliação</span>')!!}
                    </li>
                    <li>
                        {!!__('<strong>11 de Setembro a 6 de Outubro</strong><span>Avaliação dos relatos de casos clínicos</span>')!!}
                    </li>
                    <li>
                        {!!__('<strong>7 de Novembro</strong><span>Evento de divulgação de resultados</span>')!!}
                    </li>
                    <li>
                        {!!__('<strong>2020</strong><span>Divulgação dos resultados</span>')!!}
                    </li>
                </ul>
            </div>
        </div>

        <a href="{{$regulations_file_path}}" target="_blank" class="link-regulamento" title="Download do Regulamento">{{__('rm.FACA_DOWNLOAD_REGULAMENTO_AQUI')}} &raquo;</a>

        <div class="center">
            <a href="{{env('BULA_PDF')}}" class="link-bula" target="_blank" title="{{__('rm.CONSULTE_AQUI_A_BULA_DO_PRODUTO')}}">{{__('rm.CONSULTE_AQUI_A_BULA_DO_PRODUTO')}}</a>
        </div>

    </div>
  </div>

@endsection
