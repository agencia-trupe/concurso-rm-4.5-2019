@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-avaliadores com-recuoo">
    <div class="centralizar">

      <div class="coluna coluna-37">
        <h1>{{__('rm.AVALIADORES')}}</h1>
        <img src="images/avaliacao.png" alt="{{__('rm.AVALIADORES')}}">
      </div>

      <div class="coluna coluna-63">
        <div class="lista-avaliadores">

          <img src="images/avaliador1-pierre.png" alt="Dr. Pierre Laneuville">
          <h3>
            Dr. Pierre Laneuville
          </h3>
          <p>
            {{__('Professor Associado - Department of Medicine, Division of Experimental Medicine -McGill University – Canadá.')}}
          </p>

          <img src="images/avaliador2-valentin.png" alt="Dr. Valentin Garcia">
          <h3>
            Dr. Valentin Garcia
          </h3>
          <p>
            {{__('Professor associado do Departamento de Medicina – Universidade de Alcalá – Espanha e Médico adjunto do Hospital Ramon y Cajal - Espanha.')}}
          </p>

          <img src="images/avaliador3-gustavo.png" alt="Dr. Gustavo Henrique Romani Magalhães">
          <h3>
            Dr. Gustavo Henrique Romani Magalhães
          </h3>
          <h4>
            CRM: 34070
          </h4>
          <p>
            {{__('Médico hematologista da Universidade Federal de Minas Gerais e coordenador geral da Unidade de Hematologia e Oncologia do Hospital das Clínicas - UFMG.')}}
          </p>

          <img src="images/avaliador4-vaneuza.png" alt="Dra. Vaneuza Araújo Moreira Funke">
          <h3>
            Dra. Vaneuza Araújo Moreira Funke
          </h3>
          <h4>
              CRM: 15219
          </h4>
          <p>
            {{__('Professora assistente de Hematologia da Universidade Federal do Paraná e chefe da Divisão de adultos do Serviço de Transplante de Medula Óssea da Universidade Federal do Paraná.')}}
          </p>
          
        </div>
      </div>

    </div>
  </div>

@endsection
