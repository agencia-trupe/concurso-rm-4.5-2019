@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-home">
    <div class="centralizar">

      @if(session('sucesso_criacao_senha'))
        <p class="alerta alerta-sucesso" style="margin-bottom: 30px;">
          {{session('sucesso_criacao_senha')}}
        </p>
      @endif

      <div class="box-home">
        <p> 
          {{__('O PROGRAMA DE EDUCAÇÃO MÉDICA RM 4,5 DE CASOS CLÍNICOS 2019 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de Nilotinibe com resposta RM 4,5 por grupos de trabalho organizados pela Novartis envolvendo médicos do Brasil.')}}
        </p>
        <p>
          {{__('O objetivo é difundir o conhecimento técnico e científico sobre o tratamento da LMC com uso de nilotinibe e permitir o intercâmbio de boas práticas entre os hematologistas do Brasil.')}}
        </p>
        <a href="{{env('BULA_PDF')}}" class="link-bula" target="_blank" title="{{__('rm.CONSULTE_AQUI_A_BULA_DO_PRODUTO')}}">{{__('rm.CONSULTE_AQUI_A_BULA_DO_PRODUTO')}}</a>
      </div>

    </div>
  </div>

@endsection
