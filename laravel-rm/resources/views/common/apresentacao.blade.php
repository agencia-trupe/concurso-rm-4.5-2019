@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento">
    <div class="centralizar">
        <h1>{{__('rm.APRESENTACAO')}}</h1>
        <div class="coluna-regulamento">
            <p>{{__('Olá, seja bem-vindo (a)!')}}</p>
            <p>{{__('A Novartis Biociências S/A está promovendo a segunda edição do Programa de Educação Médica Continuada RM 4.5 de Relatos de Casos Clínicos 2019.')}}</p>
            <p>{{__('Esse programa envolve o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de nilotinibe, com resposta RM4.5, por grupos de trabalho organizados pela Novartis, contendo médicos do Brasil.')}}</p>
            <p>{{__('O intuito é difundir o conhecimento técnico e científico sobre o tratamento da LMC, com uso de nilotinibe e permitir o intercâmbio de boas práticas entre os hematologistas brasileiros.')}}</p>
            <p>{{__('Na próxima guia você encontrará vídeos gravados pelos participantes da primeira edição do programa realizado em 2019. Convidamos você a assistir na íntegra aos relatos!')}}</p>
            <p>{{__('Para mais informações por favor contate Douglas Vivona')}}: <a href='mailto:douglas.vivona@novartis.com'>douglas.vivona@novartis.com</a></p>
        </div>
    </div>
  </div>

@endsection
