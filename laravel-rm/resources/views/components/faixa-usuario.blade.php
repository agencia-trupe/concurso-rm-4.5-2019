@if(Auth::check())
  <div class="faixa-usuario faixa-{{Auth::user()->tipo}}">
    <div class="centralizar">

      <a href="{{ url('/logout') }}" class="btn btn-logout"
         onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          {{__('rm.SAIR')}} [X]
      </a>

      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>

      @if(Auth::user()->isAdmin)
        <a href="{{ url('/log-adesao') }}" class="btn btn-usuarios @if(str_is('usuarios-log-adesao*', Route::currentRouteName())) active @endif ">LOG</a>
        <a href="{{ url('/usuarios') }}" class="btn btn-usuarios @if(str_is('usuarios-lista*', Route::currentRouteName())) active @endif ">{{__('rm.USUARIOS')}}</a>
        <a href="{{ url('/gerenciar-grupos') }}" class="btn btn-usuarios @if(str_is('manage-groups*', Route::currentRouteName())) active @endif ">{{__('rm.GRUPOS')}}</a>
      @endif

      @if(Auth::user()->isCoordenador || Auth::user()->isMedico)
        <a href="{{ url('/grupo') }}" class="btn btn-usuarios @if(str_is('grupo*', Route::currentRouteName())) active @endif ">{{__('rm.INTEGRANTES_DO_GRUPO')}}</a>
      @endif

      <span class="nome">
        @if(Auth::user()->isAdmin)
          <strong>{{__('rm.ADMINISTRADOR')}}</strong>
        @else
          {{__('rm.OLA_DR')}} {{Auth::user()->nome}}
        @endif
      </span>

    </div>
  </div>
@endif
