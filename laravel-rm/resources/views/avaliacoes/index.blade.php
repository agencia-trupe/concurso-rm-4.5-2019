@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-avaliacoes com-recuoo" id="app-vue-avaliador">
    <div class="centralizar">

      <h2>{{__('rm.RELATOS_DE_CASOS_CLINICOS_PARA_AVALIACAO')}}</h2>

      <div class="contem-colunas">
        <div class="coluna coluna-50">
          <div class="texto-avaliacao">
            <p>
              {{__('Insira sua pontuação de 1 a 10 (número inteiro com uma casa decimal) em cada critério dos casos clínicos para sua avaliação')}}
            </p>
            <p>
              {!!__("Você pode editar as notas, basta preencher novamente e clicar em \"salvar\". Essa edição é possível até o dia <strong>:data_max_avaliacao</strong>. Após isso, o sistema será bloqueado e as notas salvas serão as consideradas.", ['data_max_avaliacao' => $dataMaxAValiacao])!!}
            </p>
          </div>
        </div>
        <div class="coluna coluna-50">
          <div class="destaque">
            <p>
              {{__('Os critérios de avaliação são (Pontuação total possível por avaliador: 100 pontos)')}}
            </p>
            <ol>
              <li>{{__('Título 1-10 pontos (peso 1)')}}</li>
              <li>{{__('Relato do caso clínico 1-10 pontos (peso 3)')}}</li>
              <li>{{__('Discussão e Conclusão 1-10 pontos (peso 2)')}}</li>
            </ol>
          </div>
        </div>
      </div>

      <p class="titulo-categoria">
        <span>{{__('rm.CATEGORIA')}} 1</span>
      </p>

      <div class="descricao-categoria">
        <span>{{__('rm.CATEGORIA_1_TITULO')}}</span>
        <p>
            {{__('rm.CATEGORIA_1_TEXTO')}}
        </p>
      </div>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria1.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria1.length == 0" v-cloak class="linha">
          <div class="nenhum">{{__('rm.NENHUM_CASO_PARA_AVALIACAO_NESSA_CATEGORIA')}}</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria1" 
              :key="linha.id" 
              :linha="linha" 
              criterio-label="{{__('rm.CRITERIO')}}" 
              salvar-label="{{__('rm.SALVAR')}}" 
              sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
              erro-notas="{{__('rm.ERRO_NOTAS')}}"
              erro-prazo="{{__('rm.ERRO_PRAZO')}}"
              v-on:mostraralerta="mostrarAlerta($event)"
              @sucesso="buscarAvaliacoes">
        </linha>
      </div>

      <p class="titulo-categoria">
        <span>{{__('rm.CATEGORIA')}} 2</span>
      </p>

      <div class="descricao-categoria">
        <span>{{__('rm.CATEGORIA_2_TITULO')}}</span>
        <p>
          {{__('rm.CATEGORIA_2_TEXTO')}}
        </p>
      </div>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria2.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria2.length == 0" v-cloak class="linha">
          <div class="nenhum">{{__('rm.NENHUM_CASO_PARA_AVALIACAO_NESSA_CATEGORIA')}}</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria2" 
              :key="linha.id" 
              :linha="linha" 
              criterio-label="{{__('rm.CRITERIO')}}"
              salvar-label="{{__('rm.SALVAR')}}"
              sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
              erro-notas="{{__('rm.ERRO_NOTAS')}}"
              erro-prazo="{{__('rm.ERRO_PRAZO')}}"
              v-on:mostraralerta="mostrarAlerta($event)"
              @sucesso="buscarAvaliacoes">
        </linha>
      </div>

      <p class="titulo-categoria">
        <span>{{__('rm.CATEGORIA')}} 3</span>
      </p>

      <div class="descricao-categoria">
        <span>{{__('rm.CATEGORIA_3_TITULO')}}</span>
        <p>
          {{__('rm.CATEGORIA_3_TEXTO')}}
        </p>
      </div>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria3.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria3.length == 0" v-cloak class="linha">
          <div class="nenhum">{{__('rm.NENHUM_CASO_PARA_AVALIACAO_NESSA_CATEGORIA')}}</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria3"
              :key="linha.id"
              :linha="linha"
              criterio-label="{{__('rm.CRITERIO')}}"
              salvar-label="{{__('rm.SALVAR')}}"
              sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
              erro-notas="{{__('rm.ERRO_NOTAS')}}"
              erro-prazo="{{__('rm.ERRO_PRAZO')}}"
              v-on:mostraralerta="mostrarAlerta($event)"
              @sucesso="buscarAvaliacoes">
        </linha>
      </div>

      <p class="titulo-categoria">
        <span>{{__('rm.CATEGORIA')}} 4</span>
      </p>

      <div class="descricao-categoria">
          <span>{{__('rm.CATEGORIA_4_TITULO')}}</span>
          <p>
            {{__('rm.CATEGORIA_4_TEXTO')}}
          </p>
      </div>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria4.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria4.length == 0" v-cloak class="linha">
          <div class="nenhum">{{__('rm.NENHUM_CASO_PARA_AVALIACAO_NESSA_CATEGORIA')}}</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria4"
              :key="linha.id"
              :linha="linha"
              criterio-label="{{__('rm.CRITERIO')}}"
              salvar-label="{{__('rm.SALVAR')}}"
              sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
              erro-notas="{{__('rm.ERRO_NOTAS')}}"
              erro-prazo="{{__('rm.ERRO_PRAZO')}}"
              v-on:mostraralerta="mostrarAlerta($event)"
              @sucesso="buscarAvaliacoes">
        </linha>
      </div>


      <!-- HISTÓRICO -->
      <div v-if="avaliacoes.historico.categoria1.length > 0 || avaliacoes.historico.categoria2.length > 0 || avaliacoes.historico.categoria3.length > 0 || avaliacoes.historico.categoria4.length > 0">

        <h2 class="titulo-historico">{{__('rm.HISTORICO_DE_AVALIACOES')}}</h2>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria1.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria1.length == 0" v-cloak class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_AVALIADO_NA_CATEGORIA')}} 1</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria1"
                          :key="linha.id"
                          :cat="'{{__('rm.CATEGORIA_1_TITULO')}}'"
                          :linha="linha"
                          criterio-label="{{__('rm.CRITERIO')}}"
                          salvar-label="{{__('rm.REAVALIAR')}}"
                          sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
                          erro-notas="{{__('rm.ERRO_NOTAS')}}"
                          erro-prazo="{{__('rm.ERRO_PRAZO')}}"
                          v-on:mostraralerta="mostrarAlerta($event)"
                          @sucesso="buscarAvaliacoes">
          </linha>
        </div>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria2.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria2.length == 0" v-cloak class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_AVALIADO_NA_CATEGORIA')}} 2</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria2"
                          :key="linha.id"
                          :cat="'{{__('rm.CATEGORIA_2_TITULO')}}'"
                          :linha="linha"
                          criterio-label="{{__('rm.CRITERIO')}}"
                          salvar-label="{{__('rm.REAVALIAR')}}"
                          sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
                          erro-notas="{{__('rm.ERRO_NOTAS')}}"
                          erro-prazo="{{__('rm.ERRO_PRAZO')}}"
                          v-on:mostraralerta="mostrarAlerta($event)"
                          @sucesso="buscarAvaliacoes">
          </linha>
        </div>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria3.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria3.length == 0" v-cloak class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_AVALIADO_NA_CATEGORIA')}} 3</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria3"
                          :key="linha.id"
                          :cat="'{{__('rm.CATEGORIA_3_TITULO')}}'"
                          :linha="linha"
                          criterio-label="{{__('rm.CRITERIO')}}"
                          salvar-label="{{__('rm.REAVALIAR')}}"
                          sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
                          erro-notas="{{__('rm.ERRO_NOTAS')}}"
                          erro-prazo="{{__('rm.ERRO_PRAZO')}}"
                          v-on:mostraralerta="mostrarAlerta($event)"
                          @sucesso="buscarAvaliacoes">
          </linha>
        </div>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria4.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria4.length == 0" v-cloak class="linha">
            <div class="nenhum">{{__('rm.NENHUM_CASO_AVALIADO_NA_CATEGORIA')}} 4</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria4"
                          :key="linha.id"
                          :cat="'{{__('rm.CATEGORIA_4_TITULO')}}'"
                          :linha="linha"
                          criterio-label="{{__('rm.CRITERIO')}}"
                          salvar-label="{{__('rm.REAVALIAR')}}"
                          sempontos-label="{{__('rm.SEM_PONTUACAO')}}"
                          erro-notas="{{__('rm.ERRO_NOTAS')}}"
                          erro-prazo="{{__('rm.ERRO_PRAZO')}}"
                          v-on:mostraralerta="mostrarAlerta($event)"
                          @sucesso="buscarAvaliacoes">
          </linha>
        </div>

      </div>


    </div>

    <div class="modal" v-show="alerta.mostrar" v-cloak>
      <div class="backdrop" @click.stop="fecharModal"></div>
      <div class="modal-conteudo caso-info">
        <div class="confirm-box">
          <p class="code"><strong>@{{alerta.codigo}}</strong></p>
          <div class="lista-dados">
            <p>
              {{__('rm.DATA')}}: @{{alerta.caso.sent_at_f}}
            </p>
            
            <p>
              <strong>
                @{{alerta.caso.categoria_titulo}}
              </strong>
            </p>

            <p>
              <strong>
                @{{alerta.caso.categoria_texto}}
              </strong>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.TITULO_RELATO_PT')}}
              </strong>
            </p>
            
            <p>
              @{{alerta.caso.titulo_pt}}
            </p>
            
            <p>
              <strong>
                {{__('rm.TITULO_RELATO_EN')}}
              </strong>
            </p>
            
            <p>
              @{{alerta.caso.titulo_en}}
            </p>
            
            <hr>
            
            <p>
              <strong>{{__('rm.DADOS_PACIENTE')}}</strong>
            </p>
            <p>
              <strong>{{__('rm.SEXO')}}</strong>: @{{alerta.caso.sexo_paciente}}
            </p>
            <p>
              <strong>{{__('rm.IDADE')}}</strong>: @{{alerta.caso.idade_paciente}}
            </p>
            <p>
              <strong>{{__('rm.DOENCA_DIAGNOSTICADA')}}</strong>: {{__('rm.LEUCEMIA')}}
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.RELATO_PT')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.relato_pt"></span>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.RELATO_EN')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.relato_en"></span>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.DISCUSSAO_PT')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.discussao_pt"></span>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.DISCUSSAO_EN')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.discussao_en"></span>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.EVOLUCAO')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.evolucao"></span>
            </p>
            
            <hr>
            
            <p>
              <strong>
                {{__('rm.REFERENCIAS')}}
              </strong>
            </p>
            
            <p>
              <span v-html="alerta.caso.referencias"></span>
            </p>
            
            <hr>
            
            <p>
              {{__('Material produzido em março de 2019. Material destinado a médicos participantes do Programa de Educação Médica RM 4,5 de casos clínicos 2019.')}}
            </p>
            <p>
              <strong>{{__('TS RESUMO PROGRAMA DE EDUCAÇÃO MÉDICA 0,25 0118 BR-04615')}}<strong>
            </p>
          </div> 
          <div class="controles">
            <a href="#" @click.prevent="fecharModal" class="botao-voltar">{{__('rm.CANCELAR')}}</a>
          </div>
        </div>
      </div>
    </div>
      
  </div>

@endsection
