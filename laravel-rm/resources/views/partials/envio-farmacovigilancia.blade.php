<div class="envio-farmaco">
    <p>
        {{__('rm.ENCAMINHAR_CASOS_CLINICOS')}}
    </p>
    <form action="encaminhar-casos" method='post'>
        {!!csrf_field()!!}
        <input type="text" name="data" placeholder="{{__('rm.DATA')}}" required class="datepicker">
        <input type="submit" value="{{__('rm.ENVIAR')}}">
    </form>
</div>