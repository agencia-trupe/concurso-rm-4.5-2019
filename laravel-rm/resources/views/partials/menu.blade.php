@if(Auth::check() && (!isset($hideMenu) || !$hideMenu))
  <nav>
    <div class="centralizar">
      @if( Auth::user()->isCoordenador || Auth::user()->isMedico )

        <ul class="menu-coordenador">
          <li>
            <a href="como-funciona" @if(Route::currentRouteName() == 'como-funciona') class="ativo" @endif title="{{__('rm.COMO_FUNCIONA')}}"><span>{{__('rm.COMO_FUNCIONA')}}</span></a>
          </li>
          <li>
            <a href="regulamento" @if(Route::currentRouteName() == 'regulamento') class="ativo" @endif title="{{__('rm.REGULAMENTO')}}"><span>{{__('rm.REGULAMENTO')}}</span></a>
          </li>
          <li>
            <a href="cronograma" @if(Route::currentRouteName() == 'cronograma') class="ativo" @endif title="{{__('rm.CRONOGRAMA')}}"><span>{{__('rm.CRONOGRAMA')}}</span></a>
          </li>
          <li>
            <a href="avaliadores" @if(Route::currentRouteName() == 'avaliadores') class="ativo" @endif title="{{__('rm.AVALIADORES')}}"><span>{{__('rm.AVALIADORES')}}</span></a>
          </li>
          <li>
            <a href="submeter-caso" @if(Route::currentRouteName() == 'submeter-caso' || Route::currentRouteName() == 'preencher-caso') class="ativo" @endif title="{{__('rm.SUBMISSAO_DO_CASO_CLINICO')}}"><span>{{__('rm.SUBMISSAO_DO_CASO_CLINICO')}}</span></a>
          </li>
          <li>
            <a href="casos-2018" @if(Route::currentRouteName() == 'blog-listagem' || Route::currentRouteName() == 'blog-detalhe') class="ativo" @endif title="{{__('rm.BLOG')}}"><span>{{__('rm.BLOG')}}</span></a>
          </li>
        </ul>

      @elseif( Auth::user()->isAvaliador )

        <ul class="menu-avaliador">
          <li>
            <a href="como-funciona" @if(Route::currentRouteName() == 'como-funciona') class="ativo" @endif title="{{__('rm.COMO_FUNCIONA')}}"><span>{{__('rm.COMO_FUNCIONA')}}</span></a>
          </li>
          <li>
            <a href="regulamento" @if(Route::currentRouteName() == 'regulamento') class="ativo" @endif title="{{__('rm.REGULAMENTO')}}"><span>{{__('rm.REGULAMENTO')}}</span></a>
          </li>
          <li>
            <a href="cronograma" @if(Route::currentRouteName() == 'cronograma') class="ativo" @endif title="{{__('rm.CRONOGRAMA')}}"><span>{{__('rm.CRONOGRAMA')}}</span></a>
          </li>          
          <li>
            <a href="avaliacoes" @if(Route::currentRouteName() == 'avaliacoes') class="ativo" @endif title="{{__('rm.AVALIACOES')}}"><span>{{__('rm.AVALIACOES')}}</span></a>
          </li>
        </ul>

      @elseif( Auth::user()->isAdmin )

        <ul class="menu-admin">
          <li>
          <a href="novos-casos" @if(Route::currentRouteName() == 'novos-casos') class="ativo" @endif title="{{__('rm.NOVOS_CASOS_CLINICOS_MAIS_ENVIO')}}"><span>{!!__('rm.NOVOS_CASOS_CLINICOS_MAIS_ENVIO')!!}</span></a>
          </li>
          <li>
          <a href="historico" @if(Route::currentRouteName() == 'historico') class="ativo" @endif title="{{__('rm.HISTORICO_DE_AVALIACOES')}}"><span>{!!__('rm.HISTORICO_DE_AVALIACOES')!!}</span></a>
          </li>
          <li>
            <a href="status-envios" @if(Route::currentRouteName() == 'status-envios') class="ativo" @endif title="{{__('rm.STATUS_DE_ENVIO_DE_COORDENADORES')}}"><span>{!!__('rm.STATUS_DE_ENVIO_DE_COORDENADORES')!!}</span></a>
          </li>
          <li>
            <a href="ranking" @if(Route::currentRouteName() == 'ranking') class="ativo" @endif title="{{__('rm.RESULTADO')}}"><span>{{__('rm.RESULTADO')}}</span></a>
          </li>
        </ul>
      
      @elseif( Auth::user()->isEditor )

        <ul class="menu-admin">
          <li>
            <a href='blog' @if(Route::currentRouteName() == 'blog.index') class="ativo" @endif title="{{__('rm.BLOG')}}"><span>{!!__('rm.BLOG')!!}</span></a>
          </li>
          <li>
            <a href='blog/create' @if(Route::currentRouteName() == 'blog.create' || Route::currentRouteName() == 'blog.edit') class="ativo" @endif title="{{__('rm.BLOG_INSERIR')}}"><span>{!!__('rm.BLOG_INSERIR')!!}</span></a>
          </li>
        </ul>      
      
      @elseif( Auth::user()->isMedicoNaoParticipante )
        
        <ul class="menu-coordenador">
          <li>
            <a href="apresentacao" @if(Route::currentRouteName() == 'apresentacao') class="ativo" @endif title="{{__('rm.APRESENTACAO')}}"><span>{{__('rm.APRESENTACAO')}}</span></a>
          </li>
          <li>
            <a href="casos-2018" @if(Route::currentRouteName() == 'blog-listagem' || Route::currentRouteName() == 'blog-detalhe') class="ativo" @endif title="{{__('rm.BLOG')}}"><span>{{__('rm.BLOG')}}</span></a>
          </li>
        </ul>

      @endif
    </div>
  </nav>
@endif
