<footer>
  <div class="centralizar">

    <div class="bloco-footer bloco-infomec">
      <img src="images/infomec.png" alt="Infomec">
    </div>

    <div class="bloco-footer bloco-disclaimer">
      <p>
        {{__('Material destinado exclusivamente a profissionais de saúde habilitados a prescrever e/ou dispensar medicamentos.')}}<br>
        {{date('Y')}} &copy; - {{__('Direitos Reservados - Novartis Biociências S.A.')}}<br>
        {{__('Proibida a reprodução total ou parcial sem a autorização do titular.')}}<br>
        {{__('Janeiro/2019')}}.<br>
        {{__('Abril/2019 || Plataforma do Programa de Educação Médica RM 4.5 BR-04739')}}
      </p>
    </div>
    
    <div class="bloco-footer bloco-novartis">
      <img src="images/novartis.png" alt="Novartis">
      <p>
        {{__('Novartis Biociências S.A.')}}<br>
        {{__('Setor Farma - Av. Prof. Vicente Rao, 90')}}<br>
        {{__('São Paulo, SP - CEP 04636-000')}}<br>
        <a href="www.novartis.com.br" target="_blank" title="Novartis">www.novartis.com.br</a><br>
        <a href="www.portalnovartis.com.br" target="_blank" title="Portal Novartis">www.portalnovartis.com.br</a>
        <br><br>
        Infomec<br>
        {{__('Informações Médico Científicas')}}<br>
        <a href="mailto:infomec.novartis@novartis.com" title="Enviar um e-mail">infomec.novartis@novartis.com</a><br>
        0800 888 3003 (fixo)<br>
        11 3253-3405 (cel)<br>
      </p>
    </div>

    <div class="bloco-footer bloco-termos">
      <p>
        {!!__("SIC | Serviço de Informações ao Cliente <br> <a href='mailto:sic.novartis@novartis.com' title='Entrar em contato'>sic.novartis@novartis.com</a>")!!}
        <br><br>
        {!!__("O uso deste site é governado por nossos <br><a href='https://www.novartis.com.br/termos-de-uso' target='_blank' title='Termos de Uso'>Termos de Uso</a> e nosso <a href='aviso-de-privacidade' title='Aviso de Privacidade'>Aviso de Privacidade</a>")!!}
        <br><br>
        &copy; {{date('Y')}} {{__('Novartis Biociências S.A.')}}
      </p>
    </div>

  </div>
</footer>
