@include('components.faixa-usuario')
@include('components.botoes-idioma')

<header>
  <div class="banner">
    <a href="{{route('home')}}" title="{{__('rm.PAGINA_INICIAL')}}">
      <img src="images/cabecalho-rm45-2019.png" alt="{{__('rm.SITE_NAME')}}">
    </a>
  </div>
  
  @include('partials.menu')
</header>
