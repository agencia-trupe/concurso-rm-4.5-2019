<style>
  body {
    font-family: 'Arial,sans-serif';
    color: #555;
    line-height: 1.5;
  }
  h2, h3 { color: #000; }
  hr {
    border: 0;
    border-top: 1px solid #999;
    margin: 2em 0;
  }
  img {
    display: block;
    max-width: 100%;
  }
</style>

<h2>CASO CLÍNICO RECEBIDO – PROGRAMA DE EDUCAÇÃO MÉDICA RM 4.5 2019</h2>
<p>DATA {{ $caso->enviado_em->format('d/m/Y') }}</p>

@if($cabecalho)
<p>
  Código do concurso: {{ $caso->codigo }}<br>
  Nome do concurso: RM 4,5 2019<br>
  Nome do médico relator: {{ $caso->coordenador->nome }}<br>
  E-mail do médico relator: {{ $caso->coordenador->email }}<br>
</p>

<hr>
@endif

<h3>{{ 'CATEGORIA '.$caso->categoria.' '.__('rm.CATEGORIA_'.$caso->categoria.'_TITULO') }}</h3>
<p>{{ __('rm.CATEGORIA_'.$caso->categoria.'_TEXTO') }}</p>

<hr>

<p><strong>TÍTULO DO RELATO DO CASO CLÍNICO - PORTUGUÊS:</strong></p>
<p>{{ $caso->titulo_pt }}</p>

<p><strong>TÍTULO DO RELATO DO CASO CLÍNICO - INGLÊS:</strong></p>
<p>{{ $caso->titulo_en }}</p>

<hr>

<p>
  <strong>DADOS DO PACIENTE</strong><br>
  <strong>Sexo</strong>: {{ $caso->sexo_paciente }}<br>
  <strong>Idade</strong>: {{ $caso->idade_paciente }}<br>
  <strong>Doença diagnosticada</strong>: Leucemia Mieloide Crônica (LMC) com resposta RM 4.5
</p>

<hr>

<p><strong>RELATO DE CASO CLÍNICO - PORTUGUÊS:</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->relato_pt) !!}

<hr>

<p><strong>RELATO DE CASO CLÍNICO - INGLÊS:</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->relato_en) !!}

<hr>

<p><strong>DISCUSSÃO E CONCLUSÃO - PORTUGUÊS:</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->discussao_pt) !!}

<hr>

<p><strong>DISCUSSÃO E CONCLUSÃO - INGLÊS:</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->discussao_en) !!}

<hr>

<p><strong>INDIQUE A EVOLUÇÃO DE EXAMES LABORATORIAIS OU OUTROS EVENTOS QUE ACHAR RELEVANTES (OPCIONAL):</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->evolucao) !!}

<hr>

<p><strong>REFERÊNCIAS COMPLETAS:</strong></p>
{!! preg_replace('/https?:\/\/www.rm45.com.br\/images\/uploads\//', 'images/uploads/', $caso->referencias) !!}

<hr>

<p>
  Material produzido em março de 2019.<br>
  Material destinado a médicos participantes do Programa de Educação Médica RM 4,5 de casos clínicos 2019.
</p>

<p><strong>TS RESUMO PROGRAMA DE EDUCAÇÃO MÉDICA 0,25 0118 BR-04615</strong></p>

<p>
  Atenciosamente,<br>
  Comissão do Concurso RM4,5
</p>
