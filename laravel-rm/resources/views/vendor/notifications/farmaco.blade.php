@component('mail::message')
# CASO CLÍNICO RECEBIDO – PROGRAMA DE EDUCAÇÃO MÉDICA RM 4.5 2019

DATA {{$data}}

## {{$categoria_titulo}}

{{$categoria_texto}}

<hr>

**{{$label1}}**

{{$value1}}

**{{$label2}}**

{{$value2}}

<hr>

**DADOS DO PACIENTE**
**Sexo**: {{$sexo}}  
**Idade**: {{$idade}}  
**Doença diagnosticada**: {{$doenca}}  

<hr>

**{!!$label3!!}**

{!!$value3!!}

<hr>

**{!!$label4!!}**

{!!$value4!!}

<hr>

**{!!$label5!!}**

{!!$value5!!}

<hr>

**{!!$label6!!}**

{!!$value6!!}

<hr>

**{!!$label7!!}**

{!!$value7!!}

<hr>

**{!!$label8!!}**

{!!$value8!!}

<hr>

Material produzido em março de 2019. Material destinado a médicos participantes do Programa de Educação Médica RM 4,5 de casos clínicos 2019.

**TS RESUMO PROGRAMA DE EDUCAÇÃO MÉDICA 0,25 0118 BR-04615**

Atenciosamente,<br>
Comissão do Concurso RM4,5
@endcomponent
