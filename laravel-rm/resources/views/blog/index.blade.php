@extends('template.index')

@section('conteudo')

    <div class="lista-blog">
        <div class="centralizar">
            <h1>{{__('rm.BLOG')}}</h1>

            @if(session('sucesso'))
                <p class="alerta alerta-sucesso auto-close">
                    {{session('sucesso')}}
                </p>
            @endif
            
            <table class="table-sortable" id="table-sortable">
                @forelse($list as $post)
                    <tr class="tr-row" id="row_{{$post->id}}">
                        <td class='order'>
                            <a href="#" title="{{__('rm.ORDENAR')}}" class="btn-move">
                                <img src="images/ico-mover.png" alt="{{__('rm.ORDENAR')}}">
                            </a>
                        </td>
                        <td class='title'>{{$post->titulo or $post->titulo_caso_clinico}}</td>
                        <td class='edit'>
                            <a href="{{route('blog.edit', ['id' => $post->id])}}" title="{{__('rm.EDITAR')}}">
                                <img src="images/ico-editar.png" alt="{{__('rm.EDITAR')}}"> {{__('rm.EDITAR')}}
                            </a>
                        </td>
                        <td class='remove'>
                            <form action="{{route('blog.destroy', ['id' => $post->id])}}" method="POST">
                                {!!csrf_field()!!}
                                <input type="hidden" name="_method" value="DELETE">
                                <button title="{{__('rm.EDITAR')}}">
                                    <img src="images/ico-excluir.png" alt="{{__('rm.EDITAR')}}"> {{__('rm.EXCLUIR')}}
                                </button>
                            </form>                            
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td><p class="nenhum-post">{{__('rm.NENHUM_POST')}}</p></td>
                    </tr>
                @endforelse
            </table>
        </div>
    </div>

@endsection
