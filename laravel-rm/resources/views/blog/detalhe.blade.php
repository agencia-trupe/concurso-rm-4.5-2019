@extends('template.index')

@section('conteudo')

<div class="detalhe-blog">
  <div class="centralizar">
    <h1>{{__('rm.BLOG')}}</h1>

    <a href="casos-2018" class="outros-videos">
      <img src="images/ico-voltar.png"> {{__('rm.OUTROS_VIDEOS')}}
    </a>
    
    <div class="coluna-blog">
      @if($post->embed)
        <div class="embed">
          {!!$post->embed!!}
        </div>
      @endif
      <h2>{{$post->titulo}}</h2>
      <div class="texto">
        <p>
          {!!nl2br($post->texto)!!}
        </p>
      </div>

      @if ($post->titulo_caso_clinico)
        <div class="texto-caso-clinico">
          <h2>{{$post->titulo_caso_clinico}}</h2>
          <div class="texto">{!!nl2br($post->texto_caso_clinico)!!}</div>
        </div>
      @endif
    </div>
  </div>
</div>

@endsection
