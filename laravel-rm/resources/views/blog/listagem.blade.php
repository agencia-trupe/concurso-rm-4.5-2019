@extends('template.index')

@section('conteudo')

<div class="lista-blog">
  <div class="centralizar blog-cell-list">
    <div class="blog-cell">
      <h1>{{__('rm.CONTEUDO_RELACIONADO')}}</h1>
      <p>{{__('rm.INSPIRE-SE')}}</p>
    </div>
    @forelse($list as $post)
      <div class="blog-cell">
        <a href="casos-2018/{{$post->id}}" title="{{$post->titulo}}">
          @if($post->embed)
            <div class="blog-thumb-container">
              <div class="blog-thumb-wrapper">
                  <div class="blog-thumb">
                    {!!youtube_embed_to_thumbnail($post->embed)!!}
                  </div>
              </div>
            </div>
          @endif
          <h2>{{$post->titulo or $post->titulo_caso_clinico}}</h2>
        </a>
      </div>
    @empty
        <tr>
            <td><p class="nenhum-post">{{__('rm.NENHUM_POST')}}</p></td>
        </tr>
    @endforelse
  </div>
</div>

@endsection
