@extends('template.index')

@section('conteudo')

    <div class="lista-blog">
        <div class="centralizar">
            <h1>{{__('rm.BLOG_INCLUIR_EDITAR')}}</h1>

            @if($errors->any())
                <p class="alerta alerta-erro">
                    {{$errors->first()}}
                </p>
            @endif

            <form action="{{route('blog.update', ['id' => $post->id])}}" method="post" class="post">
                {!!csrf_field()!!}
                <input type="hidden" name="_method" value="PUT">
                <input type="text" name="titulo" placeholder="{{__('rm.BLOG_TITULO')}}" value="{{$post->titulo}}">
                <input type="text" name="embed" placeholder="{{__('rm.BLOG_EMBED')}}" value="{{$post->embed}}">
                <textarea name="texto" placeholder="{{__('rm.BLOG_DESCRITIVO')}}">{{$post->texto}}</textarea>
                <h2 class="section-title">Caso Clínico</h2>
                <input type="text" name="titulo_caso_clinico" placeholder="{{__('rm.TITULO')}}" value="{{$post->titulo_caso_clinico}}">
                <div id="editor">{!!$post->texto_caso_clinico!!}</div>
                <input type="hidden" name="texto_caso_clinico" id="texto_caso_clinico">
                <input type="submit" value="{{__('rm.BLOG_SALVAR')}}">
            </form>
        </div>
    </div>

@endsection
