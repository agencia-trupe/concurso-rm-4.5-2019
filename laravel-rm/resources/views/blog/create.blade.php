@extends('template.index')

@section('conteudo')

    <div class="lista-blog">
        <div class="centralizar">
            <h1>{{__('rm.BLOG_INCLUIR_EDITAR')}}</h1>

            @if($errors->any())
                <p class="alerta alerta-erro">
                    {{$errors->first()}}
                </p>
            @endif

            <form action="{{route('blog.store')}}" method="post" class="post">
                {!!csrf_field()!!}
                <h2>Vídeo</h2>
                <input type="text" name="titulo" placeholder="{{__('rm.BLOG_TITULO')}}">
                <input type="text" name="embed" placeholder="{{__('rm.BLOG_EMBED')}}">
                <textarea name="texto" placeholder="{{__('rm.BLOG_DESCRITIVO')}}"></textarea>
                <h2 class="section-title">Caso Clínico</h2>
                <input type="text" name="titulo_caso_clinico" placeholder="{{__('rm.TITULO')}}">
                <div id="editor"></div>
                <input type="hidden" name="texto_caso_clinico" id="texto_caso_clinico">
                <input type="submit" value="{{__('rm.BLOG_SALVAR')}}">
            </form>
        </div>
    </div>

@endsection
