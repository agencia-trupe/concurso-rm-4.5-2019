Vue.component('linha-editavel', {
  template : `

  <div class="linha editavel" :class="classeComputada">
    <div class="avaliacao-codigo" :data-label=" 'CATEGORIA ' + this.cat ">
      <span>{{linha.caso.codigo}}</span>
    </div>
    <div class="avaliacao-criterio" :data-label="criterioLabel + ' ' + 1">
      <div class="input">
        <div class="padding">
          <input type="text" name="criterio_1" required data-next="2" :value="linha.criterio_1">
          <span></span>
        </div>
      </div>
    </div>
    <div class="avaliacao-criterio" :data-label="criterioLabel + ' ' + 2">
      <div class="input">
        <div class="padding">
          <input type="text" name="criterio_2" required data-next="3" :value="linha.criterio_2">
          <span></span>
        </div>
      </div>
    </div>
    <div class="avaliacao-criterio" :data-label="criterioLabel + ' ' + 3">
      <div class="input">
        <div class="padding">
          <input type="text" name="criterio_3" required data-next="4" :value="linha.criterio_3">
          <span></span>
        </div>
      </div>
    </div>
    <div class="avaliacao-criterio" @click.prevent="enviarNota">
      <a href="#" title="SALVAR" class="btn-salvar" v-bind:class="{'enviando' : isEnviando, 'sucesso' : isSucesso}">
        <span>{{salvarLabel}}</span><i class='loader'></i><i class='sucesso'></i>
      </a>
    </div>
    <div class="erro-linha" v-if="erros.mostrar">{{erros.msg}}</div>
  </div>


  `,
  props : [
    'linha', 
    'cat',
    'criterioLabel',
    'salvarLabel',
    'sempontosLabel',
    'erroNotas',
    'erroPrazo'
  ],
  computed: {
    classeComputada : function(){
      return 'linha_' + this.linha.id;
    }
  },
  data() {
    return {
      erros: {
        mostrar : false,
        msg : ''
      },
      isEnviando : false,
      isSucesso : false
    }
  },
  mounted(){
    this.ativarMascaraNota();
  },
  methods: {
    enviarNota(){

      this.isEnviando = true;
      this.erros.mostrar = false;
      var c = this.classeComputada;
      var c1 = $("."+c+" .avaliacao-criterio input[type='text'][name='criterio_1']:not(:disabled)").val();
      var c2 = $("."+c+" .avaliacao-criterio input[type='text'][name='criterio_2']:not(:disabled)").val();
      var c3 = $("."+c+" .avaliacao-criterio input[type='text'][name='criterio_3']:not(:disabled)").val();

      this.linha.criterio_1 = c1;
      this.linha.criterio_2 = c2;
      this.linha.criterio_3 = c3;

      var notas = {
        criterio_1 : c1,
        criterio_2 : c2,
        criterio_3 : c3,
        avaliacao : this.linha.id
      };

      axios.post('enviar-notas', {
        notas : notas
      }).then(resposta => this.notaEnviada(resposta))
      .catch(errors => this.erroAoEnviar(errors));
    },
    notaEnviada(resposta){
      var t = this;
      setTimeout( function(){

        t.isSucesso = true;

        setTimeout( function(){
          t.isEnviando = false;
          t.isSucesso = false;
        }, 750);

      }, 300);
    },
    erroAoEnviar(errors){
      this.erros.mostrar = true;
      if (
        errors.response.data['notas.criterio_1'] ||
        errors.response.data['notas.criterio_2'] ||
        errors.response.data['notas.criterio_3']
      ) {
        this.erros.msg = erroNotas;
      } else {
        this.erros.msg = erroPrazo;
      }
      this.isEnviando = false;
    },
    proximoCampo(campo_atual){
      var linha = $("."+this.classeComputada);
      var destino = campo_atual.attr('data-next');
      if(destino){
        if(destino == 'fim')
          linha.find(".btn-salvar").focus();
        else
          linha.find("[name='criterio_"+campo_atual.attr('data-next')+"']").focus();
      }
    },
    ativarMascaraNota(){
      var c = this.classeComputada;
      var inputs = $("."+c+" .avaliacao-criterio input[type='text']");
      var t = this;
      inputs.mask('Z0.0',{
        reverse: true,
        translation : {
          'Z': {pattern: /1/, optional: true},
        },
        onComplete: function(cep, event, currentField, options) {
          if(currentField.val() <= 10)
            t.proximoCampo(currentField);
        }
      }).keypress( function(event){

        if(event.keyCode == 13){
          t.proximoCampo($(this));
          return false;
        }

      }).focus( function(event){

        $(this).select();
        t.erros.mostrar = false;

      }).blur( function(){

        if($(this) != '' && $(this).val() >= 10)
          $(this).val('10.0');

        if($(this) != '' && $(this).val() <= 1)
          $(this).val('1.0');

      });
    }
  }
});
