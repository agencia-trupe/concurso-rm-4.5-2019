
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./appCoordenador');
require('./appAdmin');
require('./appAvaliador');
require('./jquery-ui-1.12.1.custom/jquery-ui.min.js')

jQuery(function($){

  $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };

  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

let activationLinkModal = {
  element: $('#activation-link-modal'),
  input: $('#activation-link-text'),
  show: function(activation_link) {
    this.input.val(activation_link);
    this.element.show();
    $('body').css('overflow', 'hidden');
  },
  hide: function() {
    $('body').css('overflow', 'auto');
    this.element.hide();
  }
}

$(document).ready( function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })

  if($('.auto-close').length){
    setTimeout( function(){
      $('.auto-close').fadeOut('normal');
    }, 2000);
  }

  $('table.table-sortable tbody').sortable({
    update : function () {
      let serial = [];
      $(this).children('tr.tr-row').each(function(idx, elm) {
        serial.push(elm.id.split('_')[1])
      });
      $.post('blog/reorder', {
        data : serial
      });
    },
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    handle : $('.btn-move'),
    items : '.tr-row'
  });

	$('.btn-move').click( function(e){e.preventDefault();});

  $('.datepicker').datepicker();

  $('#group-filter-input').keyup( function () {
    let rows = $('table.group-table tbody tr')
    let search = $(this).val()
    if (search != '') {
      rows.hide().filter(function (i, v) {
        let cells = $(this).find('td').text().toLowerCase()
        return cells.includes(search.toLowerCase())
      }).show();
    } else {
      rows.show();
    }
  })

  if ($('#editor').length) {
    new Quill('#editor', {
      theme: 'snow',
      placeholder: 'TEXTO DO CASO CLÍNICO',
      modules: {
        toolbar: [
          [{ header: [1, 2, false] }],
          [{ align: ['', 'center', 'right']}],
          ['bold', 'underline', 'strike', 'link', 'blockquote'],
          [{ 'list': 'ordered'}, { 'list': 'bullet' }]
        ]
      }
    });
  }

  if ($('.img-editor').length) {
    quillImgEditor = []
    $('.img-editor').each( function(index, item) {
      quillImgEditor[index] = new Quill(item, {
        theme: 'snow',
        modules: {
          toolbar: [
            'image'
          ],
          imageUpload: {
            upload: file => {
              return new Promise((resolve, reject) => {
                const fd = new FormData();
                const xhr = new XMLHttpRequest();
                let token = document.querySelector('meta[name="csrf-token"]').content

                fd.append("upload_file", file);
                xhr.open("POST", `/quill-img-upload`, true);
                xhr.setRequestHeader('X-CSRF-TOKEN', token);                
                xhr.onload = () => {
                  if (xhr.status === 200) {
                    const response = JSON.parse(xhr.responseText);
                    resolve(response.file_path);
                  }
                };
                xhr.send(fd);
              });
            }
          },
        }
      });

      quillImgEditor[index]['limit'] = $(item).attr('data-maxlength')
      if (quillImgEditor[index]['limit'] > 0) {
        quillImgEditor[index].on('text-change', function (delta, old, source) {
          characters = $(quillImgEditor[index].container).text().length
          if (characters > quillImgEditor[index]['limit']) {
            quillImgEditor[index].deleteText(quillImgEditor[index]['limit'], characters);
          }
        });
      }
    });
  }

  if ($('.readonly-editor').length) {
    quillReadOnly = []
    $('.readonly-editor').each( function(index, item) {
      quillReadOnly[index] = new Quill(item, {
        theme: 'snow',
        readOnly: true,
        modules: {
          toolbar: [],
        }
      });
    });
  }

  $(".lista-blog form").on("submit", function () {
    $('#texto_caso_clinico').val($('.ql-editor').html());
  });

  $(".form-caso").on("submit", function () {
    empty_content = '<p><br></p>'
    relato_pt = $('#wrapper-relato_pt .ql-editor').html()
    relato_en = $('#wrapper-relato_en .ql-editor').html()
    discussao_pt = $('#wrapper-discussao_pt .ql-editor').html()
    discussao_en = $('#wrapper-discussao_en .ql-editor').html()
    evolucao = $('#wrapper-evolucao .ql-editor').html()
    referencias = $('#wrapper-referencias .ql-editor').html()
    if (relato_pt != empty_content) { $('#input-relato_pt').val(relato_pt)}
    if (relato_en != empty_content) { $('#input-relato_en').val(relato_en) }
    if (discussao_pt != empty_content) { $('#input-discussao_pt').val(discussao_pt) }
    if (discussao_en != empty_content) { $('#input-discussao_en').val(discussao_en) }
    if (evolucao != empty_content) { $('#input-evolucao').val(evolucao) }
    if (referencias != empty_content) { $('#input-referencias').val(referencias) }
  });

  $('.activation-link-modal-open').click( function(e) {
    e.preventDefault();
    let activation_link = $(this).attr('activation-link');
    activationLinkModal.show(activation_link);
  });

  $('#activation-link-backdrop').click( function(e) {
    activationLinkModal.hide();
  });

  new ClipboardJS('#activation-link-btn');
});
