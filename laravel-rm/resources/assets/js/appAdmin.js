if(document.getElementById('app-vue-admin')){

  const app = new Vue({
    el: '#app-vue-admin',
    data() {
      return {
        isLoaded : false,
        disableAction : false,
        alerta : {
          mostrar : false,
          destino : '',
          tipo : '',
          codigo : '',
          caso: {}
        }
      }
    },
    methods: {
      mostrarDados(codigo){
        parent = this
        jQuery.getJSON("case-data/"+codigo, function(case_data) {
          caso = case_data
          parent.mostrarAlerta('dados', caso, codigo)
        });
      },
      mostrarAlerta(msg, caso, codigo){

        $('html, body').stop().animate({
          scrollTop: 0
        }, 300);

        this.alerta.mostrar = true;
        this.alerta.tipo = msg;
        this.alerta.codigo = codigo;

        if(msg == 'distribuir'){
          this.alerta.destino = 'distribuir-caso/' + codigo;
        }else if(msg == 'excluir'){
          this.alerta.destino = 'excluir-caso/' + codigo;
        }else if(msg == 'dados') {
          this.alerta.caso = caso;
        }
      },
      fecharModal(){
        this.alerta.mostrar = false;
      },
      goToDestination(url){
        window.location.href = this.alerta.destino;
      }
    },
    mounted() {
      this.isLoaded = true;
    }
  });

}
