<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute deve ser aceito.',
    'active_url'           => ':attribute não é uma URL válida.',
    'after'                => ':attribute deve ser uma data depois de :date.',
    'after_or_equal'       => ':attribute deve ser uma data depois ou igual a :date.',
    'alpha'                => ':attribute deve conter somente letras.',
    'alpha_dash'           => ':attribute deve conter letras, números e traços.',
    'alpha_num'            => ':attribute deve conter somente letras e números.',
    'array'                => ':attribute deve ser um array.',
    'before'               => ':attribute deve ser uma data antes de :date.',
    'before_or_equal'      => ':attribute deve ser uma data antes ou igual a :date.',
    'between'              => [
      'numeric' => ':attribute deve estar entre :min e :max.',
      'file'    => ':attribute deve estar entre :min e :max kilobytes.',
      'string'  => ':attribute deve estar entre :min e :max caracteres.',
      'array'   => ':attribute deve ter entre :min e :max itens.',
    ],
    'boolean'              => ':attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação de :attribute não confere.',
    'date'                 => ':attribute não é uma data válida.',
    'date_format'          => ':attribute não confere com o formato :format.',
    'different'            => ':attribute e :other devem ser diferentes.',
    'digits'               => ':attribute deve ter :digits dígitos.',
    'digits_between'       => ':attribute deve ter entre :min e :max dígitos.',
    'dimensions'           => ':attribute tem dimensões inválidas.',
    'distinct'             => ':attribute tem valor duplicado.',
    'email'                => ':attribute deve ser um endereço de e-mail válido.',
    'exists'               => 'O :attribute selecionado é inválido.',
    'file'                 => ':attribute deve ser um arquivo.',
    'filled'               => ':attribute é um campo obrigatório.',
    'image'                => ':attribute deve ser uma imagem.',
    'in'                   => ':attribute é inválida.',
    'in_array'             => ':attribute não existe em :other.',
    'integer'              => ':attribute deve ser um inteiro.',
    'ip'                   => ':attribute deve ser um endereço IP válido.',
    'json'                 => ':attribute deve ser uma string com um JSON válido.',
    'max'                  => [
      'numeric' => ':attribute não deve ser maior que :max.',
      'file'    => ':attribute não deve ter mais que :max kilobytes.',
      'string'  => ':attribute não deve ter mais que :max caracteres.',
      'array'   => ':attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => ':attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => ':attribute deve ser do tipo: :values.',
    'min'                  => [
      'numeric' => ':attribute deve ser no mínimo :min.',
      'file'    => ':attribute deve ter no mínimo :min kilobytes.',
      'string'  => ':attribute deve ter no mínimo :min caracteres.',
      'array'   => ':attribute deve ter no mínimo :min itens.',
    ],
    'not_in'               => 'O :attribute selecionado é inválido.',
    'numeric'              => 'O campo :attribute deve ser um número.',
    'present'              => 'O campo :attribute deve estar presente.',
    'regex'                => 'O formato de :attribute é inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_unless'      => 'O campo :attribute é obrigatório a não ser que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values.',
    'same'                 => ':attribute e :other devem ser iguais.',
    'size'                 => [
        'numeric' => ':attribute deve ser :size.',
        'file'    => ':attribute deve ter :size kilobytes.',
        'string'  => ':attribute deve ter :size caracteres.',
        'array'   => ':attribute deve conter :size itens.',
    ],
    'string'               => ':attribute deve ser uma string.',
    'timezone'             => ':attribute deve ser uma timezone válida.',
    'unique'               => ':attribute já está em uso.',
    'uploaded'             => 'O upload de :attribute falhou.',
    'url'                  => 'O formato de :attribute é inválido.',
    'caso_nao_enviado'     => 'Você já realizou o envio do caso desta Categoria.',
    'coordenador_pode_enviar' => 'O prazo máximo para o envio de casos expirou.',
    'nota_valida'          => 'Nota inválida.',
    'categoria_com_duas_opcoes' => 'Terceiro Caso Clínico já enviado em outra categoria',
    'group_unique' => 'Grupo já existe',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
      '_categoria' => 'A categoria de envio',
      'arquivo' => 'O arquivo do caso clínico',
      'autor_principal' => 'O nome do autor principal',
      'password' => 'Senha',
      'check_termos' => 'Estou de acordo com o termo de privacidade do Programa de casos clínicos RM 4.5',
      'titulo_pt' => 'Título do Relato do Caso Clínico - Português',
      'titulo_en' => 'Título do Relato do Caso Clínico - Inglês',
      'sexo_paciente' => 'Sexo do Paciente',
      'idade_paciente' => 'Idade do Paciente',
      'doenca_diagnosticada' => 'Doença Diagnosticada',
      'relato_pt' => 'Relato de Caso Clínico - Português',
      'relato_en' => 'Relato de Caso Clínico - Inglês',
      'discussao_pt' => 'Discussão e conclusão - Português',
      'discussao_en' => 'Discussão e conclusão - Inglês',
      'evolucao' => 'Indique a evolução de exames laboratoriais ou outros eventos que achar relevantes (opcional)',
      'referencias' => 'Referências completas',
    ],

];
