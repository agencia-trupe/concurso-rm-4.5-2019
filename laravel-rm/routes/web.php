<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('register', 'Auth\RegisterController@register'); // rota desativada
Route::post('register', 'Auth\RegisterController@postRegister'); // rota desativada

Route::get('criar-senha/{token}/{login}', ['as' => 'criar-senha', 'uses' => 'Auth\LoginController@getCriarSenha']);
Route::post('criar-senha', ['as' => 'gravar-senha', 'uses' => 'Auth\LoginController@postCriarSenha']);

Route::get('/', ['as' => 'home', 'uses' => 'CommonController@home']);

Route::get('aviso-de-privacidade', function(){ 
  if(App::getLocale() == 'en') {
    return view('common.politica-de-privacidade-en');
  }
  return view('common.politica-de-privacidade'); 
});

Route::get('lang/{locale}', function($locale) {
  if (in_array($locale, ['en', 'pt-br'])) Session::put('locale', $locale);
  return back();
});

Route::group([
  'middleware' => ['auth', 'auth.ativos']
], function () {
  Route::get('casos-2018', ['as' => 'blog-listagem', 'uses' => 'BlogController@listagem']);
  Route::get('casos-2018/{id}', ['as' => 'blog-detalhe', 'uses' => 'BlogController@detalhe']);
});

Route::group([
  'middleware' => ['auth.ativos', 'acesso_editor']
], function() {
  Route::resource('blog', 'BlogController');  
  Route::post('blog/reorder', 'BlogController@reorder');
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_padrao']
], function(){
  Route::get('como-funciona', ['as' => 'como-funciona', 'uses' => 'CommonController@comoFunciona']);
  Route::get('regulamento', ['as' => 'regulamento', 'uses' => 'CommonController@regulamento']);
  Route::get('cronograma', ['as' => 'cronograma', 'uses' => 'CommonController@cronograma']);
  Route::get('avaliadores', ['as' => 'avaliadores', 'uses' => 'CommonController@avaliadores']);
  Route::get('consent-form', ['as' => 'user.consent', 'uses' => 'UserConsentController@form']);
  Route::post('consent-form', ['as' => 'user.consent.submit', 'uses' => 'UserConsentController@submit']);
});

Route::group([
  'middleware' => ['auth.ativos', 'acesso_medico_naoparticipante']
], function(){
  Route::get('apresentacao', ['as' => 'apresentacao', 'uses' => 'CommonController@apresentacao']);
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_avaliador']
], function(){
  Route::get('avaliacoes', ['as' => 'avaliacoes', 'uses' => 'AvaliacoesController@index']);
  Route::get('buscar-avaliacoes', ['as' => 'buscar-avaliacoes', 'uses' => 'AvaliacoesController@buscar']);
  Route::post('enviar-notas', ['as' => 'avaliar', 'uses' => 'AvaliacoesController@enviarNotas']);
  Route::get('avaliation-case-data/{caso_codigo}', 'AvaliacoesController@caseData');
});

Route::group([
'middleware' => ['auth.ativos', 'envio_casos']
], function(){
  Route::get('preencher-caso/{categoria}', ['as' => 'preencher-caso', 'uses' => 'CasosController@preencherCaso']);
  Route::post('preencher-caso', ['as' => 'preencher-caso', 'uses' => 'CasosController@postPreencherCaso']);
  Route::post('preencher-caso-e-finalizar', ['as' => 'preencher-caso', 'uses' => 'CasosController@postPreencherCasoFinalizar']);
  Route::get('aviso-grupo', ['as' => 'aviso-grupo', 'uses' => 'GroupController@aviso']);
  Route::get('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@submeter']);
  Route::post('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@postSubmeter']);
  Route::get('download-formulario', ['as' => 'download-formulario', 'uses' => 'CasosController@downloadFormulario']);  
  Route::get('grupo', ['as' => 'grupo', 'uses' => 'GroupController@get']);
  Route::post('quill-img-upload', 'CasosController@quillImageUpload');
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_admin']
], function(){

  Route::get('novos-casos', ['as' => 'novos-casos', 'uses' => 'AdminController@novosCasos']);
  Route::get('download-caso/{caso_codigo}', ['as' => 'download-caso', 'uses' => 'AdminController@downloadCaso']);
  Route::get('liberar-caso/{caso_codigo}', ['as' => 'liberar-caso', 'uses' => 'AdminController@liberarCaso']);
  Route::get('distribuir-caso/{caso_codigo}', ['as' => 'distribuir-caso', 'uses' => 'AdminController@distribuirCaso']);
  Route::get('excluir-caso/{caso_codigo}', ['as' => 'excluir-caso', 'uses' => 'AdminController@excluirCaso']);
  Route::get('pdf-caso/{caso_codigo}', ['as' => 'pdf-caso', 'uses' => 'AdminController@exportarPdfCaso']);
  Route::get('status-envios', ['as' => 'status-envios', 'uses' => 'AdminController@statusEnvios']);
  Route::get('historico', ['as' => 'historico', 'uses' => 'AdminController@historico']);
  Route::get('ranking', ['as' => 'ranking', 'uses' => 'AdminController@ranking']);

  Route::get('log-adesao', ['as' => 'usuarios-log-adesao', 'uses' => 'AdminController@logAdesaoAosTermos']);
  Route::get('usuarios', ['as' => 'usuarios-lista', 'uses' => 'AdminController@usuarios']);
  Route::get('ativar-usuario/{email}', 'AdminController@ativarUsuario');
  Route::get('iniciar-usuarios', 'AdminController@iniciarUsuarios');
  Route::post('encaminhar-casos', ['as' => 'encaminhar-casos', 'uses' => 'AdminController@encaminharFarmaco']);

  Route::get('gerenciar-grupos', ['as' => 'manage-groups.index', 'uses' => 'AdminGroupController@index']);
  Route::get('gerenciar-grupos/criar-grupo', ['as' => 'manage-groups.create', 'uses' => 'AdminGroupController@create']);
  Route::post('gerenciar-grupos/criar-grupo', ['as' => 'manage-groups.store', 'uses' => 'AdminGroupController@store']);
  Route::get('gerenciar-grupos/adicionar-integrante/{group_name}', ['as' => 'manage-groups.add-form', 'uses' => 'AdminGroupController@addForm']);
  Route::post('gerenciar-grupos/adicionar-integrante', ['as' => 'manage-groups.add', 'uses' => 'AdminGroupController@add']);
  Route::get('gerenciar-grupos/convidar/{group_name}', ['as' => 'manage-groups.invite', 'uses' => 'AdminGroupController@invite']);
  Route::get('gerenciar-grupos/remover/{coordenador_id}', ['as' => 'manage-groups.destroy', 'uses' => 'AdminGroupController@destroy']);
  Route::get('gerenciar-grupos/remover-medico/{medico_id}', ['as' => 'manage-groups.destroy-groupmember', 'uses' => 'AdminGroupController@destroyGroupMember']);

  Route::get('case-data/{caso_codigo}', 'AdminController@caseData');
  
  // Route::get('enviar-todos-casos', function(){
  //   $casos = RM\Models\Caso::all();
  //   foreach($casos as $x => $caso){
  //     echo $caso->id.' enviado<br>';
  //     //  ENVIAR CASO
  //     $ids = $caso->categoria.$caso->coordenador->id.$caso->id;
  //     $cod = 'C'.str_pad($ids, 6, '0', STR_PAD_LEFT);
  //     $caso->codigo = $cod;
  //     $caso->autor = 'Nome Autor teste#'.$x;
  //     $caso->enviado_em = Carbon\Carbon::now();

  //     $filename = $cod.'_'.Date('dmYHis').'.pdf';
  //     $caso->arquivo = $filename;
  //     $caso->excluido_em = null;
  //     $caso->save();
  //   }
  // });

  // Route::get('distribuir-todos', function(){
  //   $casos = RM\Models\Caso::all();
  //   foreach($casos as $x => $caso){
  //     echo $caso->id.' distribuido<br>';

  //     // DISTRIBUIR CASO
  //     $avaliadores = RM\Models\User::avaliadores()
  //                                     ->filtrarLocal($caso->coordenador->cidade)
  //                                     ->inRandomOrder()
  //                                     ->limit(3)
  //                                     ->get();


  //     if(count($avaliadores) != 3)
  //       return die('Não foi possível distribuir o caso clínico. (Erro: Total de avaliadores insuficiente (3), selecionados:'.count($avaliadores).')');

  //     foreach ($avaliadores as $avaliador) {
  //       // Gerar 1 registro de Avaliacao para cada avaliador
  //       $avaliacao = new RM\Models\Avaliacao(['casos_id' => $caso->id]);
  //       $avaliador->avaliacoes()->save($avaliacao);
  //     }

  //     $caso->distribuido_em = date('Y-m-d H:i:s');
  //     $caso->arquivo = null;
  //     $caso->save();
  //   }
  // });
  
});
